var baseurl = location.protocol + "//" + location.host + "/thantikandh/";
$(document).ready(function(){

});

$(".publishToggle").click(function(){
	var item = $(this).attr("data-target");
	var link = $(this).attr("data-link");
	var dataId = $(this).attr("data-id");

	var request = $.ajax({
	  url: baseurl + 'admin/'+ link + '/publishToggle',
	  type: "POST",
	  data: {id : dataId},
	  dataType: "json",
	  success: function (data) {
	  			   var test = '#'+item+'-'+dataId;
	               if(data.message == "published"){
	               		$(String(test)).removeClass('btn-danger').addClass('btn-success');
	               }else{
	               		$(String(test)).removeClass('btn-success').addClass('btn-danger');
	               }
	           }	
	});
});

$(".delFile").click(function(){
	var item = $(this).attr("data-target");
	var link = $(this).attr("data-link");
	var dataId = $(this).attr("data-id");

	var request = $.ajax({
	  url: baseurl + 'admin/'+ link + '/delFile',
	  type: "POST",
	  data: {id : dataId, item: item},
	  dataType: "json",
	  success: function (data) {
	               if(data.message == "deleted"){
	               		window.location.reload(true);
	               }else{
	               		alert('failed');
	               }
	           }	
	});
});

$(document).ready(function(){
	if($("input[name='isNepali']:checked").val() == "1"){
		$('#nepSlug').show();
		$('#slug').prop('disabled', false);
	}else{
		$('#nepSlug').hide();
		$('#slug').prop('disabled', true);
	}
})
$('.langToggle').change(function(){
	if($("input[name='isNepali']:checked").val() == "1"){
		$('#nepSlug').show();
		$('#slug').prop('disabled', false);
	}else{
		$('#nepSlug').hide();
		$('#slug').prop('disabled', true);
	}
})

