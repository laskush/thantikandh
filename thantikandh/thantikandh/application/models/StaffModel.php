<?php 
    class StaffModel extends CI_Model{

    	public function checkUser($user, $pass){
           $check = $this->db->from('tbl_staff')->where('username',$user)->where('password',$pass)->get()->row();
           return $check;
    	}

    	public function getDetails($user){
           $details = $this->db->from('tbl_staff')->where('username',$user)->get()->row();
           return $details;
    	}

    }
?>