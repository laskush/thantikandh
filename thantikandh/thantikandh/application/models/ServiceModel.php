<?php 
    class ServiceModel extends CI_Model{

        public function insertService($data){
            $this->db->insert('tbl_categories',$data);
        }

        public function delete($id){
            //to delete
            $this->db->where('id', $id);
            $this->db->delete('tbl_categories');

        }

        public function getService(){
            $this->db->order_by('type','desc');
            $data = $this->db->get('tbl_categories');
            return $data->result();
        }

        public function getById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_categories');
            return $data->row();
        }

        public function update($data){
            $this->db->where('id', $data["id"]);
            return($this->db->update('tbl_categories',$data));
        }

        public function publish($id){
            $this->db->select('status');
            $this->db->from('tbl_categories');
            $this->db->where('id', $id);
            $stat = $this->db->get();
            if($stat->row('status') == '1'){
                $this->db->where('id', $id);
                $this->db->update('tbl_categories',array('status'=>'0'));
                return "unpublished";
            }else{
                $this->db->where('id', $id);
                $this->db->update('tbl_categories',array('status'=>'1'));
                return "published";
            }

        }

        
    }
?>