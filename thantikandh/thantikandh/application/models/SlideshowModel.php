<?php 
    class SlideshowModel extends CI_Model{

        public function insertImage($data){
            $this->db->insert('tbl_slideshow',$data);
        }

        public function delete($id){
            //to delete
            $this->db->where('id', $id);
            $this->db->delete('tbl_slideshow');

        }

        public function getImage(){
            $this->db->where('status', '1');
            $data = $this->db->get('tbl_slideshow');
            return $data->result();
        }

        public function getById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_slideshow');
            return $data->row();
        }

        public function update($data){
            $this->db->where('id', $data["id"]);
            return($this->db->update('tbl_slideshow',$data));
        }

        public function publish($id){
            $this->db->select('status');
            $this->db->from('tbl_slideshow');
            $this->db->where('id', $id);
            $stat = $this->db->get();
            if($stat->row('status') == '1'){
                $this->db->where('id', $id);
                $this->db->update('tbl_slideshow',array('status'=>'0'));
                return "unpublished";
            }else{
                $this->db->where('id', $id);
                $this->db->update('tbl_slideshow',array('status'=>'1'));
                return "published";
            }

        }


        //for API

        public function fetchImage(){
            $this->db->select(array('title', 'image', 'caption', 'location'));
            $this->db->where('status', '1');
            $data = $this->db->get('tbl_slideshow');
            return $data->result();
        }
    }
?>