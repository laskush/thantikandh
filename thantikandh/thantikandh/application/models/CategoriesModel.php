<?php 
    class CategoriesModel extends CI_Model{

        public function getAllCategories(){
            $data = $this->db->get('tbl_categories');
            return $data->result();
        }

        public function getCategories($type){
            $this->db->from('tbl_categories');
            $this->db->where('type', $type);
            $data = $this->db->get();
            return $data->result();
        }

    }
?>