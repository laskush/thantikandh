<?php 
    class MemberModel extends CI_Model{

        public function insertMember($data){
            $this->db->insert('tbl_member',$data);
        }

        public function delete($id){
            //to delete
            $this->db->where('id', $id);
            $this->db->delete('tbl_member');

        }

        public function getMember(){
            $data = $this->db->get('tbl_member');
            return $data->result();
        }

        public function getMemberByCate($type){
            $this->db->where('type',$type);
            $data = $this->db->get('tbl_member');
            return $data->result();
        }

        public function getById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_member');
            return $data->row();
        }

        public function update($data){
            $this->db->where('id', $data["id"]);
            return($this->db->update('tbl_member',$data));
        }

        public function getHomeMember(){
            $this->db->limit(2);
            $data = $this->db->get('tbl_member');
            return $data->result();
        }

        public function publish($id){
            $this->db->select('status');
            $this->db->from('tbl_member');
            $this->db->where('id', $id);
            $stat = $this->db->get();
            if($stat->row('status') == '1'){
                $this->db->where('id', $id);
                $this->db->update('tbl_member',array('status'=>'0'));
                return "unpublished";
            }else{
                $this->db->where('id', $id);
                $this->db->update('tbl_member',array('status'=>'1'));
                return "published";
            }

        }
    }
?>