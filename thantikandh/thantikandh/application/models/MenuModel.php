<?php 
    class MenuModel extends CI_Model{

        public function getParentMenu(){
            $this->db->where(array('parentOf'=>'0'));
            $data = $this->db->get('tbl_menu');
            return $data->result();
        }

        public function getChildMenu($id){
            $this->db->where(array('parentOf'=>$id));
            $data = $this->db->get('tbl_menu');
            return $data->result();
        }

        public function getAllMenu(){
            $data = $this->db->get('tbl_menu');
            return $data->result();
        }

        public function getMenuById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_menu');
            return $data->row();
        }

        public function insertMenu($data){
            $this->db->insert('tbl_menu',$data);
        }

        public function update($data){
            $this->db->where('id', $data["id"]);
            return($this->db->update('tbl_menu',$data));
        }

        public function delete($id){
            //to delete
            $this->db->where('id', $id);
            $this->db->delete('tbl_menu');

        }

        public function publish($id){
            $this->db->select('status');
            $this->db->from('tbl_menu');
            $this->db->where('id', $id);
            $stat = $this->db->get();
            if($stat->row('status') == '1'){
                $this->db->where('id', $id);
                $this->db->update('tbl_menu',array('status'=>'0'));
                return "unpublished";
            }else{
                $this->db->where('id', $id);
                $this->db->update('tbl_menu',array('status'=>'1'));
                return "published";
            }

        }
    }
?>