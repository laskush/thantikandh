<?php 
    class ModuleModel extends CI_Model{

        public function getModule(){
        	$this->db->where('status', '1');
        	$this->db->order_by('sort asc');
    		$data = $this->db->get('tbl_module');
    		return $data->result();
    	}

    }
?>