<?php 
    class ArticleModel extends CI_Model{

        public function insertArticle($data){
            $this->db->insert('tbl_article',$data);
        }

        public function delete($id){
            //to delete
            $this->db->where('id', $id);
            $this->db->delete('tbl_article');

        }

        public function getArticle(){
            $data = $this->db->get('tbl_article');
            return $data->result();
        }

        public function getById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_article');
            return $data->row();
        }

        public function update($data){
            $this->db->where('id', $data["id"]);
            return($this->db->update('tbl_article',$data));
        }

        public function getList(){
            $this->db->select(array('title', 'slug'));
            $this->db->from('tbl_article');
            $data = $this->db->get();
            return $data->result();
        }
        
        public function getHomeById($id){
            $this->db->select(array('title', 'slug', 'content', 'titleNe', 'contentNe'));
            $this->db->from('tbl_article');
            $this->db->where('id', $id);
            $data = $this->db->get();
            return $data->row();
        }

        public function getArticleBySlug($slug){
            $this->db->from('tbl_article');
            $this->db->where('slug', $slug);
            $data = $this->db->get();
            return $data->row();
        }

        public function publish($id){
            $this->db->select('status');
            $this->db->from('tbl_article');
            $this->db->where('id', $id);
            $stat = $this->db->get();
            if($stat->row('status') == '1'){
                $this->db->where('id', $id);
                $this->db->update('tbl_article',array('status'=>'0'));
                return "unpublished";
            }else{
                $this->db->where('id', $id);
                $this->db->update('tbl_article',array('status'=>'1'));
                return "published";
            }

        }

    }
?>