<?php 
    class OrganizationModel extends CI_Model{

        public function insertOrganization($data){
            $this->db->insert('tbl_organization',$data);
        }

        public function delete($id){
            //to delete
            $this->db->where('id', $id);
            $this->db->delete('tbl_organization');

        }

        public function getOrganization(){
            $data = $this->db->get('tbl_organization');
            return $data->result();
        }

        public function getById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_organization');
            return $data->row();
        }

        public function update($data){
            $this->db->where('id', $data["id"]);
            return($this->db->update('tbl_organization',$data));
        }

        public function getList(){
            $this->db->select(array('title', 'slug'));
            $this->db->from('tbl_organization');
            $data = $this->db->get();
            return $data->result();
        }

        public function publish($id){
            $this->db->select('status');
            $this->db->from('tbl_organization');
            $this->db->where('id', $id);
            $stat = $this->db->get();
            if($stat->row('status') == '1'){
                $this->db->where('id', $id);
                $this->db->update('tbl_organization',array('status'=>'0'));
                return "unpublished";
            }else{
                $this->db->where('id', $id);
                $this->db->update('tbl_organization',array('status'=>'1'));
                return "published";
            }

        }

        /*---------------*/
        public function getMember($id){
            $this->db->where('o_id', $id);
            $data = $this->db->get('tbl_omember');
            return $data->result();
        }

        public function deleteMember($id){
            //to delete
            $this->db->where('id', $id);
            $this->db->delete('tbl_omember');

        }

        public function addMember($data){
            $this->db->insert('tbl_omember',$data);
        }

        public function updateMember($data){
            $this->db->where('id', $data["id"]);
            return($this->db->update('tbl_omember',$data));
        }

        public function getMemById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_omember');
            return $data->row();
        }

    }
?>