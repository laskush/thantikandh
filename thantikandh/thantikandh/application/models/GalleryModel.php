<?php 
    class GalleryModel extends CI_Model{

        //gallery category operation
        public function getImage(){
            $data = $this->db->get('tbl_gallerycate');
            return $data->result();
        }

        //gallery images operation
        public function getAllGalleryImage(){
            $data = $this->db->get('tbl_gallery');
            return $data->result();
        }

        public function getGalleryImage($id){
            $this->db->where('gallery_cate', $id);
            $data = $this->db->get('tbl_gallery');
            return $data->result();
        }

        public function insertImage($data){
            $this->db->insert('tbl_gallery',$data);
        }

        public function delete($id){
            $this->db->where('id', $id);
            $this->db->delete('tbl_gallery');
        }

        public function getById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_gallery');
            return $data->row();
        }

        public function update($data){
            $this->db->where('id', $data["id"]);
            return($this->db->update('tbl_gallery',$data));
        }

        public function insertCateImage($data){
            $this->db->insert('tbl_gallerycate',$data);
        }

        public function getCateById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_gallerycate');
            return $data->row();
        }
        public function deleteCate($id){
            $this->db->where('id', $id);
            $this->db->delete('tbl_gallerycate');
        }

    }
?>