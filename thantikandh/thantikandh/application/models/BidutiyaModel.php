<?php 
    class BidutiyaModel extends CI_Model{

        public function insertBidutiya($data){
            $this->db->insert('tbl_bidutiya',$data);
        }

        public function delete($id){
            //to delete
            $this->db->where('id', $id);
            $this->db->delete('tbl_bidutiya');

        }

        public function getBidutiya(){
            $data = $this->db->get('tbl_bidutiya');
            return $data->result();
        }

        public function getBidutiyaByCate($cate){
            $this->db->where('category', $cate);
            $data = $this->db->get('tbl_bidutiya');
            return $data->result();
        }

        public function getById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_bidutiya');
            return $data->row();
        }

        public function update($data){
            $this->db->where('id', $data["id"]);
            return($this->db->update('tbl_bidutiya',$data));
        }

        public function getList(){
            $this->db->select(array('title', 'slug'));
            $this->db->from('tbl_bidutiya');
            $data = $this->db->get();
            return $data->result();
        }
        public function getHomeBidutiya($category){
            $this->db->select(array('title', 'slug', 'description', 'content', 'titleNe', 'descriptionNe', 'contentNe', 'st_date', 'end_date'));
            $this->db->from('tbl_bidutiya');
            $this->db->where(array('category'=> $category, 'isNepali' => '0'));
            $this->db->limit(6); 
            $data = $this->db->get();
            return $data->result();
        }

        public function getBidutiyaBySlug($slug){
            $this->db->from('tbl_bidutiya');
            $this->db->where('slug', $slug);
            $data = $this->db->get();
            return $data->row();
        }

        public function deleteImage($id, $file){

            $data = array($file => '');

            $this->db->where('id', $id);
            if($this->db->update('tbl_bidutiya',$data)){
                return "deleted";
            }
        }

        public function publish($id){
            $this->db->select('status');
            $this->db->from('tbl_bidutiya');
            $this->db->where('id', $id);
            $stat = $this->db->get();
            if($stat->row('status') == '1'){
                $this->db->where('id', $id);
                $this->db->update('tbl_bidutiya',array('status'=>'0'));
                return "unpublished";
            }else{
                $this->db->where('id', $id);
                $this->db->update('tbl_bidutiya',array('status'=>'1'));
                return "published";
            }

        }

        //for api
        public function fetchBidutiya(){
            $this->db->select(array('title', 'description', 'content', 'st_date'));
            $this->db->where(array('status' => '1', 'category' => 'bidutiya'));
            $data = $this->db->get('tbl_bidutiya');
            return $data->result();
        }

    }
?>