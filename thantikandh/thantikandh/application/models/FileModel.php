<?php 
    class FileModel extends CI_Model{

        public function insertFile($data){
            $this->db->insert('tbl_files',$data);
        }

        public function delete($id){
            //to delete
            $this->db->where('id', $id);
            $this->db->delete('tbl_files');

        }

        public function getFile(){
            $data = $this->db->get('tbl_files');
            return $data->result();
        }

        public function getById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_files');
            return $data->row();
        }

        public function update($data){
            $this->db->where('id', $data["id"]);
            return($this->db->update('tbl_files',$data));
        }

        public function publish($id){
            $this->db->select('status');
            $this->db->from('tbl_files');
            $this->db->where('id', $id);
            $stat = $this->db->get();
            if($stat->row('status') == '1'){
                $this->db->where('id', $id);
                $this->db->update('tbl_files',array('status'=>'0'));
                return "unpublished";
            }else{
                $this->db->where('id', $id);
                $this->db->update('tbl_files',array('status'=>'1'));
                return "published";
            }

        }
    }
?>