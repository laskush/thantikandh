<?php 
    class ContactModel extends CI_Model{

        public function getContact(){
            $this->db->where('id', '1');
            $data = $this->db->get('tbl_contact');
            return $data->row();
        }

        public function getById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_contact');
            return $data->row();
        }

        public function update($data){
            $this->db->where('id', $data["id"]);
            return($this->db->update('tbl_contact',$data));
        }

    }
?>