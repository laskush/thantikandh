<?php 
    class BudgetModel extends CI_Model{

        public function insertBudget($data){
            $this->db->insert('tbl_budget',$data);
        }

        public function delete($id){
            //to delete
            $this->db->where('id', $id);
            $this->db->delete('tbl_budget');

        }

        public function getBudget(){
            $data = $this->db->get('tbl_budget');
            return $data->result();
        }

        public function getById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_budget');
            return $data->row();
        }

        public function update($data){
            $this->db->where('id', $data["id"]);
            return($this->db->update('tbl_budget',$data));
        }

        public function getList(){
            $this->db->select(array('title', 'slug'));
            $this->db->from('tbl_budget');
            $data = $this->db->get();
            return $data->result();
        }

        public function getBudgetBySlug($slug){
            $this->db->from('tbl_budget');
            $this->db->where('slug', $slug);
            $data = $this->db->get();
            return $data->row();
        }

        public function publish($id){
            $this->db->select('status');
            $this->db->from('tbl_budget');
            $this->db->where('id', $id);
            $stat = $this->db->get();
            if($stat->row('status') == '1'){
                $this->db->where('id', $id);
                $this->db->update('tbl_budget',array('status'=>'0'));
                return "unpublished";
            }else{
                $this->db->where('id', $id);
                $this->db->update('tbl_budget',array('status'=>'1'));
                return "published";
            }

        }

        //for api
        public function fetchBudget(){
            $this->db->select(array('title', 'description', 'amount', 'date', 'particular'));
            $data = $this->db->get('tbl_budget');
            return $data->result();
        }

    }
?>