<?php 
    class PlanModel extends CI_Model{

        public function insertPlan($data){
            $this->db->insert('tbl_plan',$data);
        }

        public function delete($id){
            //to delete
            $this->db->where('id', $id);
            $this->db->delete('tbl_plan');

        }

        public function getPlan(){
            $data = $this->db->get('tbl_plan');
            return $data->result();
        }

        public function getPlanByCate($cate){
            $this->db->where('category', $cate);
            $data = $this->db->get('tbl_plan');
            return $data->result();
        }

        public function getById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_plan');
            return $data->row();
        }

        public function update($data){
            $this->db->where('id', $data["id"]);
            return($this->db->update('tbl_plan',$data));
        }

        public function getList(){
            $this->db->select(array('title', 'slug'));
            $this->db->from('tbl_plan');
            $data = $this->db->get();
            return $data->result();
        }

        public function getPlanBySlug($slug){
            $this->db->from('tbl_plan');
            $this->db->where('slug', $slug);
            $data = $this->db->get();
            return $data->row();
        }

        public function publish($id){
            $this->db->select('status');
            $this->db->from('tbl_plan');
            $this->db->where('id', $id);
            $stat = $this->db->get();
            if($stat->row('status') == '1'){
                $this->db->where('id', $id);
                $this->db->update('tbl_plan',array('status'=>'0'));
                return "unpublished";
            }else{
                $this->db->where('id', $id);
                $this->db->update('tbl_plan',array('status'=>'1'));
                return "published";
            }

        }

        //for api
        public function fetchPlan(){
            $this->db->select(array('title', 'description', 'content', 'st_date'));
            $this->db->where(array('status' => '1', 'category' => 'plan'));
            $data = $this->db->get('tbl_plan');
            return $data->result();
        }

        public function deleteImage($id, $file){

            $data = array($file => '');

            $this->db->where('id', $id);
            if($this->db->update('tbl_plan',$data)){
                return "deleted";
            }
        }

    }
?>