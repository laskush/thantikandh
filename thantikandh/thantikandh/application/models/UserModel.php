<?php 
    class UserModel extends CI_Model{

        public function getByUser($user){
            $this->db->where('username', $user);
            $data = $this->db->get('tbl_user');
            return $data->row();
        }

        public function getById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_user');
            return $data->row();
        }

        public function update($data){
            $this->db->where('id', $data["id"]);
            return($this->db->update('tbl_user',$data));
        }

    }
?>