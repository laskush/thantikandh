<?php 
    class SocialModel extends CI_Model{

        public function insertSocial($data){
            $this->db->insert('tbl_social',$data);
        }

        public function delete($id){
            //to delete
            $this->db->where('id', $id);
            $this->db->delete('tbl_social');

        }

        public function getSocial(){
            $data = $this->db->get('tbl_social');
            return $data->result();
        }

        public function getById($id){
            $this->db->where('id', $id);
            $data = $this->db->get('tbl_social');
            return $data->row();
        }

        public function update($data){
            $this->db->where('id', $data["id"]);
            return($this->db->update('tbl_social',$data));
        }

        public function getList(){
            $this->db->select(array('title', 'slug'));
            $this->db->from('tbl_news');
            $data = $this->db->get();
            return $data->result();
        }

        public function publish($id){
            $this->db->select('status');
            $this->db->from('tbl_social');
            $this->db->where('id', $id);
            $stat = $this->db->get();
            if($stat->row('status') == '1'){
                $this->db->where('id', $id);
                $this->db->update('tbl_social',array('status'=>'0'));
                return "unpublished";
            }else{
                $this->db->where('id', $id);
                $this->db->update('tbl_social',array('status'=>'1'));
                return "published";
            }

        }

    }
?>