<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'web';
$route['api'] = 'api';
$route['ne/(:any)'] = 'nepali/$1';
$route['ne/(:any)/(:any)'] = 'nepali/$1/$2';

$route['admin'] = 'admin/welcome';
$route['admin/(:any)'] = 'admin/$1';

$route['member'] = 'member/welcome';
$route['member/auth/(:any)'] = 'member/Auth/$1';
$route['member/(:any)'] = 'member/welcome/$1';
$route['member/(:any)/(:any)'] = 'member/welcome/$1/$2';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
