<?php $this->load->view('nepali/header'); ?>

<style>
	#map{width:100%;height:650px;}
	.radio {margin-top: 10px;z-index: 99;position: absolute;background: white; right:25px; float:right; padding: 15px 15px 0 15px;
</style>

<link rel="stylesheet" href="<?php echo base_url('assets/map/css/leaflet.css') ?>"/>
<script src="<?php echo base_url('assets/map/js/leaflet.js') ?>"></script>

<script src="<?php echo base_url('assets/map/js/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/map/js/randomColor.js') ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/map/js/leaflet.ajax.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/map/js/loadingoverlay.min.js') ?>"></script>

<!-- all section are included in this one section -->
<section class="clearfix mainsection-bg">
	<div class="container padder">
		<div class="row">
			<!-- this division contain the actual content -->
			<div class="col-sm-12">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Map</li>
				  </ol>
				</nav>
				<div class="about-us">
					<div class="radio">
						<input  class="CheckBox" type="checkbox" name="check"  value="province" checked > Province</input><br>
						<input class="CheckBox" type="checkbox" name="check"  value="district" >District</input><br>
						<input class="CheckBox" type="checkbox" name="check"  value="municipality">Municipality</input><br><br>
					</div>
					<div id="map"></div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end of actual div -->

<?php 
  $CI =& get_instance();
  $CI->load->model('ContactModel');
  $CI->load->model('SocialModel');
  $social = $CI->SocialModel->getSocial();
  $contact = $CI->ContactModel->getContact();
?>
<section class="clearfix top-footer padder">
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <img src="<?php echo base_url(); ?>uploads/images/contact/<?php echo $contact->logo; ?>" class="img-responsive">
        <p>
          <?php echo $contact->brief; ?>
        </p>
      </div>
      <div class="col-sm-2">
        <div class="links">
        <h4 style="margin-left: 15px;">Social Links</h4>
        <ul>
          <?php 
            foreach ($social as $link) {
              echo '<li><i class="fa '.$link->icon.'" style="color: white;"></i> &nbsp;<a href="'.$link->linksrc.'">'.$link->name.'</a></li>';
            }
          ?>
        </ul>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="service">
          <?php echo $contact->map; ?>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="other">
        <h4 style="margin-left: 20px;">Contact</h4>
          <span style="color: #fff; text-align: justify;">
            <?php 
              echo '<b>' .$contact->name . '</b><br>'; 
              echo $contact->address . '<br>'; 
              echo '<i class="fa fa-phone"></i> &nbsp;'. $contact->contact . '<br>'; 
              echo '<i class="fa fa-envelope"></i> &nbsp;'. $contact->email; 
            ?>
          </span>
        </div>
      </div>
    </div>
    <hr class="footer-hr">
    <div class="row">
      <div class="col-sm-6">
        <p class="copyright">© 2018 - Fonticons, Inc. All rights reserved</p>
      </div>
      <div class="col-sm-6">
        <p class="poweredby">Powered By: <a href="#">Prologic Solutions</a></p>
      </div>
    </div>
  </div>
</section>

<script>
	$(document).ready(function(){
		
		map = L.map("map", {
			center: [27.701871, 85.315297],
			zoom: 12
		});	
		/* var osm = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
				attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
			}).addTo(map);*/
				
		icons = L.icon({
			//iconSize: [27, 27],
			iconAnchor: [13, 27],
			popupAnchor:  [2, -24],
			iconUrl: 'http://localhost/b/map/images/marker-icon.png'
		});
		
		
		var osm = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
		});
		
		googleStreets = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
			maxZoom: 20,
			subdomains:['mt0','mt1','mt2','mt3']
		});
		googleHybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
			maxZoom: 20,
			subdomains:['mt0','mt1','mt2','mt3']
		});
		googleSat = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
			maxZoom: 20,
			subdomains:['mt0','mt1','mt2','mt3']
		});
		googleTerrain = L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
			maxZoom: 20,
			subdomains:['mt0','mt1','mt2','mt3']
		});
		//var none = "";
		var baseLayers = {
			"OpenStreetMap": osm,
			"Google Streets": googleStreets,
			"Google Hybrid": googleHybrid,
			"Google Satellite": googleSat,
			"Google Terrain": googleTerrain//,
			//"None": none
		};
		//map.addLayer(googleHybrid);

		//province layer
		  
		province = new L.geoJson.ajax("<?php echo base_url('assets/map/geojson/province.geojson') ?>", {
		  	onEachFeature: function(feature,layer){
	  			layer.bindPopup("Province No. "+feature.properties.Province);
				// table is created to put all the data of the database into the marker on one click

				layer.setStyle({
				 	fillColor:randomColor(),
				 	fillOpacity:1,
				 	weight: 1,
				 	opacity: 1,
					color: 'green'//,
					//dashArray: '3'
				});
			}
		});

		province.on('data:loaded', function (data) {
		  	map.fitBounds(province.getBounds());
		  	console.log("province");
		});
  
   		//municipal layer
   
		/*municipality = new L.geoJson.ajax("geojson/municipality.geojson", {
			onEachFeature: function(feature,layer){
				layer.bindPopup(feature.properties.GaPa_NaPa);
				// table is created to put all the data of the database into the marker on one click
				layer.setStyle({	
					fillColor:randomColor(),
					fillOpacity:1,
					weight: 1,
					opacity: 1,
					color: 'red',
					dashArray: '3'
				});
			}
		});

		municipality.on('data:loaded', function (data) {
           // map.fitBounds(municipality.getBounds());
            console.log("municipality");   
        });
        */
        province.addTo(map);
        
        $(".CheckBox").one('click',function( event ) {
        	$.LoadingOverlay("show");
        	checkboxClicked = $(this).val();
        	
        	window[checkboxClicked] = new L.geoJson.ajax("<?php echo base_url('assets/map/geojson/"+checkboxClicked+".geojson') ?>", {
        		
        		onEachFeature: function(feature,layer){
        			console.log(checkboxClicked);
        			if(checkboxClicked=="district"){
        				layer.bindPopup(feature.properties.DISTRICT);
        			}
        			else if(checkboxClicked=="municipality"){
        				layer.bindPopup("DIstrict: "+feature.properties.DISTRICT+"<br/>Municipality: "+feature.properties.GaPa_NaPa);
        			}
	 				// table is created to put all the data of the database into the marker on one click

					layer.setStyle({
					 	
					 	fillColor:randomColor(),
					 	fillOpacity:1,
					 	weight: 1,
					 	opacity: 1,
					 	color: 'green',
						//dashArray: '3'
					});
				}
			});
        	
	        window[checkboxClicked].on('data:loaded', function (data) {
	    		$.LoadingOverlay("hide", true);
	    		console.log("districts");
	    	});
        });
        
        $(".CheckBox").click(function( event ) {
        	layerClicked = window[event.target.value];
			//console.log(event);
			if (map.hasLayer(layerClicked)) {
				map.removeLayer(layerClicked);
			}
			else{
				map.addLayer(layerClicked);
			} ;
		});
    });
</script>