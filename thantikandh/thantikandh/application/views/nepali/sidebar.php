<!-- sidebar start -->
				<div class="col-sm-4">

					<!-- login div start -->
					<div class="sidemunu">
						<ul>
							<li class="margnbotm"><i class="fa fa-lock"></i> <a href="<?php echo base_url()?>member">सदस्य लगिन</a></li>
							<li class="margnbotm"><i class="fa fa-envelope"></i> <a href="#">ईमेल हेर्नुहोस्</a></li>
							<li class="margnbotm"><i class="fa fa-users"></i> <a href="<?php echo base_url()?>web/team">कर्मचारी सूची</a></li>
							<!-- <li class="margnbotm"><i class="fa fa-envelope"></i> <a href="#">Check Email</a></li>
							<li class="margnbotm"><i class="fa fa-users"></i> <a href="#">Staff List</a></li> -->
						</ul>
					</div>
					<!-- end login div -->

					<!-- start latest notice div -->
					<div class="latest-notice">
							<div class="panel-group">
							  <div class="panel panel-primary">
							      <div class="panel-heading">नवीनतम सूचना</div>
							      <div class="panel-body">
							      	<ul>
						      		<?php
						      			$CI =& get_instance();
					                    $mod = $CI->load->model('NewsModel');
					                    $notice = $CI->NewsModel->getHomeNews('notice');
						      			if(isset($notice)){
						      				foreach ($notice as $not) {
						      					echo '<li><i class="fa fa-arrow-circle-right"></i> <a href="'.base_url().'/news/'.$not->slug.'">'.$not->title.'</a></li>';
						      				}
										}
									?>
							      	</ul>
							      </div>
							    </div>
						</div>
					</div>
				<!-- end latest notive div -->

				<!-- start related div -->
				<!-- <div class="panel-group related-links">
					  <div class="panel panel-primary">
					      <div class="panel-heading">सम्बन्धित लिङ्कहरू</div>
					      <div class="panel-body">
					      	<ul>
								<li><a href="#"><i class="fa fa-arrow-circle-right"></i> नेपाल सरकार</a></li>
								<li><a href="#"><i class="fa fa-arrow-circle-right"></i> नेपाल सरकार</a></li>
								<li><a href="#"><i class="fa fa-arrow-circle-right"></i> नेपाल सरकार</a></li>
								<li><a href="#"><i class="fa fa-arrow-circle-right"></i> नेपाल सरकार</a></li>
								<li><a href="#"><i class="fa fa-arrow-circle-right"></i> नेपाल सरकार</a></li>
							</ul>
					      </div>
					    </div>
				</div> -->
				<!-- end related div -->
			</div>
			<!-- sidebar ends -->
		</div>
	</div>
</section>