<!-- all section are included in this one section -->
<section class="clearfix mainsection-bg">
	<div class="container padder">
		<div class="row">
			<!-- this division contain the actual content -->
			<div class="col-sm-8">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="#">घर</a></li>
				    <li class="breadcrumb-item active" aria-current="page">हामीलाई सम्पर्क गर्नुहोस</li>
				  </ol>
				</nav>
				<div class="contact-us">
					<h2>हामीलाई सम्पर्क गर्नुहोस</h2>
					<hr class="h-r">
					<div class="contact-map">
						<?php echo $contact->map; ?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="contact-form">
							<h4>हामीलाई एक प्रतिक्रिया पठाउनुहोस्</h4>
							<hr class="h-r">
							<form>
					          <div class="form-group">
					            <input type="text" class="form-control" id="email" placeholder="Full Name" name="Full Name">
					          </div>
					          <div class="form-group">
					            <input type="email" class="form-control" id="pwd" placeholder="Email" name="Email">
					          </div>
					          <div class="form-group">
					            <input type="text" class="form-control" id="pwd" placeholder="Subject" name="Subject">
					          </div>
					          <div class="form-group cont-messg">
					            <input type="text" class="form-control" id="msg" placeholder="Message" name="message">
					          </div>
					          
					          <p id="captImg"><?php echo $captchaImg; ?></p>
							  <p>Can't read the image? click <a href="javascript:void(0);" class="refreshCaptcha">here</a> to refresh.</p>

							  <div class="form-group cont-messg">
					            <input type="text" class="form-control" id="captcha" placeholder="Enter the code" name="captcha">
					          </div>
					          <?php if(!empty($errMsg)){
					          	echo '<p id="captImg">'.$errMsg.'</p>';
					          } ?>
					          
					          <button type="submit" class="btn btn-default" name="submit" value="submit" id="sub">Submit</button>
					        </form>
						</div>
					</div>
					<div class="col-sm-6 contact-form cont-details">
						<h4>सम्पर्क</h4>
						<hr class="h-r">
						<p><b><?php echo $contact->nameNe; ?></b></p>
						<p><?php echo $contact->addressNe; ?></p>
						<p>Phone: <?php echo $contact->contactNe; ?></p>
						<p>Toll Free No: <?php echo $contact->toll_freeNe; ?></p>
						<p>Fax: <?php echo $contact->faxNe; ?></p>
						<p>E-mail : <?php echo $contact->email; ?></p>
					</div>
				</div>
			</div>
			<!-- end of actual div -->

			<!-- captcha refresh code -->
			<script>
			$(document).ready(function(){
			    $('.refreshCaptcha').on('click', function(){
			        $.get('<?php echo base_url().'web/refresh'; ?>', function(data){
			            $('#captImg').html(data);
			        });
			    });
			});
			</script>

			