<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>Dashboard</h2>

            <ul class="actions">
                <li>
                    <a href="#">
                        <i class="zmdi zmdi-trending-up"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="zmdi zmdi-check-all"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="#">Refresh</a>
                        </li>
                        <li>
                            <a href="#">Manage Widgets</a>
                        </li>
                        <li>
                            <a href="#">Widgets Settings</a>
                        </li>
                    </ul>
                </li>
            </ul>

        </div>
        <!-- breadcrumb ends -->
		
		<!-- main content -->
		<div class="row">
		    <div class="card">
		        <div class="card-body">
		        
		        <?php
		           $CI =& get_instance();
		           $mod = $CI->load->model('ModuleModel');
		           $modules = $CI->ModuleModel->getModule();
		           $colors = array("purple", "lightblue", "orange", "bluegray", "teal", "lightgreen", "cyan");

		           if(!empty($modules)) {
		           	$i=1;
		            foreach ($modules as $module) {
		            	$selector = $i % count($colors);
		            	$color = $colors[$selector];
		            	if($i++ == 1){continue;}
		              	echo '<div class="col-sm-6 col-md-3">
			                    <div class="mini-charts-item bgm-'.$color.'" onclick = "document.location = \''.base_url().$module->link.'\'" style="cursor:pointer">
			                        <div class="clearfix">
			                            <div class="chart">
			                            	<i class="zmdi '.$module->icon.'"></i>
			                            </div>
			                            <div class="count">
			                                <h5>'.$module->name.'</h5>
			                            </div>
			                        </div>
			                    </div>
			                </div>';
						$i++;
					}
		           } else {
		                echo "No Data";
		           }
		        ?>
		        </div>
		    </div>
	    </div>

	    <!-- main ends -->
	</div>


</section>
<style>
	.mini-charts-item .chart {
	    padding: 15px 32px;
	}
	.chart .zmdi {
		font-size: 45px;
		color: #eee;
	}
	.main-menu{
		margin-bottom: 0px;
	}
</style>