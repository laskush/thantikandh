<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>Article List</h2>

            <ul class="actions">
                <a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/article/addarticle'; ?>">Add Article </a>
            </ul>

        </div>
        <!-- breadcrumb ends -->
		
		<!-- main content -->
		<div class="row">
		    <!-- ------------------------------------- -->
		    <div class="card">
			    <div class="card-header">
			        <h2><small> Home > Article List</small>
			        </h2>
			    </div>

			    <div class="card-body card-padding">
			    	<!-- --------------------------------------------------------- -->
				    <div class="panel-group" id="accordion">
					  	<div class="panel panel-default">
					      <div class="panel-heading" style="height: 45px; background: #6eaeb9;">
					        <div class="panel-title">
					        	<a href="#content" data-toggle="collapse" data-parent="#accordion">
					        		<div class="col-md-3 col-xs-8 col-sm-3">
							          	Title
						        	</div>
						        	<div class="col-md-7 hidden-xs col-sm-7">
							          	Description
						        	</div>
						        </a>
						        <div class="col-md-2 col-xs-4 col-sm-2 text-center">
						        	Action
						        </div>
					        </div>
					      </div>
					    </div>
					    <?php foreach ($articles as $article): ?>
					    	<div class="panel panel-default">
						      <div class="panel-heading" style="height: 45px; background: #a7d1d8;">
						        <div class="panel-title">
						        	<a href="<?php echo base_url() . 'admin/article/editArticle/' . $article->id; ?>">
						        		<div class="col-md-3 col-xs-8 col-sm-4">
								          	<?php echo substr(strip_tags($article->title),0,180); ?>
							        	</div>
							        	<div class="col-md-7 hidden-xs col-sm-6">
								          	<?php echo substr(strip_tags($article->description),0,180).'...'; ?>
							        	</div>
							        </a>
							        <div class="col-md-2 col-xs-4 col-sm-2 text-center" style="margin-top:-10px">
							        	<a class="btn btn-xs btn-primary" href="<?php echo base_url() . 'admin/article/editArticle/' . $article->id; ?>"><i class="fa fa-eye"></i></a>
							        	<?php 
							        		if($article->status == 1){
							        			echo '<a class="btn btn-xs btn-success publishToggle" data-target="article" data-link="article" data-id="'.$article->id.'" id="article-'.$article->id.'" title="published"><i class="fa fa-flag"></i></a>';
		        		}else{
		        			echo '<a class="btn btn-xs btn-danger publishToggle" data-target="article" data-link="article" data-id="'.$article->id.'" id="article-'.$article->id.'" title="unpublished"><i class="fa fa-flag"></i></a>';
		        		}
		        		if($article->id != 1 && $article->id != 2){ 
		        		?>
		        			<a class="btn btn-xs btn-danger" href="<?php echo base_url() . 'admin/article/deleteArticle/' . $article->id; ?>"><i class="fa fa-trash"></i></a>
		        	<?php }
		        	?>
		        </div>
	        </div>
	      </div>
	    </div>
	<?php endforeach; ?>
			    </div>
			</div>
		    <!-- ------------------------------------- -->
	    </div>

	    

	    <!-- main ends -->
	</div>


</section>