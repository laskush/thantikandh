<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>Add Article</h2>

            <ul class="actions">
                <a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/article'; ?>">List Article </a>
            </ul>

        </div>
        <!-- breadcrumb ends -->
		
		<!-- main content -->
		<div class="row">
		    <!-- ------------------------------------- -->
		    <div class="card">
			    <div class="card-header">
			        <h2><small> Home > Add Article</small>
			        </h2>
			    </div>

			    <div class="card-body card-padding">
			    	<!-- --------------------------------------------------------- -->
				    <?php 
						$action = (isset($article))?'admin/article/updateArticle':'admin/article/addArticle';

						$id = (isset($article))?$article->id:'';
						$title = (isset($article))?$article->title:'';
						$description = (isset($article))?$article->description:'';
						$content = (isset($article))?$article->content:'';
						$image = (isset($article))?$article->image:'';

						echo "<div class='row'>
						    <div class=' col-md-12'>
							<div class='box box-primary'>
								<div class='box-body'>";
						$form_att = array('class' => 'testclass', 'id' => 'addMenu');
						echo form_open_multipart($action, $form_att);

						if(!empty($id)){
							echo "<input type='hidden' name='id' value='".$id."'>";
						}

						echo"<div calss='form-group'>";

						echo form_label('Article Title :'); 
						echo form_input(array('id' => 'title', 'name' => 'title', 'class' => 'form-control input-mask', 'value' =>$title, 'required'=>'required'));
						echo "<br>";

						echo"</div><div calss='form-group'>";

						echo form_label('Short Description :'); 
						echo form_textarea(array('id' => 'desc', 'name' => 'desc', 'class' => 'form-control', 'rows' =>'3', 'value' =>$description, 'required'=>'required'));
						echo "<br>";

						echo form_label('Content :'); 
						echo form_textarea(array('id' => 'content', 'name' => 'content', 'class' => 'form-control tinymce', 'value' =>$content));
						echo "<br>";

						if(isset($article)){
							echo"</div><div calss='form-group'>";
							echo form_label('Image :');
							echo "<div class='row'>
									<div class='col-md-4'>
										<img src = '".base_url()."uploads/images/article/".$image."' class='img-responsive'>
									</div>
								</div>";
					    	echo"</div><br/>";
						}else{
							echo"</div><div calss='form-group'>";
							echo form_label('Image :');
								echo "<input type = 'file' name='image' id='image' class='form-control'>";
						    echo"</div><br/>";
						}

						echo form_submit(array('id' => 'save', 'name' => 'save', 'value' => 'Save', 'class'=>'btn waves-effect bgm-lightblue'));

						echo form_close(); 
						echo "</div>
							</div>
						  </div>
						</div>";
					?>
		    		<!-- ------------------------------------- -->
	    </div>

	    

	    <!-- main ends -->
	</div>


</section>