<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- breadcrumb part -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url(); ?>admin/"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">Add Menu</li>
		</ol>
	</section>
	<!-- breadcrumb ends -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-6 text-left">
				<h3>Add Menu</h3>
			</div>
			<div class="col-md-6 text-right">
				<a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/menu'; ?>">List Menu </a>
			</div>
		</div>

	<?php 
		$action = (isset($menu))?'admin/menu/updateMenu':'admin/menu/addMenu';
		$id = (isset($menu)) ? $menu->id :'';
		$name = (isset($menu)) ? $menu->name :'';
		$linksrc = (isset($menu)) ? $menu->linksrc :'';
		$parentOf = (isset($menu)) ? $menu->parentOf :'';

		echo "<div class='col-md-3'></div>
			<div class=' col-md-6'>
			<div class='box box-primary'>
				<div class='box-body'>";
		$form_att = array('class' => 'testclass', 'id' => 'addMenu');
		echo form_open_multipart($action, $form_att);

		if(!empty($id)){
			echo "<input type='hidden' name='id' value='".$id."'>";
		}

		echo"<div calss='form-group'>";

		echo form_label('Menu Name :'); 
		echo form_input(array('id' => 'name', 'name' => 'name', 'class' => 'form-control', 'value'=>$name, 'required'=>'required'));
		echo "<br>";

		echo"<div calss='form-group'>";

		echo"</div><div calss='form-group'>";

		echo form_label('Link source :'); 
		echo "<select id='linksrc' name='linksrc' class='form-control linksrc-drop'>
				<option value=''>None</option>
  				<optgroup label='Pages'>";
  				foreach ($articles as $article) {
  					$artSel = ($linksrc == 'page/'.$article->slug)?' selected' : '';
  					echo "<option value='page/".$article->slug."' ".$artSel.">".$article->title."</option>";
  				}
	  		echo "</outgroup>
	  			<optgroup label='News'>";
  				foreach ($news as $nwz) {
  					$nwzSel = ($linksrc == 'news/'.$nwz->slug)?' selected' : '';
  					echo "<option value='news/".$nwz->slug."' ".$nwzSel.">".$nwz->title."</option>";
  				}
	  		echo "</outgroup>
	  		</select>
	  		<p id='otherSource' style='color:red; cursor:pointer'>Use other source</p>";

	  	echo form_input(array('id' => 'linksrc', 'name' => 'linksrc', 'class' => 'form-control linksrc-txt', 'placeholder' => 'Path to page Eg. /example', 'value' => $linksrc));
		echo "<p id='otherClose' style='color:red; cursor:pointer'>Close</p><br>";

		echo"</div><div calss='form-group'>";
		echo form_label('Parent :');

		$CI =& get_instance();
	    $CI->load->model('MenuModel'); 
	    $menus= $this->MenuModel->getAllMenu();

	    echo"</div><div calss='form-group'>";
	    echo"<select name='parentOf' class='form-control'>
	    		<option value='0'>none</option>";
	    foreach ($menus as $menu) {
	    		$menuSel = ($parentOf == $menu->id)?' selected' : '';
	    		echo"<option value='".$menu->id."' ".$menuSel.">".$menu->name."</option>";
	    	}
	    echo"</select>
			<br>
			</div>";

		echo"<div calss='form-group'>";

		echo form_submit(array('id' => 'save', 'name' => 'save', 'value' => 'Save', 'class'=>'btn btn-primary'));

		echo form_close(); 
		echo "</div>
			</div>
		  </div>";
	?>
	</section>
	<!-- /.content -->
</div>
<script>
	$(document).ready(function(){
		$(".linksrc-txt").hide();
		$("#otherClose").hide();
		$(".linksrc-txt").attr('disabled', true);
		$("#otherSource").click(function(){
			$(".linksrc-drop").attr('disabled', true);
			$("#otherSource").hide();
			$(".linksrc-txt").show();
			$(".linksrc-txt").attr('disabled', false);
			$("#otherClose").show();
		});
		$("#otherClose").click(function(){
			$(".linksrc-txt").attr('disabled', true);
			$(".linksrc-txt").hide();
			$("#otherClose").hide();
			$(".linksrc-drop").show();
			$(".linksrc-drop").attr('disabled', false);
			$("#otherSource").show();
		})
	})
</script>