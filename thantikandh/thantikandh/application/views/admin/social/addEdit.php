<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>Add Important Number</h2>

            <ul class="actions">
                <a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/article'; ?>">List Numbers</a>
            </ul>

        </div>
        <!-- breadcrumb ends -->
		
		<!-- main content -->
		<div class="row">
		    <!-- ------------------------------------- -->
		    <div class="card">
			    <div class="card-header">
			        <h2><small> Home >Add Important Number</small>
			        </h2>
			    </div>

			    <div class="card-body card-padding">
			    	<!-- --------------------------------------------------------- -->
				    <?php 
		$action = (isset($social))?'admin/social/updateSocial':'admin/social/addSocial';

		$id = (isset($social))?$social->id:'';
		$name = (isset($social))?$social->name:'';
		//$icon = (isset($social))?$social->icon:'';
		$linksrc = (isset($social))?$social->linksrc:'';
		//$image = (isset($social))?$social->logo:'';

		echo "<div class='row'>
		    <div class=' col-md-12'>
			<div class='box box-primary'>
				<div class='box-body'>";
		$form_att = array('class' => 'testclass', 'id' => 'addMenu');
		echo form_open_multipart($action, $form_att);

		if(!empty($id)){
			echo "<input type='hidden' name='id' value='".$id."'>";
		}

		echo"<div class='form-group'>";

		echo form_label('Name :'); 
		echo form_input(array('id' => 'name', 'name' => 'name', 'class' => 'form-control', 'value' =>$name));
		echo "</div><br>";

		/*echo"<div class='form-group'>";
		echo form_label('Fa-icon :'); 
		echo form_input(array('id' => 'icon', 'name' => 'icon', 'class' => 'form-control', 'value' =>$icon));
		echo "</div><br>";*/

		echo"<div class='form-group'>";
		echo form_label('Number :'); 
		echo form_textarea(array('id' => 'linksrc', 'name' => 'linksrc', 'class' => 'form-control', 'rows' =>'1', 'value' =>$linksrc));
		echo "<br>";

		/*echo"</div><div class='form-group'>";
		echo form_label('Logo :');
			echo "<input type = 'file' name='logo' id='logo' class='form-control'>";
	    echo"</div><br/>";*/

		echo form_submit(array('id' => 'save', 'name' => 'save', 'value' => 'Save', 'class'=>'btn waves-effect bgm-lightblue'));

		echo form_close(); 
		echo "</div>
			</div>
		  </div>
		</div>";
	?>
		    		<!-- ------------------------------------- -->
	    </div>

	    

	    <!-- main ends -->
	</div>


</section>