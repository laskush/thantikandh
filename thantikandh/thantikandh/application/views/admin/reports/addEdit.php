<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>Add Reports</h2>

            <ul class="actions">
                <a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/article'; ?>">List Reports</a>
            </ul>

        </div>
        <!-- breadcrumb ends -->
		
		<!-- main content -->
		<div class="row">
		    <!-- ------------------------------------- -->
		    <div class="card">
			    <div class="card-header">
			        <h2><small> Home >Add Reports</small>
			        </h2>
			    </div>

			    <div class="card-body card-padding">
			    	<!-- --------------------------------------------------------- -->
				    <?php 
		$action = (isset($reports))?'admin/reports/updateReports':'admin/reports/addReports';

		$id = (isset($reports))?$reports->id:'';
		$title = (isset($reports))?$reports->title:'';
		$description = (isset($reports))?$reports->description:'';
		$content = (isset($reports))?$reports->content:'';
		$image = (isset($reports))?$reports->image:'';
		$image2 = (isset($reports))?$reports->image2:'';
		$image3 = (isset($reports))?$reports->image3:'';
		$st_date = (isset($reports))?$reports->st_date:'';
		//$end_date = (isset($reports))?$reports->end_date:'';
		$category = (isset($reports))?$reports->category:'';

		echo "<div class='row'>
		    <div class=' col-md-12'>
			<div class='box box-primary'>
				<div class='box-body'>";
		$form_att = array('class' => 'testclass', 'id' => 'addMenu');
		echo form_open_multipart($action, $form_att);

		if(!empty($id)){
			echo "<input type='hidden' name='id' value='".$id."'>";
		}

		echo"<div class='form-group'>";

		echo form_label('Reports Title :'); 
		echo form_input(array('id' => 'title', 'name' => 'title', 'class' => 'form-control', 'value' =>$title, 'required'=>'required'));
		echo "</div><br>";

		/*echo "<div class='row'>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('Start Date :')."
						".form_input(array('id' => 'st_date', 'name' => 'st_date', 'class' => 'form-control datepick', 'value' =>$st_date))."
					</div>
				</div>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('End Date :')."
						".form_input(array('id' => 'dend_ate', 'name' => 'end_date', 'class' => 'form-control datepick', 'value' =>$end_date))."
					</div>
				</div>
			</div>";*/

		echo "<div class='row'>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('Reports Date :')."
						".form_input(array('id' => 'st_date', 'name' => 'st_date', 'class' => 'form-control date-picker', 'value' =>$st_date))."
					</div>
				</div>
			</div>";

		echo "<input type='hidden' name='end_date' value='0000-00-00'>";
		echo"<div class='form-group'>";

		/*$noSel = ($category == 'notice')?'selected':'';
		$prSel = ($category == 'press-release')?'selected':'';
		$pSel = ($category == 'programs')?'selected':'';*/

		echo form_label('Category :'); 
		echo "<select name='cate' class='btn dropdown-toggle btn-default'>";
		foreach ($cate as $key) {
			$nSel = ($category == $key->slug)?'selected':'';
			echo "<option value='".$key->slug."' ".$nSel.">".$key->title."</option>";
		}
		echo "</select>
			</div><br>";

		echo"<div class='form-group'>";

		echo form_label('Short Description :'); 
		echo form_textarea(array('id' => 'desc', 'name' => 'desc', 'class' => 'form-control', 'rows' =>'3', 'value' =>$description, 'required'=>'required'));
		echo "<br>";

		echo form_label('Content :'); 
		echo form_textarea(array('id' => 'content', 'name' => 'content', 'class' => 'form-control tinymce', 'value' =>$content));
		echo "<br>";

		if(!empty($image)){
			echo"<div class='form-group'>";
			echo form_label('File One:');
			echo "<div class='row'>
					<div class='col-md-6'>
						<input type='hidden' name='img1' value='".$image."'>
						<a href = '".base_url()."uploads/images/reports/".$image."' target='blank'>File One</a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a class='btn btn-xs btn-danger delFile' data-target='image' data-link='reports' data-id='".$id."' id='reports-".$id."' title='delete'><i class='fa fa-trash'></i></a>
					</div>
				</div>";
	    	echo"</div><br/>";
		}else{
			echo"<div class='form-group'>";
			echo form_label('File One :');
				echo "<input type = 'file' name='image' id='image' class='form-control'>";
		    echo"</div><br/>";
		}

		if(!empty($image2)){
			echo"<div class='form-group'>";
			echo form_label('File Two:');
			echo "<div class='row'>
					<div class='col-md-6'>
						<input type='hidden' name='img2' value='".$image2."'>
						<a href = '".base_url()."uploads/images/reports/".$image2."' target='blank'>File Two</a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a class='btn btn-xs btn-danger delFile' data-target='image2' data-link='reports' data-id='".$id."' id='reports-".$id."' title='delete'><i class='fa fa-trash'></i></a>
					</div>
				</div>";
	    	echo"</div><br/>";
		}else{
			echo"<div class='form-group'>";
			echo form_label('File Two :');
			echo "<input type = 'file' name='image2' id='image2' class='form-control'>";
		    echo"</div><br/>";
		}

		if(!empty($image3)){
			echo"<div class='form-group'>";
			echo form_label('File Three:');
			echo "<div class='row'>
					<div class='col-md-6'>
						<input type='hidden' name='img3' value='".$image3."'>
						<a href = '".base_url()."uploads/images/reports/".$image3."' target='blank'>File Three</a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a class='btn btn-xs btn-danger delFile' data-target='image3' data-link='reports' data-id='".$id."' id='reports-".$id."' title='delete'><i class='fa fa-trash'></i></a>
					</div>
				</div>";
	    	echo"</div><br/>";
		}else{
			echo"<div class='form-group'>";
			echo form_label('File Three :');
				echo "<input type = 'file' name='image3' id='image3' class='form-control'>";
		    echo"</div><br/>";
		}

		echo form_submit(array('id' => 'save', 'name' => 'save', 'value' => 'Save', 'class'=>'btn waves-effect bgm-lightblue'));

		echo form_close(); 
		echo "</div>
			</div>
		  </div>
		</div>";
	?>
		    		<!-- ------------------------------------- -->
	    </div>

	    

	    <!-- main ends -->
	</div>


</section>