<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>User Details</h2>

            <!-- <ul class="actions">
                <a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/article'; ?>">List Article </a>
            </ul> -->

        </div>
        <!-- breadcrumb ends -->
		
		<!-- main content -->
		<div class="row">
		    <!-- ------------------------------------- -->
		    <div class="card">
			    <div class="card-header">
			        <h2><small> Home >User Details</small>
			        </h2>
			    </div>

			    <div class="card-body card-padding">
			    	<!-- --------------------------------------------------------- -->
				    <?php  	
		$action = 'admin/user/update';

		$id = $user->id;
		$fullname = $user->fullname;
		$username = $user->username;
		$email = $user->email;

		echo "<div class='row'>
		    <div class=' col-md-12'>
			<div class='box box-primary'>
				<div class='box-body'>";
		$form_att = array('class' => 'testclass', 'id' => 'addMenu');
		echo form_open_multipart($action, $form_att);

		if(!empty($id)){
			echo "<input type='hidden' name='id' value='".$id."'>";
		}

		echo"<div class='row'><div class='col-md-6 col-xs-12'>";
		echo"<div class='form-group'>";
		echo form_label('Fullname :'); 
		echo form_input(array('id' => 'name', 'name' => 'fullname', 'class' => 'form-control', 'value' =>$fullname));
		echo "</div></div>";

		echo"<div class='col-md-6 col-xs-12'>";
		echo"<div class='form-group'>";
		echo form_label('Username :'); 
		echo '<span class="form-control">'.$username.'</span>';
		echo "</div></div></div>";

		echo"<div class='form-group'>";
		echo form_label('Email :'); 
		echo form_input(array('id' => 'email', 'name' => 'email', 'class' => 'form-control', 'value' =>$email));
		echo "</div>";

		echo"<div class='form-group'>";
		echo form_label('Old Password :'); 
		echo form_password(array('id' => 'oldPass', 'name' => 'oldPass', 'class' => 'form-control', 'required'=>'required'));
		echo "</div>";

		echo"<div class='form-group'>";
		echo form_label('New Password :'); 
		echo form_password(array('id' => 'newPass', 'name' => 'password', 'class' => 'form-control', 'required'=>'required'));
		echo "</div>";

		echo"<div class='form-group'>";
		echo form_label('Confirm Password :'); 
		echo form_password(array('id' => 'confirmPass', 'name' => 'confirmPass', 'class' => 'form-control', 'required'=>'required'));
		echo "</div>
		<div id='errpsw' style='color:red'></div>";

		echo form_submit(array('id' => 'save', 'name' => 'save', 'value' => 'Update', 'class'=>'btn waves-effect bgm-lightblue'));

		echo form_close(); 
		echo "</div>
			</div>
		  </div>
		</div>";
	?>
		    		<!-- ------------------------------------- -->
	    </div>

	    

	    <!-- main ends -->
	</div>


</section>