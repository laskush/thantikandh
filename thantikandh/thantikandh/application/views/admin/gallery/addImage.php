<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- breadcrumb part -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url(); ?>admin/"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">Gallery</li>
		</ol>
	</section>
	<!-- breadcrumb ends -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-6 text-left">
				<h3>Add Images</h3>
			</div>
			<div class="col-md-6 text-right">
				<a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/gallery'; ?>">View Images </a>
			</div>
		</div>
  	
  		<div class="row">
  			<div class="col-md-6">
  				<div id="fileuploader">Upload</div>
  			</div>
  		</div>

	</section>
	<!-- /.content -->
</div>

<script>
	$(document).ready(function(){
	    $("#fileuploader").uploadFile({
	      url:"<?php echo base_url() . 'admin/gallery/addImage'; ?>",
	      formData: { cat_id: '<?php echo($cat_id); ?>'},
	      fileName:"myfile",
	      showStatusAfterSuccess: true,
	      showPreview: true,
	      showDone: true,
	      showProgress: true
	    });
	});
</script>
