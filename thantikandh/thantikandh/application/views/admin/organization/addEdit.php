<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>Add Organization</h2>

            <ul class="actions">
                <a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/article'; ?>">List Organization</a>
            </ul>

        </div>
        <!-- breadcrumb ends -->
		
		<!-- main content -->
		<div class="row">
		    <!-- ------------------------------------- -->
		    <div class="card">
			    <div class="card-header">
			        <h2><small> Home >Add Organization</small>
			        </h2>
			    </div>

			    <div class="card-body card-padding">
			    	<!-- --------------------------------------------------------- -->
				    <?php 
		$action = (isset($organization))?'admin/organization/updateOrganization':'admin/organization/addOrganization';

		$id = (isset($organization))?$organization->id:'';
		$name = (isset($organization))?$organization->name:'';
		$description = (isset($organization))?$organization->description:'';
		$address = (isset($organization))?$organization->address:'';
		$contact = (isset($organization))?$organization->contact:'';
		$website = (isset($organization))?$organization->website:'';
		$image = (isset($organization))?$organization->logo:'';

		echo "<div class='row'>
		    <div class=' col-md-12'>
			<div class='box box-primary'>
				<div class='box-body'>";
		$form_att = array('class' => 'testclass', 'id' => 'addMenu');
		echo form_open_multipart($action, $form_att);

		if(!empty($id)){
			echo "<input type='hidden' name='id' value='".$id."'>";
		}

		echo"<div class='form-group'>";

		echo form_label('Organization Name :'); 
		echo form_input(array('id' => 'name', 'name' => 'name', 'class' => 'form-control', 'value' =>$name));
		echo "</div><br>";

		echo"<div class='form-group'>";

		echo form_label('Address :'); 
		echo form_input(array('id' => 'address', 'name' => 'address', 'class' => 'form-control', 'value' =>$address));
		echo "</div><br>";

		echo "<div class='row'>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('Contact Details :')."
						".form_input(array('id' => 'contact', 'name' => 'contact', 'class' => 'form-control', 'value' =>$contact))."
					</div>
				</div>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('Website :')."
						".form_input(array('id' => 'website', 'name' => 'website', 'class' => 'form-control', 'value' =>$website))."
					</div>
				</div>
			</div>";

		echo"<div class='form-group'>";
		echo form_label('Description :'); 
		echo form_textarea(array('id' => 'desc', 'name' => 'desc', 'class' => 'form-control tinymce', 'rows' =>'5', 'value' =>$description));

		if(isset($organization)){
			echo"</div><div calss='form-group'>";
			echo form_label('Image :');
			echo "<div class='row'>
					<div class='col-md-4'>
						<img src = '".base_url()."uploads/images/organization/".$image."' class='img-responsive'>
					</div>
				</div>";
	    	echo"</div><br/>";
		}else{
			echo"</div><div calss='form-group'>";
			echo form_label('Image :');
				echo "<input type = 'file' name='logo' id='logo' class='form-control'>";
		    echo"</div><br/>";
		}

		echo form_submit(array('id' => 'save', 'name' => 'save', 'value' => 'Save', 'class'=>'btn waves-effect bgm-lightblue'));

		echo form_close(); 
		echo "</div>
			</div>
		  </div>
		</div>";
	?>
		    		<!-- ------------------------------------- -->
	    </div>

	    

	    <!-- main ends -->
	</div>


</section>