<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>Add Member</h2>

            <ul class="actions">
                <a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/organization'; ?>">List Members</a>
            </ul>

        </div>
        <!-- breadcrumb ends -->
		
		<!-- main content -->
		<div class="row">
		    <!-- ------------------------------------- -->
		    <div class="card">
			    <div class="card-header">
			        <h2><small> Home >Add Member</small>
			        </h2>
			    </div>

			    <div class="card-body card-padding">
			    	<!-- --------------------------------------------------------- -->
				    <?php 
		$action = (isset($member))?'admin/organization/updateMember':'admin/organization/addMember';

		$id = (isset($member))?$member->id:'';
		$name = (isset($member))?$member->name:'';
		$description = (isset($member))?$member->description:'';
		$address = (isset($member))?$member->address:'';
		$contact = (isset($member))?$member->contact:'';
		$email = (isset($member))?$member->email:'';
		$oid = (isset($member))?$member->o_id:'';
		$image = (isset($member))?$member->image:'';

		echo "<div class='row'>
		    <div class=' col-md-12'>
			<div class='box box-primary'>
				<div class='box-body'>";
		$form_att = array('class' => 'testclass', 'id' => 'addMenu');
		echo form_open_multipart($action, $form_att);

		if(!empty($id)){
			echo "<input type='hidden' name='id' value='".$id."'>";
		}

		echo"<div class='form-group'>";

		echo form_label('Member Name :'); 
		echo form_input(array('id' => 'name', 'name' => 'name', 'class' => 'form-control', 'value' =>$name, 'required' => 'required'));
		echo "</div><br>";

		echo"<div class='form-group'>";

		echo form_label('Designation :'); 
		echo form_input(array('id' => 'desc', 'name' => 'desc', 'class' => 'form-control', 'value' =>$description));
		echo "</div><br>";

		echo"<div class='form-group'>";

		echo form_label('Category :'); 
		echo "<select name='cate' class='btn dropdown-toggle btn-default'>";
		foreach ($cate as $key) {
			$nSel = ($oid == $key->id)?'selected':'';
			echo "<option value='".$key->id."' ".$nSel.">".$key->name."</option>";
		}
		echo "</select>
			</div><br>";

		echo"<div class='form-group'>";

		echo form_label('Address :'); 
		echo form_input(array('id' => 'address', 'name' => 'address', 'class' => 'form-control', 'value' =>$address));
		echo "</div><br>";

		echo "<div class='row'>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('Contact Details :')."
						".form_input(array('id' => 'contact', 'name' => 'contact', 'class' => 'form-control', 'value' =>$contact))."
					</div>
				</div>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('Email Address :')."
						".form_input(array('id' => 'email', 'name' => 'email', 'class' => 'form-control', 'value' =>$email))."
					</div>
				</div>
			</div>";

		if(!empty($member->image)){
			echo"<div calss='form-group'>";
			echo form_label('Image :');
			echo "<div class='row'>
					<div class='col-md-4'>
						<img src = '".base_url()."uploads/images/organization/member/".$image."' class='img-responsive'>
					</div>
				</div>";
	    	echo"<br/>";
		}else{
			echo"<div calss='form-group'>";
			echo form_label('Image :');
				echo "<input type = 'file' name='logo' id='logo' class='form-control'>";
		    echo"<br/>";
		}

		echo form_submit(array('id' => 'save', 'name' => 'save', 'value' => 'Save', 'class'=>'btn waves-effect bgm-lightblue'));

		echo form_close(); 
		echo "</div>
			</div>
		  </div>
		</div>";
	?>
		    		<!-- ------------------------------------- -->
	    </div>

	    

	    <!-- main ends -->
	</div>


</section>