<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>Organization's Member List</h2>

            <ul class="actions">
                <a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/organization/addMember'; ?>">Add</a>
            </ul>

        </div>
        <!-- breadcrumb ends -->
		
		<!-- main content -->
		<div class="row">
		    <!-- ------------------------------------- -->
		    <div class="card">
			    <div class="card-header">
			        <h2><small> Home > Organization's Member List</small>
			        </h2>
			    </div>

			    <div class="card-body card-padding">
			    	<!-- --------------------------------------------------------- -->
				    <div class="panel-group" id="accordion">
					  	<div class="panel panel-default">
					      <div class="panel-heading" style="height: 45px; background: #6eaeb9;">
					        <div class="panel-title">
					        	<a href="#content" data-toggle="collapse" data-parent="#accordion">
					        		<div class="col-md-4 col-xs-2">
							          	Name
						        	</div>
						        	<div class="col-md-3 col-xs-6">
							          	Description
						        	</div>
						        	<div class="col-md-3 hidden-xs">
							          	Contact
						        	</div>
						        </a>
						        <div class="col-md-2 col-xs-4 text-center">
						        	Action
						        </div>
					        </div>
					      </div>
					    </div>
					    <?php foreach ($members as $ind): ?>
					    	<div class="panel panel-default">
						      <div class="panel-heading" style="height: 45px; background: #a7d1d8;">
						        <div class="panel-title">
						        	<a href="<?php echo base_url() . 'admin/organization/editMember/' . $ind->id; ?>">
						        		<div class="col-md-4 col-xs-2">
											<?php echo $ind->name;?>
							        	</div>
							        	<div class="col-md-3 col-xs-6">
								          	<?php echo $ind->description; ?>
							        	</div>
							        	<div class="col-md-3 hidden-xs">
								          	<?php echo $ind->contact; ?>
							        	</div>
							        </a>
							        <div class="col-md-2 col-xs-4 text-center" style="margin-top:-10px">
							        	<a class="btn btn-xs btn-primary" href="<?php echo base_url() . 'admin/organization/editMember/' . $ind->id; ?>"><i class="fa fa-eye"></i></a>
							        	<a class="btn btn-xs btn-danger" href="<?php echo base_url() . 'admin/organization/deleteMember/' . $ind->id; ?>"><i class="fa fa-trash"></i></a>
							        </div>
						        </div>
						      </div>
						    </div>
						<?php endforeach; ?>
			    </div>
			</div>
		    <!-- ------------------------------------- -->
	    </div>

	    

	    <!-- main ends -->
	</div>


</section>