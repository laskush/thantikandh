<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>Program Categories List</h2>

            <ul class="actions">
                <a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/plan/addPlan'; ?>">Add Program </a>
            </ul>

        </div>
        <!-- breadcrumb ends -->
		
		<!-- main content -->
		<div class="row">
		    <!-- ------------------------------------- -->
		    <div class="card">
			    <div class="card-header">
			        <h2><small> Home > Program List</small>
			        </h2>
			    </div>

			    <div class="card-body card-padding">
			    	<!-- --------------------------------------------------------- -->
				    <div class="panel-group" id="accordion">

					    <?php foreach ($programs as $ind): ?>
					    	<div class="panel panel-default">
						      <div class="panel-heading" style="height: 45px; background: #a7d1d8;">
						        <div class="panel-title">
						        	<a href="<?php echo base_url() . 'admin/plan/ViewPlan/' . $ind->slug; ?>">
						        		<div class="col-md-4 col-xs-8">
								          	<?php echo $ind->title; ?>
							        	</div>
							        </a>
							        <div class="col-md-2 col-xs-4 text-center">
							        	-
							        </div>
						        </div>
						      </div>
						    </div>
						<?php endforeach; ?>
					</div>
				    <!-- --------------------------------------------------------- -->
			    </div>
			</div>

	    
		</div>
	    <!-- main ends -->
	</div>


</section>