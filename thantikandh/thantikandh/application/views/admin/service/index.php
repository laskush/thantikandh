<style>
	.panel-collapse .panel-heading .panel-title > a::after, .panel-collapse .panel-heading .panel-title > a::before, .panel-collapse .panel-heading.active::after, .panel-collapse .panel-heading::before {
			display: none !important;
	}
</style>
<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>Article List</h2>

            <ul class="actions">
                <a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/service/addService'; ?>">Add Categories </a>
            </ul>

        </div>
        <!-- breadcrumb ends -->
		
		<!-- main content -->
		<div class="row">
		    <!-- ------------------------------------- -->
		    <div class="card">
			    <div class="card-header">
			        <h2><small> Home > Category List</small>
			        </h2>
			    </div>

			    <div class="card-body card-padding">
			    	<!-- --------------------------------------------------------- -->
			    	<?php
			    		$cateArr = array('News'=>'news','Programs'=>'programs','Reports'=>'reports','Bidutiya Susasan'=>'bidutiya','Members'=>'members','Board Members'=>'o-members');
			    		echo '<div id="accordion" class="panel-group">';
			    		$i = 1;
			    		foreach ($cateArr as $key => $cate) {
			    			$active = ($i==1)?'active':'';
			    			$in = ($i==1)?'in':'';
			    			$col = ($i!=1)?'collapsed':'';
			    			echo '<div class="panel panel-default">
							      <div class="panel-heading '.$active.'" style="background:#a7d1d8">
							      <h4 class="panel-title">
							        <a href="#p-'.$cate.'" class="accordion-toggle '.$col.'" data-toggle="collapse" data-parent="#accordion">'.$key.'</a>
							        </h4>
							      </div>
							      <div id="p-'.$cate.'" class="panel-collapse collapse '.$in.'">
							      <div class="panel-body" style="background: #efefef; padding:10px">';
							/*------------------------------------------*/
							foreach ($services as $service): 
								if($service->type == $cate){ ?>

									<div class="panel panel-default">
								      <div class="panel-heading" style="height: 35px; background: #59b3fb;">
								        <div class="panel-title">
								        	<a href="<?php echo base_url() . 'admin/service/editService/' . $service->id; ?>">
								        		<div class="col-md-6 col-xs 8">
										          	<?php echo $service->title; ?>
									        	</div>
									        	<div class="col-md-3  col-xs-8">
										          	<?php echo $service->type; ?>
									        	</div>
									        </a>
									        <div class="col-md-2 col-xs-4 text-center" style="margin-top: -20px;">
									        	<a class="btn btn-xs btn-primary" href="<?php echo base_url() . 'admin/service/editService/' . $service->id; ?>"><i class="fa fa-eye"></i></a>
									        	<?php 
									        		if($service->status == 1){
									        			echo '<a class="btn btn-xs btn-success publishToggle" data-target="service" data-link="service" data-id="'.$service->id.'" id="service-'.$service->id.'" title="published"><i class="fa fa-flag"></i></a>';
									        		}else{
									        			echo '<a class="btn btn-xs btn-danger publishToggle" data-target="service" data-link="service" data-id="'.$service->id.'" id="service-'.$service->id.'" title="unpublished"><i class="fa fa-flag"></i></a>';
									        		}
									        	?>
									        	<a class="btn btn-xs btn-danger" href="<?php echo base_url() . 'admin/service/deleteService/' . $service->id; ?>"><i class="fa fa-trash"></i></a>
									        </div>
								        </div>
								      </div>
								    </div>

							<?php }
						    	
								endforeach;


							/*-------------------------------------------*/
							          
							echo'</div>
							      </div>
							    </div>';
							$i++;
			    		}    
						echo '</div>';
			    	?>
					  	
			    </div>
			</div>
		    <!-- ------------------------------------- -->
	    </div>

	    

	    <!-- main ends -->
	</div>


</section>