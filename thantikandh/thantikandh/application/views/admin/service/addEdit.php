<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>Add Categories</h2>

            <ul class="actions">
                <a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/service'; ?>">List Categories </a>
            </ul>

        </div>
        <!-- breadcrumb ends -->
		
		<!-- main content -->
		<div class="row">
		    <!-- ------------------------------------- -->
		    <div class="card">
			    <div class="card-header">
			        <h2><small> Home >Add Categories</small>
			        </h2>
			    </div>

			    <div class="card-body card-padding">
			    	<!-- --------------------------------------------------------- -->
				    <?php 
						$action = (isset($service))?'admin/service/updateService':'admin/service/addService';

						$id = (isset($service))?$service->id:'';
						$name = (isset($service))?$service->title:'';
						$description = (isset($service))?$service->type:'';

						echo "<div class='row'>
						    <div class=' col-md-12'>
							<div class='box box-primary'>
								<div class='box-body'>";
						$form_att = array('class' => 'testclass', 'id' => 'addMenu');
						echo form_open_multipart($action, $form_att);

						if(!empty($id)){
							echo "<input type='hidden' name='id' value='".$id."'>";
						}

						echo"<div calss='form-group'>";

						echo form_label('Service Name :'); 
						echo form_input(array('id' => 'name', 'name' => 'name', 'class' => 'form-control', 'value' =>$name, 'required'=>'required'));
						echo "<br>";

						echo"</div><div calss='form-group'>";

						echo form_label('Category :'); 
						//echo form_textarea(array('id' => 'description', 'name' => 'description', 'class' => 'form-control', 'rows' =>'3', 'value' =>$description, 'required'=>'required'));
						echo "<select name='description' class='btn dropdown-toggle btn-default'>";

							$nSel = ($description == 'news')?'selected':'';
							$mSel = ($description == 'members')?'selected':'';
							$rSel = ($description == 'reports')?'selected':'';
							$oSel = ($description == 'o-members')?'selected':'';
							$pSel = ($description == 'programs')?'selected':'';
							$bSel = ($description == 'bidutiya')?'selected':'';
							echo "<option value='news' ".$nSel.">News</option>";
							echo "<option value='reports' ".$rSel.">Reports</option>";
							echo "<option value='programs' ".$pSel.">Programs</option>";
							echo "<option value='bidutiya' ".$bSel.">Bidutiya Susasan</option>";
							echo "<option value='members' ".$mSel.">Members</option>";
							echo "<option value='o-members' ".$oSel.">Board Members</option>";

						echo "</select><br><br>";

						echo form_submit(array('id' => 'save', 'name' => 'save', 'value' => 'Save', 'class'=>'btn waves-effect bgm-lightblue'));

						echo form_close(); 
						echo "</div>
							</div>
						  </div>
						</div>";
					?>
		    		<!-- ------------------------------------- -->
	    </div>

	    

	    <!-- main ends -->
	</div>


</section>