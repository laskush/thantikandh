<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Notifiction Test</title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <h2>Test For Notification</h2>







<script src="https://www.gstatic.com/firebasejs/5.0.3/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyDhZkugHhPOocNX0q5rj0Z9TR_0WqguxrQ",
    authDomain: "badayatal-nagarpalika.firebaseapp.com",
    databaseURL: "https://badayatal-nagarpalika.firebaseio.com",
    projectId: "badayatal-nagarpalika",
    storageBucket: "badayatal-nagarpalika.appspot.com",
    messagingSenderId: "1075941332848"
  };
  firebase.initializeApp(config);

  var messaging = firebase.messaging();

// On load register service worker
if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('http://localhost/nagarpalika/assets/js/firebase-messaging-sw.js').then((registration) => {
      // Successfully registers service worker
      console.log('ServiceWorker registration successful with scope: ', registration.scope);
      messaging.useServiceWorker(registration);
    })
    .then(() => {
      // Requests user browser permission
      return messaging.requestPermission();
    })
    .then(() => {
      // Gets token
      return messaging.getToken();
    })
    .then((token) => {
      // Simple ajax call to send user token to server for saving
      $.ajax({
        type: 'POST',
        url: '/api/setToken',
        dataType: 'json',
        data: JSON.stringify({token: token}),
        contentType: 'application/json',
        success: (data) => {
          console.log('Success ', data);
        },
        error: (err) => {
          console.log('Error ', err);
        }
      })
    })
    .catch((err) => {
      console.log('ServiceWorker registration failed: ', err);
    });
  });
}
</script>


</body>
</html>
