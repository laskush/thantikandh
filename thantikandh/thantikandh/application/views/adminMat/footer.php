</section>
<footer id="footer">
    Copyright &copy; 2018 Thantikandh Municipality
</footer>

<!-- Page Loader -->
<div class="page-loader">
    <div class="preloader pls-blue">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p>Please wait...</p>
    </div>
</div>
<style>
    #footer {
        height: 45px;
        padding-top: 15px;
        padding-bottom: 15px;
    }
</style>

<!-- Older IE warning message -->
<!--[if lt IE 9]>
    <div class="ie-warning">
        <h1 class="c-white">Warning!!</h1>
        <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
        <div class="iew-container">
            <ul class="iew-download">
                <li>
                    <a href="http://www.google.com/chrome/">
                        <img src="img/browsers/chrome.png" alt="">
                        <div>Chrome</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.mozilla.org/en-US/firefox/new/">
                        <img src="img/browsers/firefox.png" alt="">
                        <div>Firefox</div>
                    </a>
                </li>
                <li>
                    <a href="http://www.opera.com">
                        <img src="img/browsers/opera.png" alt="">
                        <div>Opera</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.apple.com/safari/">
                        <img src="img/browsers/safari.png" alt="">
                        <div>Safari</div>
                    </a>
                </li>
                <li>
                    <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                        <img src="img/browsers/ie.png" alt="">
                        <div>IE (New)</div>
                    </a>
                </li>
            </ul>
        </div>
        <p>Sorry for the inconvenience!</p>
    </div>
<![endif]-->

<!-- Javascript Libraries -->
<script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- <script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/bower_components/flot/jquery.flot.js"></script>
<script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/bower_components/flot/jquery.flot.resize.js"></script>
<script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
<script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/sparklines/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script> -->

<script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/bower_components/moment/min/moment.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script> -->
<!-- <script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script> -->
<script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/bower_components/Waves/dist/waves.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>


<!-- Placeholder for IE9 -->
<!--[if IE 9 ]>
    <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
<![endif]-->

<script src="<?php echo base_url(); ?>assets/adminMaterial/js/app.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/script.js"></script>

<script src="<?php echo base_url(); ?>assets/adminMaterial/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script>
    $(function(){
    
    $('.datepick').datepicker({
        dateFormat: 'yy-mm-dd',
        altFormat: 'yy-mm-dd'
    });
    
  });
</script>
 <script type="text/javascript" src="<?php echo base_url(); ?>assets/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/tinymce/js/tinymce/tinymce.min.js"></script>

    <script type="text/javascript">
      tinymce.init({
        selector: "textarea.tinymce",theme: "modern",height: 300,
        plugins: [
             "advlist autolink link image lists charmap print preview hr anchor pagebreak",
             "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
             "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
       ],
       toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
       toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
       image_advtab: true ,
       
       external_filemanager_path:"<?php echo base_url() ?>assets/filemanager/",
       filemanager_title:"Responsive Filemanager" ,
       external_plugins: { "filemanager" : "<?php echo base_url() ?>assets/tinymce/js/tinymce/plugins/responsivefilemanager/plugin.min.js"}
     });
    </script>

    
</body>

<!-- Mirrored from byrushan.com/projects/ma/1-7-1/jquery/light/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Jun 2018 07:41:22 GMT -->
</html>
