 <?php
  if($this->session->has_userdata('fullname')){
    $name = $this->session->userdata('fullname');
  }
  if($this->session->has_userdata('c_name')){
    $name = $this->session->userdata('c_name');
  }
  $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
  $uri_segments = explode('/', $uri_path);
  $user = $uri_segments[2] == "member"?"member":"admin";
?>
 <section id="main">
    <aside id="sidebar" class="sidebar c-overflow">
        <div class="s-profile">
            <a href="#" data-ma-action="profile-menu-toggle">
                <div class="sp-pic">
                    <img src="<?php echo base_url(); ?>assets/adminMaterial/img/demo/profile-pics/1.jpg" alt="">
                    <!-- <img src="<?php echo base_url(); ?>assets/adminMaterial/img/NepalGovt.png" alt=""> -->

                </div>

                <div class="sp-info">
                    <?php echo $name ?>

                    <i class="zmdi zmdi-caret-down"></i>
                </div>
            </a>

            <ul class="main-menu">
                <li>
                    <a href="#"><i class="zmdi zmdi-account"></i> View Profile</a>
                </li>
                <li>
                    <a href="#"><i class="zmdi zmdi-input-antenna"></i> Privacy Settings</a>
                </li>
                <li>
                    <a href="#"><i class="zmdi zmdi-settings"></i> Settings</a>
                </li>
                <li>
                    <a href="<?php echo base_url() .$user. "/auth/logout"; ?>"><i class="zmdi zmdi-time-restore"></i> Logout</a>
                </li>
            </ul>
        </div>

        <ul class="main-menu">
            <?php
               $CI =& get_instance();
               $mod = $CI->load->model('ModuleModel');
               $modules = $CI->ModuleModel->getModule();
               if(!empty($modules)) {
                foreach ($modules as $module) {
                  echo '<li class="">
                            <a href="'.base_url().$module->link.'">
                                <i class="zmdi '.$module->icon.'"></i> '.$module->name.'
                            </a>
                        </li>';
                }
               } else {
                    echo "No Data";
               }
            ?>
        </ul>
    </aside>