<?php
  if($this->session->has_userdata('fullname')){
    $name = $this->session->userdata('fullname');
  }
  if($this->session->has_userdata('c_name')){
    $name = $this->session->userdata('c_name');
  }
?>
<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar fix-side">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/adminLTE/image/avatar2.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $name ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN MENU</li>

        <?php
           $CI =& get_instance();
           $mod = $CI->load->model('ModuleModel');
           $modules = $CI->ModuleModel->getModule();
           if(!empty($modules)) {
            foreach ($modules as $module) {
              echo '<li>
                      <a href="'.base_url().$module->link.'">
                        <i class="fa '.$module->icon.'"></i> <span>'.$module->name.'</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                    </li>';
            }
           } else {
                echo "No Data";
           }
        ?>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>