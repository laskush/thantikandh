<?php
  if($this->session->has_userdata('fullname')){
    $name = $this->session->userdata('fullname');
  }
  if($this->session->has_userdata('mem_fullname')){
    $name = $this->session->userdata('mem_fullname');
  }
?>
<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar fix-side">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/adminLTE/image/avatar2.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $name; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN MENU</li>

        <li>
          <a href="<?php echo base_url(); ?>member">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>

        <li>
          <a href="<?php echo base_url(); ?>member/profile">
            <i class="fa fa-gift"></i> <span>Profile</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>

        <li>
          <a href="<?php echo base_url(); ?>member/files">
            <i class="fa fa-file"></i> <span>Files</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>