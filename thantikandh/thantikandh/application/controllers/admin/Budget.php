<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Budget extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$user_browser = $_SERVER['HTTP_USER_AGENT'];
		$token = hash('sha512', $this->session->userdata('username') . $user_browser);

		if($this->session->userdata('status')!="success" && $this->session->userdata('token')!=$token){	

			redirect('/admin/auth/login', 'refresh');
			
		}
	}

	public function index()
	{
		$data['budget']= $this->BudgetModel->getBudget();
		$this->load->view('adminLTE/header');
		$this->load->view('adminLTE/sidebar');
		$this->load->view('admin/budget/index', $data);
		$this->load->view('adminLTE/footer');
	}
	
	public function addBudget()
	{
		if($this->input->post('save')){

			$parti = $this->input->post('particular');
			$price = $this->input->post('price'); 

			$particularArr = array();
			for($i=0; $i< count($parti); $i++){
				$particularArr[$parti[$i]] = $price[$i];
			}

			$particular = serialize($particularArr);
			

			$userData = array(
				'title' => $this->input->post('title'),
				'date' => $this->input->post('date'),
				'description' => $this->input->post('desc'),
				'amount' => $this->input->post('amount'),
				'particular' => $particular
			);

			$this->BudgetModel->insertBudget($userData);
			redirect('/admin/budget', 'refresh');

		}else{
			$this->load->view('adminLTE/header');
			$this->load->view('adminLTE/sidebar');
			$this->load->view('admin/budget/addEdit');
			$this->load->view('adminLTE/footer');
		}
	}

	public function editBudget($id){
		$data['budget']=$this->BudgetModel->getById($id);
		
		//Form for adding user data
		$this->load->view('adminLTE/header');
		$this->load->view('adminLTE/sidebar');
		$this->load->view('admin/budget/addEdit', $data);
		$this->load->view('adminLTE/footer');
	}

	public function updateBudget(){
		if($this->input->post('save')){

			$parti = $this->input->post('particular');
			$price = $this->input->post('price'); 

			$particularArr = array();
			for($i=0; $i< count($parti); $i++){
				$particularArr[$parti[$i]] = $price[$i];
			}

			$particular = serialize($particularArr);

			$userData = array(
				'id' => $this->input->post('id'),
				'title' => $this->input->post('title'),
				'date' => $this->input->post('date'),
				'description' => $this->input->post('desc'),
				'amount' => $this->input->post('amount'),
				'particular' => $particular
			);

			//Pass user data to model
			$this->BudgetModel->update($userData);
			redirect('/admin/budget', 'refresh');
		}
	}

	public function deleteBudget($id){
		$data = $this->BudgetModel->delete($id);
		redirect('/admin/budget', 'refresh');
	}

	public function publishToggle(){
		$id = $this->input->post('id');
		$status = $this->BudgetModel->publish($id);
		$response['status'] = 'success';
		$response['message'] = $status;
		echo json_encode($response);
	}
}
