<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slideshow extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$user_browser = $_SERVER['HTTP_USER_AGENT'];
		$token = hash('sha512', $this->session->userdata('username') . $user_browser);

		if($this->session->userdata('status')!="success" && $this->session->userdata('token')!=$token){	

			redirect('/admin/auth/login', 'refresh');
			
		}
	}

	public function index()
	{
		$data['slideshows']= $this->SlideshowModel->getImage();
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/slideshow/index', $data);
		$this->load->view('adminMat/footer');
	}
	
	public function addImage()
	{
		if($this->input->post('save')){
			if(!empty($_FILES["image"]["name"])){
	 			// Save images
				$config['upload_path'] = './uploads/images/slideshow';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image')){
					echo "error in pic upload"; 
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic=$upload_pic['file_name'];

					//save into database
					$userData = array(
						'title' => $this->input->post('title'),
						'slug' => $this->slug($this->input->post('title')),
						'caption' => $this->input->post('caption'),
						'image' => $pic,
						'location' => base_url('uploads/images/slideshow/')
						);

					//Pass user data to model
					$this->SlideshowModel->insertImage($userData);
					redirect('/admin/slideshow', 'refresh');
				}
			}else{
				echo '<script>alert("No image is selected")</script>';
				$this->load->view('adminMat/header');
				$this->load->view('adminMat/sidebar');
				$this->load->view('admin/slideshow/addEdit');
				$this->load->view('adminMat/footer');
			}

		}else{
			$this->load->view('adminMat/header');
			$this->load->view('adminMat/sidebar');
			$this->load->view('admin/slideshow/addEdit');
			$this->load->view('adminMat/footer');
		}
	}

	public function editImage($id){
		$data['slideshow']=$this->SlideshowModel->getById($id);
		
		//Form for adding user data
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/slideshow/addEdit', $data);
		$this->load->view('adminMat/footer');
	}

	public function updateImage(){
		if($this->input->post('save')){
			$userData = array(
				'id' => $this->input->post('id'),
				'slug' => $this->slug($this->input->post('title')),
				'title' => $this->input->post('title'),
				'caption' => $this->input->post('caption')
			);

			//Pass user data to model
			$this->SlideshowModel->update($userData);
			redirect('/admin/slideshow', 'refresh');
		}
	}

	public function deleteImage($id){
		$data = $this->SlideshowModel->delete($id);
		redirect('/admin/slideshow', 'refresh');
	}

	public function publishToggle(){
		$id = $this->input->post('id');
		$status = $this->SlideshowModel->publish($id);
		$response['status'] = 'success';
		$response['message'] = $status;
		echo json_encode($response);
	}
}
