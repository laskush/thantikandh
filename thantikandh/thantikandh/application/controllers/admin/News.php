<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$user_browser = $_SERVER['HTTP_USER_AGENT'];
		$token = hash('sha512', $this->session->userdata('username') . $user_browser);

		if($this->session->userdata('status')!="success" && $this->session->userdata('token')!=$token){	

			redirect('/admin/auth/login', 'refresh');
			
		}
	}

	public function index()
	{
		//$data['news']= $this->NewsModel->getNews();
		$data['news']= $this->CategoriesModel->getCategories('news');
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/news/newsCategory', $data);
		$this->load->view('adminMat/footer');
	}

	public function ViewNews($slug)
	{
		$data['news']= $this->NewsModel->getNewsByCate($slug);
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/news/index', $data);
		$this->load->view('adminMat/footer');
	}
	
	public function addNews()
	{
		if($this->input->post('save')){
			//$slug = ($this->input->post('slug')) ? $this->slug($this->input->post('slug')) : $this->slug($this->input->post('category'));

			if(!empty($_FILES["image2"]["name"])){
	 			// Save images
				$config['upload_path'] = './uploads/images/news';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image2')){
					echo "error in pic upload"; 
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic2=$upload_pic['file_name'];
				}
			}else{$pic2='';}
			if(!empty($_FILES["image3"]["name"])){
	 			// Save images
				$config['upload_path'] = './uploads/images/news';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image3')){
					echo "error in pic upload"; 
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic3=$upload_pic['file_name'];
				}
			}else{$pic3='';}

			if(!empty($_FILES["image"]["name"])){
	 			// Save images
				$config['upload_path'] = './uploads/images/news';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image')){
					echo "error in pic upload"; 
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic=$upload_pic['file_name'];
					//save into database
					$userData = array(
						'title' => $this->input->post('title'),
						'slug' => $this->slug($this->input->post('title')),
						'category' => $this->input->post('cate'),
						'st_date' => $this->input->post('st_date'),
						'end_date' => $this->input->post('end_date'),
						'description' => $this->input->post('desc'),
						'content' => $this->input->post('content'),
						'image' => $pic,
						'image2' => $pic2,
						'image3' => $pic3
						);

					//Pass user data to model
					$this->NewsModel->insertNews($userData);
					redirect('/admin/news', 'refresh');
				}
			}else{
				$userData = array(
					'title' => $this->input->post('title'),
					'slug' => $this->slug($this->input->post('title')),
					'category' => $this->input->post('cate'),
					'st_date' => $this->input->post('st_date'),
					'end_date' => $this->input->post('end_date'),
					'description' => $this->input->post('desc'),
					'content' => $this->input->post('content')
					);

				//Pass user data to model
				$this->NewsModel->insertNews($userData);
				redirect('/admin/news', 'refresh');
			}

		}else{
			$data['cate']= $this->CategoriesModel->getCategories('news');
			$this->load->view('adminMat/header');
			$this->load->view('adminMat/sidebar');
			$this->load->view('admin/news/addEdit', $data);
			$this->load->view('adminMat/footer');
		}
	}

	public function editNews($id){
		$data['news']=$this->NewsModel->getById($id);
		$data['cate']= $this->CategoriesModel->getCategories('news');
		//Form for adding user data
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/news/addEdit', $data);
		$this->load->view('adminMat/footer');
	}

	public function updateNews(){
		if($this->input->post('save')){
			$slug = ($this->input->post('slug')) ? $this->slug($this->input->post('slug')) : $this->slug($this->input->post('title'));

			if(!empty($_FILES["image"]["name"])){
	 			// Save images
				$config['upload_path'] = './uploads/images/news';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image')){
					echo "error in pic upload"; 
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic=$upload_pic['file_name'];
				}
			}else{$pic=$this->input->post('img1');}

			if(!empty($_FILES["image2"]["name"])){
	 			// Save images
				$config['upload_path'] = './uploads/images/news';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image2')){
					echo "error in pic upload"; 
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic2=$upload_pic['file_name'];
				}
			}else{$pic2=$this->input->post('img2');}
			if(!empty($_FILES["image3"]["name"])){
	 			// Save images
				$config['upload_path'] = './uploads/images/news';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image3')){
					echo "error in pic upload"; 
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic3=$upload_pic['file_name'];
				}
			}else{$pic3=$this->input->post('img3');}

			$userData = array(
				'id' => $this->input->post('id'),
				'title' => $this->input->post('title'),
				'slug' => $slug,
				'category' => $this->input->post('cate'),
				'st_date' => $this->input->post('st_date'),
				'end_date' => $this->input->post('end_date'),
				'description' => $this->input->post('desc'),
				'content' => $this->input->post('content'),
				'image'=>$pic,
				'image2'=>$pic2,
				'image3'=>$pic3
			);

			//Pass user data to model
			$this->NewsModel->update($userData);
			redirect('/admin/news', 'refresh');
		}
	}

	public function deleteNews($id){
		$data = $this->NewsModel->delete($id);
		redirect('/admin/news', 'refresh');
	}

	public function publishToggle(){
		$id = $this->input->post('id');
		$status = $this->NewsModel->publish($id);
		$response['status'] = 'success';
		$response['message'] = $status;
		echo json_encode($response);
	}

	public function delFile(){
		$id = $this->input->post('id');
		$file = $this->input->post('item');
		$status = $this->NewsModel->deleteImage($id, $file);
		$response['status'] = 'success';
		$response['message'] = $status;
		echo json_encode($response);
	}
}
