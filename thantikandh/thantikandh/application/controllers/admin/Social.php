<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Social extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$user_browser = $_SERVER['HTTP_USER_AGENT'];
		$token = hash('sha512', $this->session->userdata('username') . $user_browser);

		if($this->session->userdata('status')!="success" && $this->session->userdata('token')!=$token){	

			redirect('/admin/auth/login', 'refresh');
			
		}
	}

	public function index()
	{
		$data['socials']= $this->SocialModel->getSocial();
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/social/index', $data);
		$this->load->view('adminMat/footer');
	}
	
	public function addSocial()
	{
		if($this->input->post('save')){

			$userData = array(
				'name' => $this->input->post('name'),
				'linksrc' => $this->input->post('linksrc')
				);

			//Pass user data to model
			$this->SocialModel->insertSocial($userData);
			redirect('/admin/social', 'refresh');

			/*if(!empty($_FILES["logo"]["name"])){
				// Save images
				$config['upload_path'] = './uploads/images/social';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('logo')){
					echo "error in pic upload"; 
					echo $this->upload->display_errors();
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic=$upload_pic['file_name'];

					//save into database
					$userData = array(
						'name' => $this->input->post('name'),
						'icon' => $this->input->post('icon'),
						'linksrc' => $this->input->post('linksrc'),
						'logo' => $pic
						);

					//Pass user data to model
					$this->SocialModel->insertSocial($userData);
					redirect('/admin/social', 'refresh');
				}
			}
			else{
				$userData = array(
					'name' => $this->input->post('name'),
					'icon' => $this->input->post('icon'),
					'linksrc' => $this->input->post('linksrc')
					);

				//Pass user data to model
				$this->SocialModel->insertSocial($userData);
				redirect('/admin/social', 'refresh');
			}*/

		}else{
			$this->load->view('adminMat/header');
			$this->load->view('adminMat/sidebar');
			$this->load->view('admin/social/addEdit');
			$this->load->view('adminMat/footer');
		}
	}

	public function editSocial($id){
		$data['social']=$this->SocialModel->getById($id);
		
		//Form for adding user data
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/social/addEdit', $data);
		$this->load->view('adminMat/footer');
	}

	public function updateSocial(){
		if($this->input->post('save')){
			$userData = array(
				'id' => $this->input->post('id'),
				'name' => $this->input->post('name'),
				'linksrc' => $this->input->post('linksrc')
			);

			//Pass user data to model
			$this->SocialModel->update($userData);
			redirect('/admin/social', 'refresh');
		}
	}

	public function deleteSocial($id){
		$data = $this->SocialModel->delete($id);
		redirect('/admin/social', 'refresh');
	}

	public function publishToggle(){
		$id = $this->input->post('id');
		$status = $this->SocialModel->publish($id);
		$response['status'] = 'success';
		$response['message'] = $status;
		echo json_encode($response);
	}
}
