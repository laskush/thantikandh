<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$user_browser = $_SERVER['HTTP_USER_AGENT'];
		$token = hash('sha512', $this->session->userdata('username') . $user_browser);

		if($this->session->userdata('status')!="success" && $this->session->userdata('token')!=$token){	

			redirect('/admin/auth/login', 'refresh');
			
		}

		/*ckeditor*/
	    $this->load->library('ckeditor');
		$this->load->library('ckfinder');
	 
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
	                array( 'Source', '-', 'Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList' )
	                                                    );
		$this->ckeditor->config['language'] = 'it';
		$this->ckeditor->config['width'] = '730px';
		$this->ckeditor->config['height'] = '300px';            
	 
		//Add Ckfinder to Ckeditor
		$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets/ckfinder/');
	
	}

	public function index()
	{
		$data['articles']= $this->ArticleModel->getArticle();
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/article/index', $data);
		$this->load->view('adminMat/footer');
	}
	
	public function addArticle()
	{
		if($this->input->post('save')){

			if(!empty($_FILES["image"]["name"])){
	 			// Save images
				$config['upload_path'] = './uploads/images/article';
				$config['allowed_types'] = 'gif|jpg|png|pdf';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image')){
					echo "error in pic upload"; 
					echo $this->upload->display_errors();
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic=$upload_pic['file_name'];

					//save into database
					$userData = array(
						'title' => $this->input->post('title'),
						'slug' => $this->slug($this->input->post('title')),
						'description' => $this->input->post('desc'),
						'content' => $this->input->post('content'),
						'image' => $pic
						);

					//Pass user data to model
					$this->ArticleModel->insertArticle($userData);
					redirect('/admin/article', 'refresh');
				}
			}else{
				$userData = array(
					'title' => $this->input->post('title'),
					'slug' => $this->slug($this->input->post('title')),
					'description' => $this->input->post('desc'),
					'content' => $this->input->post('content')
					);

				//Pass user data to model
				$this->ArticleModel->insertArticle($userData);
				redirect('/admin/article', 'refresh');
			}

		}else{
			$this->load->view('adminMat/header');
			$this->load->view('adminMat/sidebar');
			$this->load->view('admin/article/addEdit');
			$this->load->view('adminMat/footer');
		}
	}

	public function editArticle($id){
		$data['article']=$this->ArticleModel->getById($id);
		
		//Form for adding user data
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/article/addEdit', $data);
		$this->load->view('adminMat/footer');
	}

	public function updateArticle(){
		if($this->input->post('save')){
			
			$userData = array(
				'id' => $this->input->post('id'),
				'title' => $this->input->post('title'),
				'slug' => $this->slug($this->input->post('title')),
				'description' => $this->input->post('desc'),
				'content' => $this->input->post('content')
			);

			//Pass user data to model
			$this->ArticleModel->update($userData);
			redirect('/admin/article', 'refresh');
		}
	}

	public function deleteArticle($id){
		$data = $this->ArticleModel->delete($id);
		redirect('/admin/article', 'refresh');
	}

	public function publishToggle(){
		$id = $this->input->post('id');
		$status = $this->ArticleModel->publish($id);
		$response['status'] = 'success';
		$response['message'] = $status;
		echo json_encode($response);
	}
}
