<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$user_browser = $_SERVER['HTTP_USER_AGENT'];
		$token = hash('sha512', $this->session->userdata('username') . $user_browser);

		if($this->session->userdata('status')!="success" && $this->session->userdata('token')!=$token){	

			redirect('/admin/auth/login', 'refresh');
			
		}
	}

	public function index()
	{
		$data['contact']=$this->ContactModel->getById(1);
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/contact/addEdit', $data);
		$this->load->view('adminMat/footer');
	}
	
	public function update()
	{
		if($this->input->post('save')){

			if(!empty($_FILES["image"]["name"])){
				// Save images
				$config['upload_path'] = './uploads/images/contact';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image')){
					echo "error in pic upload"; 
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic=$upload_pic['file_name'];

					//save into database
					$userData = array(
						'id' => $this->input->post('id'),
						'name' => $this->input->post('name'),
						'address' => $this->input->post('address'),
						'contact' => $this->input->post('contact'),
						'toll_free' => $this->input->post('tollfree'),
						'fax' => $this->input->post('fax'),
						'email' => $this->input->post('email'),
						'cc' => $this->input->post('cc'),
						'map' => $this->input->post('map'),
						'brief' => $this->input->post('brief'),
						'logo' => $pic
					);

					//Pass user data to model
					$this->ContactModel->update($userData);
					redirect('/admin/contact', 'refresh');
				}
			}else{
				$userData = array(
					'id' => $this->input->post('id'),
					'name' => $this->input->post('name'),
					'address' => $this->input->post('address'),
					'contact' => $this->input->post('contact'),
					'toll_free' => $this->input->post('tollfree'),
					'fax' => $this->input->post('fax'),
					'email' => $this->input->post('email'),
					'cc' => $this->input->post('cc'),
					'map' => $this->input->post('map'),
					'brief' => $this->input->post('brief')
				);

				//Pass user data to model
				$this->ContactModel->update($userData);
				redirect('/admin/contact', 'refresh');
			}
		}
	}
}
