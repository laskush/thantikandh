<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bidutiya extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$user_browser = $_SERVER['HTTP_USER_AGENT'];
		$token = hash('sha512', $this->session->userdata('username') . $user_browser);

		if($this->session->userdata('status')!="success" && $this->session->userdata('token')!=$token){	

			redirect('/admin/auth/login', 'refresh');
			
		}
	}

	public function index()
	{
		//$data['bidutiya']= $this->BidutiyaModel->getBidutiya();
		$data['bidutiya']= $this->CategoriesModel->getCategories('bidutiya');
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/bidutiya/bidutiyaCategory', $data);
		$this->load->view('adminMat/footer');
	}

	public function ViewBidutiya($slug)
	{
		$data['bidutiya']= $this->BidutiyaModel->getBidutiyaByCate($slug);
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/bidutiya/index', $data);
		$this->load->view('adminMat/footer');
	}
	
	public function addBidutiya()
	{
		if($this->input->post('save')){
			//$slug = ($this->input->post('slug')) ? $this->slug($this->input->post('slug')) : $this->slug($this->input->post('category'));

			if(!empty($_FILES["image2"]["name"])){
	 			// Save images
				$config['upload_path'] = './uploads/images/bidutiya';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image2')){
					echo "error in pic upload"; 
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic2=$upload_pic['file_name'];
				}
			}else{$pic2='';}
			if(!empty($_FILES["image3"]["name"])){
	 			// Save images
				$config['upload_path'] = './uploads/images/bidutiya';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image3')){
					echo "error in pic upload"; 
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic3=$upload_pic['file_name'];
				}
			}else{$pic3='';}
			if(!empty($_FILES["image"]["name"])){
	 			// Save images
				$config['upload_path'] = './uploads/images/bidutiya';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image')){
					echo "error in pic upload"; 
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic=$upload_pic['file_name'];

					//save into database
					$userData = array(
						'title' => $this->input->post('title'),
						'slug' => $this->slug($this->input->post('title')),
						'category' => $this->input->post('cate'),
						'st_date' => $this->input->post('st_date'),
						'end_date' => $this->input->post('end_date'),
						'description' => $this->input->post('desc'),
						'content' => $this->input->post('content'),
						'image' => $pic,
						'image2' => $pic2,
						'image3' => $pic3
						);

					//Pass user data to model
					$this->BidutiyaModel->insertBidutiya($userData);
					redirect('/admin/bidutiya', 'refresh');
				}
			}else{
				$userData = array(
					'title' => $this->input->post('title'),
					'slug' => $this->slug($this->input->post('title')),
					'category' => $this->input->post('cate'),
					'st_date' => $this->input->post('st_date'),
					'end_date' => $this->input->post('end_date'),
					'description' => $this->input->post('desc'),
					'content' => $this->input->post('content')
					);

				//Pass user data to model
				$this->BidutiyaModel->insertBidutiya($userData);
				redirect('/admin/bidutiya', 'refresh');
			}

		}else{
			$data['cate']= $this->CategoriesModel->getCategories('bidutiya');
			$this->load->view('adminMat/header');
			$this->load->view('adminMat/sidebar');
			$this->load->view('admin/bidutiya/addEdit', $data);
			$this->load->view('adminMat/footer');
		}
	}

	public function editBidutiya($id){
		$data['bidutiya']=$this->BidutiyaModel->getById($id);
		$data['cate']= $this->CategoriesModel->getCategories('bidutiya');
		//Form for adding user data
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/bidutiya/addEdit', $data);
		$this->load->view('adminMat/footer');
	}

	public function updateBidutiya(){
		if($this->input->post('save')){
			$slug = ($this->input->post('slug')) ? $this->slug($this->input->post('slug')) : $this->slug($this->input->post('title'));

			if(!empty($_FILES["image"]["name"])){
	 			// Save images
				$config['upload_path'] = './uploads/images/bidutiya';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image')){
					echo "error in pic upload"; 
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic=$upload_pic['file_name'];
				}
			}else{$pic=$this->input->post('img1');}

			if(!empty($_FILES["image2"]["name"])){
	 			// Save images
				$config['upload_path'] = './uploads/images/bidutiya';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image2')){
					echo "error in pic upload"; 
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic2=$upload_pic['file_name'];
				}
			}else{$pic2=$this->input->post('img2');}
			if(!empty($_FILES["image3"]["name"])){
	 			// Save images
				$config['upload_path'] = './uploads/images/bidutiya';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image3')){
					echo "error in pic upload"; 
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic3=$upload_pic['file_name'];
				}
			}else{$pic3=$this->input->post('img3');}

			$userData = array(
				'id' => $this->input->post('id'),
				'title' => $this->input->post('title'),
				'slug' => $slug,
				'category' => $this->input->post('cate'),
				'st_date' => $this->input->post('st_date'),
				'end_date' => $this->input->post('end_date'),
				'description' => $this->input->post('desc'),
				'content' => $this->input->post('content'),
				'image'=>$pic,
				'image2'=>$pic2,
				'image3'=>$pic3
			);

			//Pass user data to model
			$this->BidutiyaModel->update($userData);
			redirect('/admin/bidutiya', 'refresh');
		}
	}

	public function deleteBidutiya($id){
		$data = $this->BidutiyaModel->delete($id);
		redirect('/admin/bidutiya', 'refresh');
	}

	public function publishToggle(){
		$id = $this->input->post('id');
		$status = $this->BidutiyaModel->publish($id);
		$response['status'] = 'success';
		$response['message'] = $status;
		echo json_encode($response);
	}

	public function delFile(){
		$id = $this->input->post('id');
		$file = $this->input->post('item');
		$status = $this->BidutiyaModel->deleteImage($id, $file);
		$response['status'] = 'success';
		$response['message'] = $status;
		echo json_encode($response);
	}
}
