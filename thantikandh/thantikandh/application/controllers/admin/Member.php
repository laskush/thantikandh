<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$user_browser = $_SERVER['HTTP_USER_AGENT'];
		$token = hash('sha512', $this->session->userdata('username') . $user_browser);

		if($this->session->userdata('status')!="success" && $this->session->userdata('token')!=$token){	

			redirect('/admin/auth/login', 'refresh');
			
		}
	}

	public function index()
	{
		//$data['members']= $this->MemberModel->getMember();
		$data['members']= $this->CategoriesModel->getCategories('members');
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/member/memberCategory', $data);
		$this->load->view('adminMat/footer');
	}

	public function board(){
        $data['members'] = $this->CategoriesModel->getCategories('o-members');
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/member/memberCategory', $data);
		$this->load->view('adminMat/footer');
	}

	public function ViewMembers($slug)
	{
		$data['members']= $this->MemberModel->getMemberByCate($slug);
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/member/index', $data);
		$this->load->view('adminMat/footer');
	}
	
	public function addMember()
	{
		if($this->input->post('save')){
			if(!empty($_FILES["image"]["name"])){
	 			// Save images
				$config['upload_path'] = './uploads/images/members';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('image')){
					echo "error in pic upload"; 
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic=$upload_pic['file_name'];

					//save into database
					$userData = array(
						'name' => $this->input->post('name'),
						'description' => $this->input->post('pos'),
						'email' => $this->input->post('email'),
						'phone' => $this->input->post('phone'),
						'mobile' => $this->input->post('mobile'),
						'type' => $this->input->post('cate'),
						'image' => $pic
						);

					//Pass user data to model
					$this->MemberModel->insertMember($userData);
					redirect('/admin/member', 'refresh');
				}
			}else{
				echo '<script>alert("Please select an image to upload")</script>';
				$this->load->view('adminMat/header');
				$this->load->view('adminMat/sidebar');
				$this->load->view('admin/member/addEdit');
				$this->load->view('adminMat/footer');
			}

		}else{
			$data['cate']= $this->CategoriesModel->getCategories('members');
			$data['o_cate']= $this->CategoriesModel->getCategories('o-members');

			$this->load->view('adminMat/header');
			$this->load->view('adminMat/sidebar');
			$this->load->view('admin/member/addEdit', $data);
			$this->load->view('adminMat/footer');
		}
	}

	public function editMember($id){
		$data['member']=$this->MemberModel->getById($id);
		$data['cate']= $this->CategoriesModel->getCategories('members');
		$data['o_cate']= $this->CategoriesModel->getCategories('o-members');
		
		//Form for adding user data
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/member/addEdit', $data);
		$this->load->view('adminMat/footer');
	}

	public function updateMember(){
		if($this->input->post('save')){
			$userData = array(
				'id' => $this->input->post('id'),
				'name' => $this->input->post('name'),
				'description' => $this->input->post('pos'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'mobile' => $this->input->post('mobile'),
				'type' => $this->input->post('cate')
			);

			//Pass user data to model
			$this->MemberModel->update($userData);
			redirect('/admin/member', 'refresh');
		}
	}

	public function deleteMember($id){
		$data = $this->MemberModel->delete($id);
		redirect('/admin/member', 'refresh');
	}

	public function publishToggle(){
		$id = $this->input->post('id');
		$status = $this->MemberModel->publish($id);
		$response['status'] = 'success';
		$response['message'] = $status;
		echo json_encode($response);
	}
}
