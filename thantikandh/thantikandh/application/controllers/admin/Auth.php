<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Auth extends CI_Controller{

		public function __construct(){
			parent::__construct();
			$this->load->model('AdminModel');

		}

		public function login(){
			if($this->input->post('login')){
				$uname = $this->input->post('username');
				$upass = $this->input->post('password');
				
				$chk= $this->AdminModel->checkUser($uname, $upass);
				if(!$chk){
					$this->session->set_flashdata('error','Incorrect Username or Password');					
					$this->load->view('admin/login');
				}else{
					$user_browser = $_SERVER['HTTP_USER_AGENT'];
					$login_string = hash('sha512', $chk->username . $user_browser);

					$sessData = array(
							'status'    =>'success',
					        'username'  => $chk->username,
					        'fullname'  => $chk->fullname,
					        'token'     => $login_string
					);

					$this->session->set_userdata($sessData);
					redirect('admin/welcome');
				}
			}else{
				
				$user_browser = $_SERVER['HTTP_USER_AGENT'];
				$token = hash('sha512', $this->session->userdata('username') . $user_browser);

				if($this->session->userdata('status')!="success" && $this->session->userdata('token')!=$token){	

					$this->load->view('admin/login');
					
				}else{
					redirect('admin/welcome');
				}

			}
		}

		public function logout(){
			$sessData = array('status','username','fullname','token','__ci_last_regenerate');
			$this->session->unset_userdata($sessData);
			$this->load->view('admin/login');
		}
	}

