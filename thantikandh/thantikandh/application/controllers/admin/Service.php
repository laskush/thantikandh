<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$user_browser = $_SERVER['HTTP_USER_AGENT'];
		$token = hash('sha512', $this->session->userdata('username') . $user_browser);

		if($this->session->userdata('status')!="success" && $this->session->userdata('token')!=$token){	

			redirect('/admin/auth/login', 'refresh');
			
		}
	}

	public function index()
	{
		$data['services']= $this->ServiceModel->getService();
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/service/index', $data);
		$this->load->view('adminMat/footer');
	}
	
	public function addService()
	{
		if($this->input->post('save')){

			$userData = array(
				'title' => $this->input->post('name'),
				'slug' => $this->slug($this->input->post('name')),
				'type' => $this->input->post('description')
				);

			//Pass user data to model
			$this->ServiceModel->insertService($userData);
			redirect('/admin/service', 'refresh');

		}else{
			$this->load->view('adminMat/header');
			$this->load->view('adminMat/sidebar');
			$this->load->view('admin/service/addEdit');
			$this->load->view('adminMat/footer');
		}
	}

	public function editService($id){
		$data['service']=$this->ServiceModel->getById($id);
		
		//Form for adding user data
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/service/addEdit', $data);
		$this->load->view('adminMat/footer');
	}

	public function updateService(){
		if($this->input->post('save')){
			$userData = array(
				'id' => $this->input->post('id'),
				'title' => $this->input->post('name'),
				'slug' => $this->slug($this->input->post('name')),
				'type' => $this->input->post('description')
			);

			//Pass user data to model
			$this->ServiceModel->update($userData);
			redirect('/admin/service', 'refresh');
		}
	}

	public function deleteService($id){
		$data = $this->ServiceModel->delete($id);
		redirect('/admin/service', 'refresh');
	}

	public function publishToggle(){
		$id = $this->input->post('id');
		$status = $this->ServiceModel->publish($id);
		$response['status'] = 'success';
		$response['message'] = $status;
		echo json_encode($response);
	}
}
