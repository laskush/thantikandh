<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organization extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$user_browser = $_SERVER['HTTP_USER_AGENT'];
		$token = hash('sha512', $this->session->userdata('username') . $user_browser);

		if($this->session->userdata('status')!="success" && $this->session->userdata('token')!=$token){	

			redirect('/admin/auth/login', 'refresh');
			
		}
	}

	public function index()
	{
		$data['organizations']= $this->OrganizationModel->getOrganization();
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/organization/index', $data);
		$this->load->view('adminMat/footer');
	}
	
	public function addOrganization()
	{
		if($this->input->post('save')){

			if(!empty($_FILES["logo"]["name"])){
				// Save images
				$config['upload_path'] = './uploads/images/organization';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('logo')){
					echo "error in pic upload"; 
					echo $this->upload->display_errors();
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic=$upload_pic['file_name'];

					//save into database
					$userData = array(
						'name' => $this->input->post('name'),
						'description' => $this->input->post('desc'),
						'address' => $this->input->post('address'),
						'contact' => $this->input->post('contact'),
						'website' => $this->input->post('website'),
						'logo' => $pic
					);

					//Pass user data to model
					$this->OrganizationModel->insertOrganization($userData);
					redirect('/admin/organization', 'refresh');
				}
			}
			else{
				$userData = array(
					'name' => $this->input->post('name'),
					'description' => $this->input->post('desc'),
					'address' => $this->input->post('address'),
					'contact' => $this->input->post('contact'),
					'website' => $this->input->post('website')
				);

				//Pass user data to model
				$this->OrganizationModel->insertOrganization($userData);
				redirect('/admin/organization', 'refresh');
			}

		}else{
			$this->load->view('adminMat/header');
			$this->load->view('adminMat/sidebar');
			$this->load->view('admin/organization/addEdit');
			$this->load->view('adminMat/footer');
		}
	}

	public function editOrganization($id){
		$data['organization']=$this->OrganizationModel->getById($id);
		
		//Form for adding user data
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/organization/addEdit', $data);
		$this->load->view('adminMat/footer');
	}

	public function updateOrganization(){
		if($this->input->post('save')){
			$userData = array(
				'id' => $this->input->post('id'),
				'name' => $this->input->post('name'),
				'description' => $this->input->post('desc'),
				'address' => $this->input->post('address'),
				'contact' => $this->input->post('contact'),
				'website' => $this->input->post('website')
			);

			//Pass user data to model
			$this->OrganizationModel->update($userData);
			redirect('/admin/organization', 'refresh');
		}
	}

	public function deleteOrganization($id){
		$data = $this->OrganizationModel->delete($id);
		redirect('/admin/organization', 'refresh');
	}

	public function publishToggle(){
		$id = $this->input->post('id');
		$status = $this->OrganizationModel->publish($id);
		$response['status'] = 'success';
		$response['message'] = $status;
		echo json_encode($response);
	}

	/*------------------------------------------------------------------------------*/
	public function memberList($id)
	{
		$data['members']= $this->OrganizationModel->getMember($id);
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/organization/memberList', $data);
		$this->load->view('adminMat/footer');
	}

	public function deleteMember($id){
		$data = $this->OrganizationModel->deleteMember($id);
		redirect('/admin/organization', 'refresh');
	}

	public function addMember(){
		if($this->input->post('save')){
			if(!empty($_FILES["logo"]["name"])){
				// Save images
				$config['upload_path'] = './uploads/images/organization/member';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('logo')){
					echo "error in pic upload"; 
					echo $this->upload->display_errors();
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic=$upload_pic['file_name'];
				}
			}else{$pic='';}
			$userData = array(
				'name' => $this->input->post('name'),
				'description' => $this->input->post('desc'),
				'address' => $this->input->post('address'),
				'contact' => $this->input->post('contact'),
				'email' => $this->input->post('email'),
				'o_id' => $this->input->post('cate'),
				'image' => $pic
			);

			//Pass user data to model
			$this->OrganizationModel->addMember($userData);
			redirect('/admin/organization', 'refresh');
		}else{
			$data["cate"] = $this->OrganizationModel->getOrganization();
			$this->load->view('adminMat/header');
			$this->load->view('adminMat/sidebar');
			$this->load->view('admin/organization/addMember', $data);
			$this->load->view('adminMat/footer');
		}
	}

	public function editMember($id){
		$data['member']=$this->OrganizationModel->getMemById($id);
		$data["cate"] = $this->OrganizationModel->getOrganization();
		
		//Form for adding user data
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/organization/addMember', $data);
		$this->load->view('adminMat/footer');
	}

	public function updateMember(){
		if($this->input->post('save')){
			if(!empty($_FILES["logo"]["name"])){
				// Save images
				$config['upload_path'] = './uploads/images/organization/member';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('logo')){
					echo "error in pic upload"; 
					echo $this->upload->display_errors();
				}else{
					//get uoloaded picture name
					$upload_pic = $this->upload->data();
					$pic=$upload_pic['file_name'];
				}
			}else{$pic='';}
			$userData = array(
				'id' => $this->input->post('id'),
				'name' => $this->input->post('name'),
				'description' => $this->input->post('desc'),
				'address' => $this->input->post('address'),
				'contact' => $this->input->post('contact'),
				'email' => $this->input->post('email'),
				'o_id' => $this->input->post('cate'),
				'image' => $pic
			);

			//Pass user data to model
			$this->OrganizationModel->updateMember($userData);
			redirect('/admin/organization', 'refresh');
		}
	}
}
