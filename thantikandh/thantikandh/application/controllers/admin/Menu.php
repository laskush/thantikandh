<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$user_browser = $_SERVER['HTTP_USER_AGENT'];
		$token = hash('sha512', $this->session->userdata('username') . $user_browser);

		if($this->session->userdata('status')!="success" && $this->session->userdata('token')!=$token){	

			redirect('/admin/auth/login', 'refresh');
			
		}
	}

	public function index()
	{
		$data['menus']= $this->MenuModel->getParentMenu();
		$this->load->view('adminLTE/header');
		$this->load->view('adminLTE/sidebar');
		$this->load->view('admin/menu/index', $data);
		$this->load->view('adminLTE/footer');
	}
	public function addMenu()
	{
		if($this->input->post('save')){
			$userData = array(
				'name' => $this->input->post('name'),
				'linksrc' => $this->input->post('linksrc'),
				'parentOf' => $this->input->post('parentOf')
				);

			//Pass user data to model
			$this->MenuModel->insertMenu($userData);
			redirect('/admin/menu', 'refresh');

		}else{
			$data['articles']= $this->ArticleModel->getList();
			$data['news']= $this->NewsModel->getList();
			$this->load->view('adminLTE/header');
			$this->load->view('adminLTE/sidebar');
			$this->load->view('admin/menu/addEdit', $data);
			$this->load->view('adminLTE/footer');
		}
	}

	public function editMenu($id)
	{
		$data['menu']= $this->MenuModel->getMenuById($id);
		$data['articles']= $this->ArticleModel->getList();
		$data['news']= $this->NewsModel->getList();
		$this->load->view('adminLTE/header');
		$this->load->view('adminLTE/sidebar');
		$this->load->view('admin/menu/addEdit', $data);
		$this->load->view('adminLTE/footer');
	}

	public function updateMenu(){
		if($this->input->post('save')){
			$userData = array(
				'id' => $this->input->post('id'),
				'name' => $this->input->post('name'),
				'linksrc' => $this->input->post('linksrc'),
				'parentOf' => $this->input->post('parentOf')
			);

			//Pass user data to model
			$this->MenuModel->update($userData);
			redirect('/admin/menu', 'refresh');
		}
	}

	public function deleteMenu($id){
		$data = $this->MenuModel->delete($id);
		redirect('/admin/menu', 'refresh');
	}

	public function publishToggle(){
		$id = $this->input->post('id');
		$status = $this->MenuModel->publish($id);
		$response['status'] = 'success';
		$response['message'] = $status;
		echo json_encode($response);
	}
}
