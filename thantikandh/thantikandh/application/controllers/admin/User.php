<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$user_browser = $_SERVER['HTTP_USER_AGENT'];
		$token = hash('sha512', $this->session->userdata('username') . $user_browser);

		if($this->session->userdata('status')!="success" && $this->session->userdata('token')!=$token){	

			redirect('/admin/auth/login', 'refresh');
			
		}
	}

	public function index()
	{
		$data['user']=$this->UserModel->getByUSer($this->session->userdata('username'));
		$this->load->view('adminMat/header');
		$this->load->view('adminMat/sidebar');
		$this->load->view('admin/user/addEdit', $data);
		$this->load->view('adminMat/footer');
	}
	
	public function update()
	{
		if($this->input->post('save')){
			$userData = array(
				'id' => $this->input->post('id'),
				'fullname' => $this->input->post('fullname'),
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password')
				);

			//Pass user data to model
			$this->UserModel->update($userData);
			redirect('admin/auth/logout', 'refresh');
		}
	}
}
