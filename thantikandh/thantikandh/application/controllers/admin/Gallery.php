<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$user_browser = $_SERVER['HTTP_USER_AGENT'];
		$token = hash('sha512', $this->session->userdata('username') . $user_browser);

		if($this->session->userdata('status')!="success" && $this->session->userdata('token')!=$token){	

			redirect('/admin/auth/login', 'refresh');
			
		}
	}

	public function index()
	{
		$data['cate']= $this->GalleryModel->getImage();
		$this->load->view('adminLTE/header');
		$this->load->view('adminLTE/sidebar');
		$this->load->view('admin/gallery/index', $data);
		$this->load->view('adminLTE/footer');
	}

	public function galleryImages($id)
	{
		$data['cat_id'] = $id;
		$data['images']= $this->GalleryModel->getGalleryImage($id);
		$this->load->view('adminLTE/header');
		$this->load->view('adminLTE/sidebar');
		$this->load->view('admin/gallery/index', $data);
		$this->load->view('adminLTE/footer');
	}


	public function add($id)
	{
		$data['cat_id'] = $id;
		$this->load->view('adminLTE/header');
		$this->load->view('adminLTE/sidebar');
		$this->load->view('admin/gallery/addImage', $data);
		$this->load->view('adminLTE/footer');
	}

	public function addImage(){
		// Save images
		$config['upload_path'] = './uploads/images/gallery';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if(!$this->upload->do_upload('myfile')){
			echo "error in pic upload"; 
		}else{
			//get uoloaded picture name
			$upload_pic = $this->upload->data();
			$pic=$upload_pic['file_name'];

			$source_path =  FCPATH . 'uploads/images/gallery/' . $pic;
		    $target_path =  FCPATH . 'uploads/images/gallery/thumbnails';

		    var_dump($source_path);
		    var_dump($target_path);

		    $config_manip = array(
		        'image_library' => 'gd2',
		        'source_image' => $source_path,
		        'new_image' => $target_path,
		        'maintain_ratio' => TRUE,
		        'create_thumb' => TRUE,
		        'thumb_marker' => '',
		        'width' => 250
		    );
		    $this->load->library('image_lib', $config_manip);
		    if (!$this->image_lib->resize()) {
		        echo $this->image_lib->display_errors();
		    }
			//save into database
			$userData = array(
				'title' => $pic,
				'gallery_cate' =>$_POST["cat_id"],
				'image_location' => base_url('uploads/images/gallery/'),
				'thumb_location' => base_url('uploads/images/gallery/thumbnails/')
				);

			//Pass user data to model
			$this->GalleryModel->insertImage($userData);
			redirect('/admin/gallery', 'refresh');
		}
	}

	public function editImage($id){
		$data['slideshow']=$this->SlideshowModel->getById($id);
		
		//Form for adding user data
		$this->load->view('adminLTE/header');
		$this->load->view('adminLTE/sidebar');
		$this->load->view('admin/slideshow/addEdit', $data);
		$this->load->view('adminLTE/footer');
	}

	public function updateImage(){
		if($this->input->post('save')){
			$userData = array(
				'id' => $this->input->post('id'),
				'slug' => $this->slug($this->input->post('title')),
				'title' => $this->input->post('title'),
				'caption' => $this->input->post('caption')
			);

			//Pass user data to model
			$this->SlideshowModel->update($userData);
			redirect('/admin/slideshow', 'refresh');
		}
	}

	public function deleteImage(){
		$id = $_REQUEST['id'];
		$imgName = $this->GalleryModel->getById($id);
		if(unlink("./uploads/images/gallery/" . $imgName->title)){
			$data = $this->GalleryModel->delete($id);
		}else{
			echo "failed";
		}
		
	}

	public function addCate(){
		$this->load->view('adminLTE/header');
		$this->load->view('adminLTE/sidebar');
		$this->load->view('admin/gallery/addcate');
		$this->load->view('adminLTE/footer');
	}

	public function addCateImage(){
		if(!empty($_FILES["imageasd"]["name"])){
			// Save images
			$config['upload_path'] = './uploads/images/galleryCate';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if(!$this->upload->do_upload('imageasd')){
				echo $this->upload->display_errors();
			}else{
				//get uoloaded picture name
				$upload_pic = $this->upload->data();
				$pic=$upload_pic['file_name'];

				//get uoloaded picture name
				$upload_pic = $this->upload->data();
				$pic=$upload_pic['file_name'];

				$source_path =  FCPATH . 'uploads/images/galleryCate/' . $pic;
			    $target_path =  FCPATH . 'uploads/images/galleryCate/thumbnails';

			    $config_manip = array(
			        'image_library' => 'gd2',
			        'source_image' => $source_path,
			        'new_image' => $target_path,
			        'maintain_ratio' => TRUE,
			        'create_thumb' => TRUE,
			        'thumb_marker' => '',
			        'width' => 250
			    );
			    $this->load->library('image_lib', $config_manip);
			    if (!$this->image_lib->resize()) {
			        echo $this->image_lib->display_errors();
			    }

				//save into database
				$userData = array(
					'title' => $this->input->post('title'),
					'slug' => $this->slug($this->input->post('title')),
					'image' => $pic,
					'image_location' => base_url('uploads/images/galleryCate/'),
					'thumb_location' => base_url('uploads/images/galleryCate/thumbnails/')
					);

				//Pass user data to model
				$this->GalleryModel->insertCateImage($userData);
				redirect('/admin/gallery', 'refresh');
			}
		}else{
			echo '<script>alert("Please select an image to upload")</script>';
			$this->load->view('adminLTE/header');
			$this->load->view('adminLTE/sidebar');
			$this->load->view('admin/gallery/addcate');
			$this->load->view('adminLTE/footer');
		}
	}

	public function deleteCateImage(){
		$id = $_REQUEST['id'];
		$imgName = $this->GalleryModel->getCateById($id);
		if(unlink("./uploads/images/galleryCate/" . $imgName->image)){
			$data = $this->GalleryModel->deleteCate($id);
		}else{
			echo "failed";
		}
		
	}
}
