<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	function __construct() {
        parent::__construct();
    }

	function json_output($statusHeader,$response){
        $ci =& get_instance();
        $ci->output->set_content_type('application/json');
        $ci->output->set_status_header($statusHeader);
        $ci->output->set_header('Access-Control-Allow-Credentials: true');
        $ci->output->set_output(json_encode($response));
    }

    public function index(){
        //$this->json_output(404,array('status' => 403,'records' => 'Permission Denied'));
        echo "403 Permission Denied!";
    }

    public function fetchSlideshow(){
        $slideshow = $this->SlideshowModel->fetchImage();
        if(!empty($slideshow)){
            $this->json_output(200,array('status' => 200,'records' => $slideshow));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    public function fetchAbout(){
        $about = $this->ArticleModel->getById(1);
        $cont = $this->ContactModel->getContact();
        $arr = array('title'=>$about->title, 
                      'description'=>$about->description, 
                      'contact'=>$cont->contact,
                      'toll_free'=>$cont->toll_free,
                      'address'=>$cont->address,
                      'fax'=>$cont->fax,
                      'email'=>$cont->email
                  );
        if(!empty($arr)){
            $this->json_output(200,array('status' => 200,'records' => $arr));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    public function fetchBadapatra(){
        $about = $this->ArticleModel->getById(2);
        if(!empty($about)){
            $this->json_output(200,array('status' => 200,'records' => $about));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    public function fetchAllNews(){
        $news = $this->NewsModel->fetchNews();
        if(!empty($news)){
            $this->json_output(200,array('status' => 200,'records' => $news));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    /* news category */
    public function fetchNewsCategory(){
        $news = $this->CategoriesModel->getCategories('news');
        if(!empty($news)){
            $this->json_output(200,array('status' => 200,'records' => $news));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    /* new categorised news */
    public function fetchNews($slug){
        $news = $this->NewsModel->getNewsByCate($slug);
        $temp = array();
        foreach ($news as $key) {
            $mem['id'] = $key->id;
            $mem['title'] = $key->title;
            $mem['slug'] = $key->slug;
            $mem['description'] = $key->description;
            $mem['content'] = $key->content;
            $mem['st_date'] = $key->st_date;
            $mem['end_date'] = $key->end_date;
            $mem['category'] = $key->category;
            $mem['image'] = array('img1'=>(!empty($key->image))?base_url().'uploads/images/news/'.$key->image:'','img2'=>(!empty($key->image2))?base_url().'uploads/images/news/'.$key->image2:'','img3'=>(!empty($key->image3))?base_url().'uploads/images/news/'.$key->image3:'');
            array_push($temp, $mem);
        }
        if(!empty($temp)){
            $this->json_output(200,array('status' => 200,'records' => $temp));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    public function fetchPlan(){
        $news = $this->PlanModel->fetchPlan();
        if(!empty($news)){
            $this->json_output(200,array('status' => 200,'records' => $news));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    public function fetchContact(){
        $contact = $this->ContactModel->getContact();
        if(!empty($contact)){
            $this->json_output(200,array('status' => 200,'records' => $contact));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    public function fetchGalleryCategory(){
        $cate = $this->GalleryModel->getImage();
        if(!empty($cate)){
            $this->json_output(200,array('status' => 200,'records' => $cate));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    public function fetchGalleryImage($id){
        $cate = $this->GalleryModel->getGalleryImage($id);
        if(!empty($cate)){
            $this->json_output(200,array('status' => 200,'records' => $cate));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    public function fetchBudget(){
        $budList = $this->BudgetModel->fetchBudget();
        $budget = array();
        $partiList = array();
        $particularList = array();
        $budgetList = array();
        foreach ($budList as $bud) {
            $budget["title"] = $bud->title;
            $budget["description"] = $bud->description;
            $budget["amount"] = $bud->amount;
            $budget["date"] = $bud->date;
            $parti = unserialize($bud->particular);
            foreach ($parti as $key => $value) {
                $partiList["name"] = $key;
                $partiList["price"] = $value;
                array_push($particularList, $partiList);
            }
            $budget["particular"] = $particularList;
            array_push($budgetList, $budget);
            unset($particularList);
            $particularList = array();
        }

        if(!empty($budget)){
            $this->json_output(200,array('status' => 200,'records' => $budgetList));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    public function fetchService(){
        $news = $this->ServiceModel->getService();
        if(!empty($news)){
            $this->json_output(200,array('status' => 200,'records' => $news));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    public function fetchOrganization(){
        $news = $this->OrganizationModel->getOrganization();
        if(!empty($news)){
            $this->json_output(200,array('status' => 200,'records' => $news));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    public function fetchOrganizationMember($id){
        $news = $this->OrganizationModel->getMember($id);
        if(!empty($news)){
            $this->json_output(200,array('status' => 200,'records' => $news));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    /* for reports */
    public function fetchAllReports(){
        $reports = $this->ReportModel->fetchReports();
        if(!empty($reports)){
            $this->json_output(200,array('status' => 200,'records' => $reports));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    /* reports category */
    public function fetchReportCategory(){
        $reports = $this->CategoriesModel->getCategories('reports');
        if(!empty($reports)){
            $this->json_output(200,array('status' => 200,'records' => $reports));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    /* new categorised reports */
    public function fetchReport($slug){
        $reports = $this->ReportModel->getReportsByCate($slug);
        $temp = array();
        foreach ($reports as $key) {
            $mem['id'] = $key->id;
            $mem['title'] = $key->title;
            $mem['slug'] = $key->slug;
            $mem['description'] = $key->description;
            $mem['content'] = $key->content;
            $mem['st_date'] = $key->st_date;
            $mem['end_date'] = $key->end_date;
            $mem['category'] = $key->category;
            $mem['image'] = array('img1'=>(!empty($key->image))?base_url().'uploads/images/report/'.$key->image:'','img2'=>(!empty($key->image2))?base_url().'uploads/images/report/'.$key->image2:'','img3'=>(!empty($key->image3))?base_url().'uploads/images/report/'.$key->image3:'');
            array_push($temp, $mem);
        }
        if(!empty($temp)){
            $this->json_output(200,array('status' => 200,'records' => $temp));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    /* members category */
    public function fetchMembersCategory(){
        $reports = $this->CategoriesModel->getCategories('members');
        if(!empty($reports)){
            $this->json_output(200,array('status' => 200,'records' => $reports));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    /* new categorised reports */
    public function fetchMember($slug){
        $reports = $this->MemberModel->getMemberByCate($slug);
        $temp = array();
        foreach ($reports as $key) {
            $mem['name'] = $key->name;
            $mem['description'] = $key->description;
            $mem['type'] = $key->type;
            $mem['email'] = $key->email;
            $mem['phone'] = $key->phone;
            $mem['mobile'] = $key->mobile;
            $mem['image'] = base_url('uploads/images/members') .'/'. $key->image;
            array_push($temp, $mem);
        }
         
        if(!empty($reports)){
            $this->json_output(200,array('status' => 200,'records' => $temp));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    /* members category */
    public function fetchOtherMembers(){
        $reports = $this->CategoriesModel->getCategories('o-members');
        if(!empty($reports)){
            $this->json_output(200,array('status' => 200,'records' => $reports));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    /* important numbers */
    public function fetchNumbers(){
        $reports = $this->SocialModel->getSocial();
        $temp = array();
        foreach ($reports as $key) {
            $mem['name'] = $key->name;
            $mem['number'] = $key->linksrc;
            array_push($temp, $mem);
        }
        if(!empty($reports)){
            $this->json_output(200,array('status' => 200,'records' => $temp));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    /* program category */
    public function fetchProgramCategory(){
        $programs = $this->CategoriesModel->getCategories('programs');
        if(!empty($programs)){
            $this->json_output(200,array('status' => 200,'records' => $programs));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    /* new categorised program */
    public function fetchProgram($slug){
        $programs = $this->PlanModel->getPlanByCate($slug);
        $temp = array();
        foreach ($programs as $key) {
            $mem['id'] = $key->id;
            $mem['title'] = $key->title;
            $mem['slug'] = $key->slug;
            $mem['description'] = $key->description;
            $mem['content'] = $key->content;
            $mem['st_date'] = $key->st_date;
            $mem['end_date'] = $key->end_date;
            $mem['category'] = $key->category;
            $mem['image'] = array('img1'=>(!empty($key->image))?base_url().'uploads/images/plan/'.$key->image:'','img2'=>(!empty($key->image2))?base_url().'uploads/images/plan/'.$key->image2:'','img3'=>(!empty($key->image3))?base_url().'uploads/images/plan/'.$key->image3:'');
            array_push($temp, $mem);
        }
        if(!empty($temp)){
            $this->json_output(200,array('status' => 200,'records' => $temp));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    /* bidutiya category */
    public function fetchBidutiyaCategory(){
        $programs = $this->CategoriesModel->getCategories('bidutiya');
        if(!empty($programs)){
            $this->json_output(200,array('status' => 200,'records' => $programs));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }

    /* new categorised bidutiya */
    public function fetchBidutiya($slug){
        $programs = $this->BidutiyaModel->getBidutiyaByCate($slug);
        $temp = array();
        foreach ($programs as $key) {
            $mem['id'] = $key->id;
            $mem['title'] = $key->title;
            $mem['slug'] = $key->slug;
            $mem['description'] = $key->description;
            $mem['content'] = $key->content;
            $mem['st_date'] = $key->st_date;
            $mem['end_date'] = $key->end_date;
            $mem['category'] = $key->category;
            $mem['image'] = array('img1'=>(!empty($key->image))?base_url().'uploads/images/bidutiya/'.$key->image:'','img2'=>(!empty($key->image2))?base_url().'uploads/images/bidutiya/'.$key->image2:'','img3'=>(!empty($key->image3))?base_url().'uploads/images/bidutiya/'.$key->image3:'');
            array_push($temp, $mem);
        }
        if(!empty($temp)){
            $this->json_output(200,array('status' => 200,'records' => $temp));
        }else{
            $this->json_output(404,array('status' => 400,'records' => 'Not found'));
        }  
    }
    
    /* for email */
    public function sendMail(){
    	$method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            $this->json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            
            $auth  = $this->input->get_request_header('Authorization', TRUE);

            if($auth == 'jayess_shakya'){

                $params = json_decode(file_get_contents('php://input'), TRUE);
                $sub = $params["subject"];
		$name = $params["name"];
		$email = $params["email"];
		$mobile = $params["mobile_no"];
		$body = $params["description"];
		
		$to = "jayesshakya@gmail.com";
		$subject = "Feedback Mail from Thantikandh Android App";
		$txt = "From: ". $name ." (". $email .") \r\n".
			"Contact: ". $mobile. "\r\n" .
			"Subject: ". $sub. "\r\n" .
			"Message: ".$body;
			
		$headers = "From: ". $email . "\r\n" .
		"CC: rajanshresthance@gmail.com";;

		if(mail($to,$subject,$txt,$headers)){
			$this->json_output(400,array('status' => 200,'message' => 'Feedback Sent Successfully'));
		}else{
			$this->json_output(400,array('status' => 400,'message' => 'Feedback Not Sent'));
		}
                

            }else{
                $this->json_output(404,array('status' => 400,'message' => 'Authorization Failed'));
            }
	}
    }

}
