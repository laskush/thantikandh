<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- breadcrumb part -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url(); ?>admin/"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">Contact Details</li>
		</ol>
	</section>
	<!-- breadcrumb ends -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-6 text-left">
				<h3>Contact Details</h3>
			</div>
		</div>

	<?php  	
		$action = 'admin/contact/update';

		$id = $contact->id;
		$name = $contact->name;
		$address = $contact->address;
		$tel = $contact->contact;
		$tollfree = $contact->toll_free;
		$fax = $contact->fax;
		$email = $contact->email;
		$cc = $contact->cc;
		$logo = $contact->logo;
		$map = $contact->map;
		$brief = $contact->brief;

		echo "<div class='row'>
			<div class='col-md-2'></div>
		    <div class=' col-md-8'>
			<div class='box box-primary'>
				<div class='box-body'>";
		$form_att = array('class' => 'testclass', 'id' => 'addMenu');
		echo form_open_multipart($action, $form_att);

		if(!empty($id)){
			echo "<input type='hidden' name='id' value='".$id."'>";
		}

		echo"<div class='form-group'>";

		echo form_label('Name :'); 
		echo form_input(array('id' => 'name', 'name' => 'name', 'class' => 'form-control', 'value' =>$name));
		echo "</div><br>";

		echo"<div class='form-group'>";

		echo form_label('Address :'); 
		echo form_input(array('id' => 'address', 'name' => 'address', 'class' => 'form-control', 'value' =>$address));
		echo "</div><br>";

		echo "<div class='row'>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('Contact Details :')."
						".form_input(array('id' => 'contact', 'name' => 'contact', 'class' => 'form-control', 'value' =>$tel))."
					</div>
				</div>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('Toll Free Number :')."
						".form_input(array('id' => 'tollfree', 'name' => 'tollfree', 'class' => 'form-control', 'value' =>$tollfree))."
					</div>
				</div>
			</div>";

		echo"<div class='row'>
				<div class='col-md-6'>
					<div class='form-group'>";
		echo form_label('Fax :'); 
		echo form_input(array('id' => 'fax', 'name' => 'fax', 'class' => 'form-control', 'value' =>$fax));
		echo "</div></div></div>";

		echo "<div class='row'>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('E-mail Address :')."
						".form_input(array('id' => 'email', 'name' => 'email', 'class' => 'form-control', 'value' =>$email))."
					</div>
				</div>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('CC - Email :')."
						".form_input(array('id' => 'cc', 'name' => 'cc', 'class' => 'form-control', 'value' =>$cc))."
					</div>
				</div>
			</div>";

		echo"<div class='form-group'>";

		echo form_label('Iframe of Google Map :'); 
		echo form_textarea(array('id' => 'map', 'name' => 'map', 'class' => 'form-control', 'rows' =>'5', 'value' =>$map));
		echo "<br>";

		echo form_label('Short Description :'); 
		echo form_textarea(array('id' => 'brief', 'name' => 'brief', 'class' => 'form-control', 'rows' =>'5', 'value' =>$brief));
		echo "<br>";

		echo"</div><div class='form-group'>";
		echo form_label('Choose Logo :');
			echo "<input type = 'file' name='image' id='image' class='form-control'>";
	    echo"</div><br/>";

	    if(!empty($logo)){
	    	echo "<div class='row'>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('Current Logo :')."
						<img src = '".base_url()."uploads/images/contact/".$logo."' class='img-responsive'>
					</div>
				</div>
			</div>";
	    }

		echo form_submit(array('id' => 'save', 'name' => 'save', 'value' => 'Update', 'class'=>'btn btn-primary'));

		echo form_close(); 
		echo "</div>
			</div>
		  </div>
		</div>";
	?>
	</section>
	<!-- /.content -->

</div>