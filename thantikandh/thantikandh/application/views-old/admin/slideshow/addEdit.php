<!-- Content Wrapper. Contains page content -->
<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>Add Slideshow Images</h2>

            <ul class="actions">
                <a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/slideshow'; ?>">List Slideshow Images </a>
            </ul>

        </div>

	<?php 
		$action = (isset($slideshow))?'admin/slideshow/updateImage':'admin/slideshow/addImage';

		$id = (isset($slideshow))?$slideshow->id:'';
		$title = (isset($slideshow))?$slideshow->title:'';
		$caption = (isset($slideshow))?$slideshow->caption:'';
		$image = (isset($slideshow))?$slideshow->image:'';

		echo "<div class='row'>
			<div class='col-md-2'></div>
		    <div class=' col-md-8'>
			<div class='card'>
				<div class='card-body card-padding'>";
					$form_att = array('class' => 'testclass', 'id' => 'addMenu');
					echo form_open_multipart($action, $form_att);

					if(!empty($id)){
						echo "<input type='hidden' name='id' value='".$id."'>";
					}

					echo"<div calss='form-group'>";

					echo form_label('Image Name :'); 
					echo form_input(array('id' => 'title', 'name' => 'title', 'class' => 'form-control', 'value' =>$title, 'required'=>'required'));
					echo "<br>";

					echo"</div><div calss='form-group'>";

					echo form_label('Caption :'); 
					echo form_textarea(array('id' => 'caption', 'name' => 'caption', 'class' => 'form-control tinymce', 'rows' =>'3', 'value' =>$caption));
					echo "<br>";

					if(isset($slideshow)){
						echo"</div><div calss='form-group'>";
						echo form_label('Image :');
						echo "<div class='row'>
								<div class='col-md-4'>
									<img src = '".base_url()."uploads/images/slideshow/".$image."' class='img-responsive'>
								</div>
							</div>";
				    	echo"</div><br/>";
					}else{
						echo"</div><div calss='form-group'>";
						echo form_label('Image :');
						echo "<input type = 'file' name='image' id='image' class='form-control' required>";
				    	echo"</div><br/>";
					}
					

					echo form_submit(array('id' => 'save', 'name' => 'save', 'value' => 'Save', 'class'=>'btn btn-primary'));

					echo form_close(); 
					echo "</div>
						</div>
					  </div>
					</div>";
	?>
	</div>
</section>
	<!-- /.content -->

