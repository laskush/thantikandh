<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>Slideshow Image List</h2>

            <ul class="actions">
                <a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/slideshow/addImage'; ?>">Add Images </a>
            </ul>

        </div>
        <!-- breadcrumb ends -->
		
		<!-- main content -->
		<div class="row">
		    <!-- ------------------------------------- -->
		    <div class="card">
			    <div class="card-header">
			        <h2><small> Home > Slideshow Images</small>
			        </h2>
			    </div>

			    <div class="card-body card-padding">
			    	<!-- --------------------------------------------------------- -->
				    <div class="panel-group" id="accordion">
					  	<div class="panel panel-default">
					      <div class="panel-heading" style="height: 45px; background: #6eaeb9;">
					        <div class="panel-title">
					        	<a href="#content" data-toggle="collapse" data-parent="#accordion">
					        		<div class="col-md-3 hidden-xs">
							          	Title
						        	</div>
						        	<div class="col-md-1 col-xs-8">
							          	Image
						        	</div>
						        	<div class="col-md-6 hidden-xs">
							          	Caption
						        	</div>
						        </a>
						        <div class="col-md-2 col-xs-4 text-center">
						        	Action
						        </div>
					        </div>
					      </div>
					    </div>
					    <?php 
					    	if(!empty($slideshows)){
						    	foreach ($slideshows as $slideshow): ?>
							    	<div class="panel panel-default">
								      <div class="panel-heading" style="height: 45px;background: #a7d1d8;">
								        <div class="panel-title">
								        	<a href="<?php echo base_url() . 'admin/slideshow/editImage/' . $slideshow->id; ?>">
								        		<div class="col-md-3 col-xs-8" style="font-size:14px;">
										          	<?php echo substr(strip_tags($slideshow->title),0,80).'...'; ?>
									        	</div>
									        	<div class="col-md-1 hidden-xs">
										          	<img src="<?php echo base_url() . '/uploads/images/slideshow/' . $slideshow->image; ?>" class="img-responsive pull-left">
									        	</div>
									        	<div class="col-md-6 hidden-xs"  style="font-size:14px;">
										          	<?php echo substr(strip_tags($slideshow->caption),0,180).'...'; ?>
									        	</div>
									        </a>
									        <div class="col-md-2 col-xs-4 text-center" style="margin-top:-10px">
									        	<a class="btn btn-xs btn-primary" href="<?php echo base_url() . 'admin/slideshow/editImage/' . $slideshow->id; ?>"><i class="fa fa-eye"></i></a>
									        	<?php 
									        		if($slideshow->status == 1){
									        			echo '<a class="btn btn-xs btn-success publishToggle" data-target="slideshow" data-link="slideshow" data-id="'.$slideshow->id.'" id="slideshow-'.$slideshow->id.'" title="published"><i class="fa fa-flag"></i></a>';
									        		}else{
									        			echo '<a class="btn btn-xs btn-danger publishToggle" data-target="slideshow" data-link="slideshow" data-id="'.$slideshow->id.'" id="slideshow-'.$slideshow->id.'" title="unpublished"><i class="fa fa-flag"></i></a>';
									        		}
									        	?>
									        	<a class="btn btn-xs btn-danger" href="<?php echo base_url() . 'admin/slideshow/deleteImage/' . $slideshow->id; ?>"><i class="fa fa-trash"></i></a>
									        </div>
								        </div>
								      </div>
								    </div>
							<?php endforeach;
							} ?>
			    </div>
			</div>
		    <!-- ------------------------------------- -->
	    </div>

	    

	    <!-- main ends -->
	</div>


</section>