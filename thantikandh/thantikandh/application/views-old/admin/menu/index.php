<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- breadcrumb part -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url(); ?>admin/"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">Menu List</li>
		</ol>
	</section>
	<!-- breadcrumb ends -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-6 text-left">
				<h3>Menu List</h3>
			</div>
			<div class="col-md-6 text-right">
				<a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/menu/addmenu'; ?>">Add Menu </a>
			</div>
		</div>

  <div class="panel-group" id="accordion">
    <?php foreach ($menus as $menu): ?>
    	<div class="panel panel-default">
	      <div class="panel-heading" style="height: 45px; background: #6eaeb9;">
	        <div class="panel-title">
	        	<a href="#content-<?php echo str_replace(' ', '', $menu->name); ?>" data-toggle="collapse" data-parent="#accordion">
	        		<div class="col-md-3 col-xs-8 col-sm-3">
			          	<?php echo $menu->name; ?>
		        	</div>
		        	<div class="col-md-7 hidden-xs col-sm-7">
			          	Path: <?php echo $menu->linksrc; ?>
		        	</div>
		        </a>
		        <div class="col-md-2 col-xs-4 col-sm-2 text-center">
		        	<?php 
		        		if($menu->status == 1){
		        			echo '<a class="btn btn-xs btn-success publishToggle" data-target="menu" data-link="menu" data-id="'.$menu->id.'" id="menu-'.$menu->id.'" title="published"><i class="fa fa-flag"></i></a>';
		        		}else{
		        			echo '<a class="btn btn-xs btn-danger publishToggle" data-target="menu" data-link="menu" data-id="'.$menu->id.'" id="menu-'.$menu->id.'" title="unpublished"><i class="fa fa-flag"></i></a>';
		        		}
		        	?>
		        	
		        	<a class="btn btn-xs btn-primary" href="<?php echo base_url() . 'admin/menu/editMenu/' . $menu->id; ?>"><i class="fa fa-edit"></i></a>
		        	<a class="btn btn-xs btn-danger" href="<?php echo base_url() . 'admin/menu/deleteMenu/' . $menu->id; ?>"><i class="fa fa-trash"></i></a>
		        </div>
	        </div>
	      </div>

	      <!-- for sub menus -->
	      <?php
	        $CI =& get_instance();
	        $CI->load->model('MenuModel'); 
	        $submenus= $this->MenuModel->getChildMenu($menu->id);

	        if(!empty($submenus)){ 
	      ?>
	       	  <div class="panel-colapse collapse" id="content-<?php echo str_replace(' ', '', $menu->name); ?>">
	       	     <div class="panel-body">
	       	     	<h4>Sub menus of <?php echo $menu->name; ?></h4>
	       	     	<div class="panel-group" id="accordionTwo">
		       	     <?php
		       	     	foreach ($submenus as $submenu) { ?>
				        	<!-- /*------sub menus-------*/ -->
				        	<div class="panel panel-default">
						      <div class="panel-heading" style="height: 45px; background: #7daa61">
						        <div class="panel-title">
						        	<a href="#sub-<?php echo str_replace(' ', '', $submenu->name); ?>" data-toggle="collapse" data-parent="#accordionTwo">
						        		<div class="col-md-3 col-xs-8">
								          	<?php echo $submenu->name; ?>
							        	</div>
							        	<div class="col-md-7 hidden-xs">
								          	Path: <?php echo $submenu->linksrc; ?>
							        	</div>
							        </a>
							        <div class="col-md-2 col-xs-4 text-center">
							        	<?php 
							        		if($submenu->status == 1){
							        			echo '<a class="btn btn-xs btn-success publishToggle" data-target="submenu" data-link="menu" data-id="'.$submenu->id.'" id="submenu-'.$submenu->id.'" title="published"><i class="fa fa-flag"></i></a>';
							        		}else{
							        			echo '<a class="btn btn-xs btn-danger publishToggle" data-target="submenu" data-link="menu" data-id="'.$submenu->id.'" id="submenu-'.$submenu->id.'" title="unpublished"><i class="fa fa-flag"></i></a>';
							        		}
							        	?>
							        	<a class="btn btn-xs btn-primary" href="<?php echo base_url() . 'admin/menu/editMenu/' . $submenu->id; ?>"><i class="fa fa-edit"></i></a>
							        	<a class="btn btn-xs btn-danger" href="<?php echo base_url() . 'admin/menu/deleteMenu/' . $submenu->id; ?>"><i class="fa fa-trash"></i></a>
							        </div>
						        </div>
						      </div>
				        	<!-- /*------sub menu ends------*/ -->

				        	<!-- ----------------------------------------- -->
				        	<!-- for 2nd level menus -->
							<?php
							$CI =& get_instance();
							$CI->load->model('MenuModel'); 
							$submenus2= $this->MenuModel->getChildMenu($submenu->id);

							if(!empty($submenus2)){ 
							?>
								  <div class="panel-colapse collapse" id="sub-<?php echo str_replace(' ', '', $submenu->name); ?>">
								     <div class="panel-body">
								     	<h4>Sub menus of <?php echo $submenu->name; ?></h4>
								     	<div class="panel-group" id="accordionThree">
							   	     <?php
							   	     	foreach ($submenus2 as $submenu2) { ?>
								        	<!-- /*------2nd level menus-------*/ -->
								        	<div class="panel panel-default">
										      <div class="panel-heading" style="height: 45px; background: #cad27a;">
										        <div class="panel-title">
										        	<a href="#sub2-<?php echo str_replace(' ', '', $submenu2->name); ?>" data-toggle="collapse" data-parent="#accordionThree">
										        		<div class="col-md-3 col-xs-7">
												          	<?php echo $submenu2->name; ?>
											        	</div>
											        	<div class="col-md-7 hidden-xs">
												          	Path: <?php echo $submenu2->linksrc; ?>
											        	</div>
											        </a>
											        <div class="col-md-2 col-xs-5 text-center">
											        	<?php 
											        		if($submenu2->status == 1){
											        			echo '<a class="btn btn-xs btn-success publishToggle" data-target="submenu2" data-link="menu" data-id="'.$submenu2->id.'" id="submenu2-'.$submenu2->id.'" title="published"><i class="fa fa-flag"></i></a>';
											        		}else{
											        			echo '<a class="btn btn-xs btn-danger publishToggle" data-target="submenu2" data-link="menu" data-id="'.$submenu2->id.'" id="submenu2-'.$submenu2->id.'" title="unpublished"><i class="fa fa-flag"></i></a>';
											        		}
											        	?>
											        	<a class="btn btn-xs btn-primary" href="<?php echo base_url() . 'admin/menu/editMenu/' . $submenu2->id; ?>"><i class="fa fa-edit"></i></a>
											        	<a class="btn btn-xs btn-danger" href="<?php echo base_url() . 'admin/menu/deleteMenu/' . $submenu2->id; ?>"><i class="fa fa-trash"></i></a>
											        </div>
										        </div>
										      </div>
										  </div>

								        	<!-- /*------2nd level menu ends------*/ -->
								     <?php   }
								     ?>
								 	</div>
								     </div>
								 </div>
							<?php
								}
								echo"</div>";
							?>
							<!-- sub menu ends -->
				        	<!-- ----------------------------------------- -->
				     <?php   }
				     ?>
				 	</div>
	       	     </div>
	       	 </div>
	      <?php
	   		}
		  ?>
	      <!-- sub menu ends -->

	    </div>
	<?php endforeach; ?>

	</section>
	<!-- /.content -->
</div>
<style>
	.panel-title a:hover{
		color: #000;
	}
</style>