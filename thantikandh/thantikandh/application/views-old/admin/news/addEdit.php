<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- breadcrumb part -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url(); ?>admin/"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">Add News</li>
		</ol>
	</section>
	<!-- breadcrumb ends -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-6 text-left">
				<h3>Add News</h3>
			</div>
			<div class="col-md-6 text-right">
				<a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/news'; ?>">List News </a>
			</div>
		</div>

	<?php 
		$action = (isset($news))?'admin/news/updateNews':'admin/news/addNews';

		$id = (isset($news))?$news->id:'';
		$title = (isset($news))?$news->title:'';
		$description = (isset($news))?$news->description:'';
		$content = (isset($news))?$news->content:'';
		$image = (isset($news))?$news->image:'';
		$st_date = (isset($news))?$news->st_date:'';
		//$end_date = (isset($news))?$news->end_date:'';
		$category = (isset($news))?$news->category:'';

		echo "<div class='row'>
			<div class='col-md-2'></div>
		    <div class=' col-md-8'>
			<div class='box box-primary'>
				<div class='box-body'>";
		$form_att = array('class' => 'testclass', 'id' => 'addMenu');
		echo form_open_multipart($action, $form_att);

		if(!empty($id)){
			echo "<input type='hidden' name='id' value='".$id."'>";
		}

		echo"<div class='form-group'>";

		echo form_label('News Title :'); 
		echo form_input(array('id' => 'title', 'name' => 'title', 'class' => 'form-control', 'value' =>$title, 'required'=>'required'));
		echo "</div><br>";

		/*echo "<div class='row'>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('Start Date :')."
						".form_input(array('id' => 'st_date', 'name' => 'st_date', 'class' => 'form-control datepick', 'value' =>$st_date))."
					</div>
				</div>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('End Date :')."
						".form_input(array('id' => 'dend_ate', 'name' => 'end_date', 'class' => 'form-control datepick', 'value' =>$end_date))."
					</div>
				</div>
			</div>";*/

		echo "<div class='row'>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('News Date :')."
						".form_input(array('id' => 'st_date', 'name' => 'st_date', 'class' => 'form-control datepick', 'value' =>$st_date))."
					</div>
				</div>
			</div>";

		echo "<input type='hidden' name='end_date' value='0000-00-00'>";
		echo"<div class='form-group'>";

		/*$noSel = ($category == 'notice')?'selected':'';
		$prSel = ($category == 'press-release')?'selected':'';
		$pSel = ($category == 'programs')?'selected':'';*/

		echo form_label('Category :'); 
		echo "<select name='cate' class='form-control'>";
		foreach ($cate as $key) {
			$nSel = ($category == $key->slug)?'selected':'';
			echo "<option value='".$key->slug."' ".$nSel.">".$key->title."</option>";
		}
		echo "</select>
			</div><br>";

		echo"<div class='form-group'>";

		echo form_label('Short Description :'); 
		echo form_textarea(array('id' => 'desc', 'name' => 'desc', 'class' => 'form-control', 'rows' =>'3', 'value' =>$description, 'required'=>'required'));
		echo "<br>";

		echo form_label('Content :'); 
		echo form_textarea(array('id' => 'content', 'name' => 'content', 'class' => 'form-control tinymce', 'value' =>$content));
		echo "<br>";

		if(isset($news)){
			echo"</div><div calss='form-group'>";
			echo form_label('Image :');
			echo "<div class='row'>
					<div class='col-md-4'>
						<img src = '".base_url()."uploads/images/news/".$image."' class='img-responsive'>
					</div>
				</div>";
	    	echo"</div><br/>";
		}else{
			echo"</div><div class='form-group'>";
			echo form_label('Image :');
				echo "<input type = 'file' name='image' id='image' class='form-control'>";
		    echo"</div><br/>";
		}

		echo form_submit(array('id' => 'save', 'name' => 'save', 'value' => 'Save', 'class'=>'btn btn-primary'));

		echo form_close(); 
		echo "</div>
			</div>
		  </div>
		</div>";
	?>
	</section>
	<!-- /.content -->

</div>