<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>News List</h2>

            <ul class="actions">
                <a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/news/addNews'; ?>">Add News </a>
            </ul>

        </div>
        <!-- breadcrumb ends -->
		
		<!-- main content -->
		<div class="row">
		    <!-- ------------------------------------- -->
		    <div class="card">
			    <div class="card-header">
			        <h2><small> Home > News List</small>
			        </h2>
			    </div>

			    <div class="card-body card-padding">
			    	<!-- --------------------------------------------------------- -->
				    <div class="panel-group" id="accordion">
					  	<div class="panel panel-default">
					      <div class="panel-heading" style="height: 45px; background: #6eaeb9;">
					        <div class="panel-title">
					        	<a href="#content" data-toggle="collapse" data-parent="#accordion">
					        		<div class="col-md-4 col-xs-8">
							          	Title
						        	</div>
						        	<div class="col-md-3 hidden-xs">
							          	Category
						        	</div>
						        	<div class="col-md-3 hidden-xs">
							          	Issued Date
						        	</div>
						        </a>
						        <div class="col-md-2 col-xs-4 text-center">
						        	Action
						        </div>
					        </div>
					      </div>
					    </div>
					    <?php foreach ($news as $ind): ?>
					    	<div class="panel panel-default">
						      <div class="panel-heading" style="height: 45px; background: #a7d1d8;">
						        <div class="panel-title">
						        	<a href="<?php echo base_url() . 'admin/news/editNews/' . $ind->id; ?>">
						        		<div class="col-md-4 col-xs-8">
								          	<?php echo $ind->title; ?>
							        	</div>
							        	<div class="col-md-3 hidden-xs">
								          	<?php echo $ind->category; ?>
							        	</div>
							        	<div class="col-md-3 hidden-xs">
								          	<?php echo $ind->st_date; ?>
							        	</div>
							        </a>
							        <div class="col-md-2 col-xs-4 text-center">
							        	<a class="btn btn-xs btn-primary" href="<?php echo base_url() . 'admin/news/editNews/' . $ind->id; ?>"><i class="fa fa-eye"></i></a>
							        	<?php 
							        		if($ind->status == 1){
							        			echo '<a class="btn btn-xs btn-success publishToggle" data-target="news" data-link="news" data-id="'.$ind->id.'" id="news-'.$ind->id.'" title="published"><i class="fa fa-flag"></i></a>';
							        		}else{
							        			echo '<a class="btn btn-xs btn-danger publishToggle" data-target="news" data-link="news" data-id="'.$ind->id.'" id="news-'.$ind->id.'" title="unpublished"><i class="fa fa-flag"></i></a>';
							        		}
							        	?>
							        	<a class="btn btn-xs btn-danger" href="<?php echo base_url() . 'admin/news/deleteNews/' . $ind->id; ?>"><i class="fa fa-trash"></i></a>
							        </div>
						        </div>
						      </div>
						    </div>
						<?php endforeach; ?>
				    <!-- --------------------------------------------------------- -->
			    </div>
			</div>
		    <!-- ------------------------------------- -->
	    </div>

	    

	    <!-- main ends -->
	</div>


</section>