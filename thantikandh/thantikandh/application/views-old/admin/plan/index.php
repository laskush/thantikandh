<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- breadcrumb part -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url(); ?>admin/"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">Plan List</li>
		</ol>
	</section>
	<!-- breadcrumb ends -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-6 text-left">
				<h3>Plan List</h3>
			</div>
			<div class="col-md-6 text-right">
				<a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/plan/addPlan'; ?>">Add Plan </a>
			</div>
		</div>

  <div class="panel-group" id="accordion">
  	<div class="panel panel-default">
      <div class="panel-heading" style="height: 45px; background: #6eaeb9;">
        <div class="panel-title">
        	<a href="#content" data-toggle="collapse" data-parent="#accordion">
        		<div class="col-md-4 col-xs-8">
		          	Title
	        	</div>
	        	<div class="col-md-3 hidden-xs">
		          	Category
	        	</div>
	        	<div class="col-md-3 hidden-xs">
		          	Issued Date
	        	</div>
	        </a>
	        <div class="col-md-2 col-xs-4 text-center">
	        	Action
	        </div>
        </div>
      </div>
    </div>
    <?php foreach ($plans as $ind): ?>
    	<div class="panel panel-default">
	      <div class="panel-heading" style="height: 45px; background: #a7d1d8;">
	        <div class="panel-title">
	        	<a href="<?php echo base_url() . 'admin/plan/editPlan/' . $ind->id; ?>">
	        		<div class="col-md-4 col-xs-8">
			          	<?php echo $ind->title; ?>
		        	</div>
		        	<div class="col-md-3 hidden-xs">
			          	<?php echo $ind->category; ?>
		        	</div>
		        	<div class="col-md-3 hidden-xs">
			          	<?php echo $ind->st_date; ?>
		        	</div>
		        </a>
		        <div class="col-md-2 col-xs-4 text-center">
		        	<a class="btn btn-xs btn-primary" href="<?php echo base_url() . 'admin/plan/editPlan/' . $ind->id; ?>"><i class="fa fa-eye"></i></a>
		        	<?php 
		        		if($ind->status == 1){
		        			echo '<a class="btn btn-xs btn-success publishToggle" data-target="plan" data-link="plan" data-id="'.$ind->id.'" id="plan-'.$ind->id.'" title="published"><i class="fa fa-flag"></i></a>';
		        		}else{
		        			echo '<a class="btn btn-xs btn-danger publishToggle" data-target="plan" data-link="plan" data-id="'.$ind->id.'" id="plan-'.$ind->id.'" title="unpublished"><i class="fa fa-flag"></i></a>';
		        		}
		        	?>
		        	<a class="btn btn-xs btn-danger" href="<?php echo base_url() . 'admin/plan/deletePlan/' . $ind->id; ?>"><i class="fa fa-trash"></i></a>
		        </div>
	        </div>
	      </div>
	    </div>
	<?php endforeach; ?>

	</section>
	<!-- /.content -->
</div>
<style>
	.panel-title a:hover{
		color: #000;
	}
</style>