<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- breadcrumb part -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url(); ?>admin/"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">Add Plan</li>
		</ol>
	</section>
	<!-- breadcrumb ends -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-6 text-left">
				<h3>Add Plan</h3>
			</div>
			<div class="col-md-6 text-right">
				<a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/plan'; ?>">List Plan </a>
			</div>
		</div>

	<?php 
		$action = (isset($plans))?'admin/plan/updatePlan':'admin/plan/addPlan';

		$id = (isset($plans))?$plans->id:'';
		$title = (isset($plans))?$plans->title:'';
		$description = (isset($plans))?$plans->description:'';
		$content = (isset($plans))?$plans->content:'';
		$image = (isset($plans))?$plans->image:'';
		$st_date = (isset($plans))?$plans->st_date:'';
		//$end_date = (isset($plans))?$plans->end_date:'';
		//$category = (isset($plans))?$plans->category:'';

		echo "<div class='row'>
			<div class='col-md-2'></div>
		    <div class=' col-md-8'>
			<div class='box box-primary'>
				<div class='box-body'>";
		$form_att = array('class' => 'testclass', 'id' => 'addMenu');
		echo form_open_multipart($action, $form_att);

		if(!empty($id)){
			echo "<input type='hidden' name='id' value='".$id."'>";
		}

		echo"<div class='form-group'>";

		echo form_label('Programme Title :'); 
		echo form_input(array('id' => 'title', 'name' => 'title', 'class' => 'form-control', 'value' =>$title, 'required'=>'required'));
		echo "</div><br>";

		/*echo "<div class='row'>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('Start Date :')."
						".form_input(array('id' => 'st_date', 'name' => 'st_date', 'class' => 'form-control datepick', 'value' =>$st_date))."
					</div>
				</div>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('End Date :')."
						".form_input(array('id' => 'dend_ate', 'name' => 'end_date', 'class' => 'form-control datepick', 'value' =>$end_date))."
					</div>
				</div>
			</div>";*/

		echo "<div class='row'>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('Issued Date :')."
						".form_input(array('id' => 'st_date', 'name' => 'st_date', 'class' => 'form-control datepick', 'value' =>$st_date))."
					</div>
				</div>
			</div>";

		echo "<input type='hidden' name='cate' value='plan'>
			<input type='hidden' name='end_date' value='0000-00-00'>";
		/*echo"<div class='form-group'>";

		$nSel = ($category == 'news')?'selected':'';
		$noSel = ($category == 'notice')?'selected':'';
		$prSel = ($category == 'press-release')?'selected':'';
		$pSel = ($category == 'programs')?'selected':'';

		echo form_label('Category :'); 
		echo "<select name='cate' class='form-control'>
				<option value='news' ".$nSel.">News</option>
				<option value='notice' ".$noSel.">Notice</option>
				<option value='press-release' ".$prSel.">Press Release</option>
				<option value='programs' ".$pSel.">Programs</option>
			</select>
			</div><br>";*/

		echo"<div class='form-group'>";

		echo form_label('Short Description :'); 
		echo form_textarea(array('id' => 'desc', 'name' => 'desc', 'class' => 'form-control', 'rows' =>'3', 'value' =>$description, 'required'=>'required'));
		echo "<br>";

		echo form_label('Content :'); 
		echo form_textarea(array('id' => 'content', 'name' => 'content', 'class' => 'form-control tinymce', 'value' =>$content));
		echo "<br>";

		if(isset($plans)){
			echo"</div><div calss='form-group'>";
			echo form_label('Image :');
			echo "<div class='row'>
					<div class='col-md-4'>
						<img src = '".base_url()."uploads/images/plan/".$image."' class='img-responsive'>
					</div>
				</div>";
	    	echo"</div><br/>";
		}else{
			echo"</div><div class='form-group'>";
			echo form_label('Image :');
				echo "<input type = 'file' name='image' id='image' class='form-control'>";
		    echo"</div><br/>";
		}

		echo form_submit(array('id' => 'save', 'name' => 'save', 'value' => 'Save', 'class'=>'btn btn-primary'));

		echo form_close(); 
		echo "</div>
			</div>
		  </div>
		</div>";
	?>
	</section>
	<!-- /.content -->

</div>