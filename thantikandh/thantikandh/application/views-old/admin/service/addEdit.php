<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- breadcrumb part -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url(); ?>admin/"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">Add Service</li>
		</ol>
	</section>
	<!-- breadcrumb ends -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-6 text-left">
				<h3>Add Service</h3>
			</div>
			<div class="col-md-6 text-right">
				<a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/service'; ?>">List Services </a>
			</div>
		</div>

	<?php 
		$action = (isset($service))?'admin/service/updateService':'admin/service/addService';

		$id = (isset($service))?$service->id:'';
		$name = (isset($service))?$service->name:'';
		$description = (isset($service))?$service->description:'';

		echo "<div class='row'>
			<div class='col-md-2'></div>
		    <div class=' col-md-8'>
			<div class='box box-primary'>
				<div class='box-body'>";
		$form_att = array('class' => 'testclass', 'id' => 'addMenu');
		echo form_open_multipart($action, $form_att);

		if(!empty($id)){
			echo "<input type='hidden' name='id' value='".$id."'>";
		}

		echo"<div calss='form-group'>";

		echo form_label('Service Name :'); 
		echo form_input(array('id' => 'name', 'name' => 'name', 'class' => 'form-control', 'value' =>$name, 'required'=>'required'));
		echo "<br>";

		echo"</div><div calss='form-group'>";

		echo form_label('Description :'); 
		echo form_textarea(array('id' => 'description', 'name' => 'description', 'class' => 'form-control', 'rows' =>'3', 'value' =>$description, 'required'=>'required'));
		echo "<br>";

		echo form_submit(array('id' => 'save', 'name' => 'save', 'value' => 'Save', 'class'=>'btn btn-primary'));

		echo form_close(); 
		echo "</div>
			</div>
		  </div>
		</div>";
	?>
	</section>
	<!-- /.content -->

</div>