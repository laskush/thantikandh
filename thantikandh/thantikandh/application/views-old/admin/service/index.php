<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- breadcrumb part -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url(); ?>admin/"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">Services List</li>
		</ol>
	</section>
	<!-- breadcrumb ends -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-6 text-left">
				<h3>Services List</h3>
			</div>
			<div class="col-md-6 text-right">
				<a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/service/addService'; ?>">Add Services </a>
			</div>
		</div>

  <div class="panel-group" id="accordion">
  	<div class="panel panel-default">
      <div class="panel-heading" style="height: 45px; background: #6eaeb9;">
        <div class="panel-title">
        	<a href="#content" data-toggle="collapse" data-parent="#accordion">
        		<div class="col-md-4 col-xs 8">
		          	Name
	        	</div>
	        	<div class="col-md-6 hidden-xs">
		          	Description
	        	</div>
	        </a>
	        <div class="col-md-2 col-xs-4 text-center">
	        	Action
	        </div>
        </div>
      </div>
    </div>
    <?php foreach ($services as $service): ?>
    	<div class="panel panel-default">
	      <div class="panel-heading" style="height: 45px; background: #a7d1d8;">
	        <div class="panel-title">
	        	<a href="<?php echo base_url() . 'admin/service/editService/' . $service->id; ?>">
	        		<div class="col-md-4 col-xs 8">
			          	<?php echo $service->name; ?>
		        	</div>
		        	<div class="col-md-6  col-xs-8">
			          	<?php echo $service->description; ?>
		        	</div>
		        </a>
		        <div class="col-md-2 col-xs-4 text-center">
		        	<a class="btn btn-xs btn-primary" href="<?php echo base_url() . 'admin/service/editService/' . $service->id; ?>"><i class="fa fa-eye"></i></a>
		        	<?php 
		        		if($service->status == 1){
		        			echo '<a class="btn btn-xs btn-success publishToggle" data-target="service" data-link="service" data-id="'.$service->id.'" id="service-'.$service->id.'" title="published"><i class="fa fa-flag"></i></a>';
		        		}else{
		        			echo '<a class="btn btn-xs btn-danger publishToggle" data-target="service" data-link="service" data-id="'.$service->id.'" id="service-'.$service->id.'" title="unpublished"><i class="fa fa-flag"></i></a>';
		        		}
		        	?>
		        	<a class="btn btn-xs btn-danger" href="<?php echo base_url() . 'admin/service/deleteService/' . $service->id; ?>"><i class="fa fa-trash"></i></a>
		        </div>
	        </div>
	      </div>
	    </div>
	<?php endforeach; ?>

	</section>
	<!-- /.content -->
</div>
<style>
	.panel-title a:hover{
		color: #000;
	}
</style>