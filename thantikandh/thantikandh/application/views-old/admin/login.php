<!DOCTYPE html>
<html>
<head>
  <title>ScanNepal - Login</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/adminLTE/css/bootstrap.min.css">
</head>
<body>
  <style type="text/css">
    :root {
      --input-padding-x: .75rem;
      --input-padding-y: .75rem;
    }

    html,
    body {
      height: 100%;
    }

    body {
      display: -ms-flexbox;
      display: -webkit-box;
      display: flex;
      -ms-flex-align: center;
      -ms-flex-pack: center;
      -webkit-box-align: center;
      align-items: center;
      -webkit-box-pack: center;
      justify-content: center;
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
    }

    .form-signin {
      width: 100%;
      max-width: 420px;
      padding: 15px;
      margin: 0 auto;
    }

    .form-label-group {
      position: relative;
      margin-bottom: 1rem;
    }

    .form-label-group > input,
    .form-label-group > label {
      padding: var(--input-padding-y) var(--input-padding-x);
    }

    .form-label-group > label {
      position: absolute;
      top: 0;
      left: 0;
      display: block;
      width: 100%;
      margin-bottom: 0; /* Override default <label> margin */
      line-height: 1.5;
      color: #495057;
      border: 1px solid transparent;
      border-radius: .25rem;
      transition: all .1s ease-in-out;
    }

    .form-label-group input::-webkit-input-placeholder {
      color: transparent;
    }

    .form-label-group input:-ms-input-placeholder {
      color: transparent;
    }

    .form-label-group input::-ms-input-placeholder {
      color: transparent;
    }

    .form-label-group input::-moz-placeholder {
      color: transparent;
    }

    .form-label-group input::placeholder {
      color: transparent;
    }

    .form-label-group input:not(:placeholder-shown) {
      padding-top: calc(var(--input-padding-y) + var(--input-padding-y) * (2 / 3));
      padding-bottom: calc(var(--input-padding-y) / 3);
    }

    .form-label-group input:not(:placeholder-shown) ~ label {
      padding-top: calc(var(--input-padding-y) / 3);
      padding-bottom: calc(var(--input-padding-y) / 3);
      font-size: 12px;
      color: #777;
    }
  </style>
  <form class="form-signin" method="post" action="<?php echo base_url(); ?>admin/auth/login">
    <div class="text-center mb-4">
      <img class="mb-4" src="<?php echo base_url(); ?>assets/adminLTE/image/avatar2.png" alt="Logo Image" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Cadastral Survey</h1>
    </div>

    <div class="form-label-group">
      <input type="text" id="inputEmail" name="username" class="form-control" placeholder="Username" required autofocus>
      <label for="inputEmail">Username</label>
    </div>

    <div class="form-label-group">
      <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required>
      <label for="inputPassword">Password</label>
    </div>
    <input type="submit" class="btn btn-lg btn-primary btn-block" name="login" value="Log In">
  </form>
</body>
</html>