<section id="content">
    <div class="container">
    	<!-- breadcrumb -->
        <div class="block-header">
            <h2>Members List</h2>

            <ul class="actions">
                <a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/member/addMember'; ?>">Add Members </a>
            </ul>

        </div>
        <!-- breadcrumb ends -->
		
		<!-- main content -->
		<div class="row">
		    <!-- ------------------------------------- -->
		    <div class="card">
			    <div class="card-header">
			        <h2><small> Home > Members List</small>
			        </h2>
			    </div>

			    <div class="card-body card-padding">
			    	<!-- --------------------------------------------------------- -->
				    <div class="panel-group" id="accordion">
					  	<div class="panel panel-default">
					      <div class="panel-heading" style="height: 45px; background: #6eaeb9;">
					        <div class="panel-title">
					        	<a href="#content" data-toggle="collapse" data-parent="#accordion">
					        		<div class="col-md-4 hidden-xs">
							          	Name
						        	</div>
						        	<div class="col-md-1 hidden-xs">
							          	
						        	</div>
						        	<div class="col-md-5 col-xs-8">
							          	Position
						        	</div>
						        </a>
						        <div class="col-md-2 col-xs-4 text-center">
						        	Action
						        </div>
					        </div>
					      </div>
					    </div>
					    <?php foreach ($members as $member): ?>
					    	<div class="panel panel-default">
						      <div class="panel-heading"  style="height: 45px; background: #a7d1d8;">
						        <div class="panel-title">
						        	<a href="<?php echo base_url() . 'admin/member/editMember/' . $member->id; ?>">
						        		<div class="col-md-4 hidden-xs">
								          	<?php echo $member->name; ?>
							        	</div>
							        	<div class="col-md-1 hidden-xs">
							        	</div>
							        	<div class="col-md-5  col-xs-8">
								          	<?php echo $member->description; ?>
							        	</div>
							        </a>
							        <div class="col-md-2 col-xs-4 text-center" style="margin-top: -10px;">
							        	<a class="btn btn-xs btn-primary" href="<?php echo base_url() . 'admin/member/editMember/' . $member->id; ?>"><i class="fa fa-eye"></i></a>
							        	<?php 
							        		if($member->status == 1){
							        			echo '<a class="btn btn-xs btn-success publishToggle" data-target="member" data-link="member" data-id="'.$member->id.'" id="member-'.$member->id.'" title="published"><i class="fa fa-flag"></i></a>';
							        		}else{
							        			echo '<a class="btn btn-xs btn-danger publishToggle" data-target="member" data-link="member" data-id="'.$member->id.'" id="member-'.$member->id.'" title="unpublished"><i class="fa fa-flag"></i></a>';
							        		}
							        	?>
							        	<a class="btn btn-xs btn-danger" href="<?php echo base_url() . 'admin/member/deleteMember/' . $member->id; ?>"><i class="fa fa-trash"></i></a>
							        </div>
						        </div>
						      </div>
						    </div>
						<?php endforeach; ?>
				    <!-- --------------------------------------------------------- -->
			    </div>
			</div>
		    <!-- ------------------------------------- -->
	    </div>

	    

	    <!-- main ends -->
	</div>


</section>