<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- breadcrumb part -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url(); ?>admin/"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">Add Member</li>
		</ol>
	</section>
	<!-- breadcrumb ends -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-6 text-left">
				<h3>Add Member</h3>
			</div>
			<div class="col-md-6 text-right">
				<a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/member'; ?>">List Members </a>
			</div>
		</div>

	<?php 
		$action = (isset($member))?'admin/member/updateMember':'admin/member/addMember';

		$id = (isset($member))?$member->id:'';
		$name = (isset($member))?$member->name:'';
		$position = (isset($member))?$member->description:'';
		$image = (isset($member))?$member->image:'';
		$category = (isset($member))?$member->type:'';
		$email = (isset($member))?$member->email:'';
		$phone = (isset($member))?$member->phone:'';
		$mobile = (isset($member))?$member->mobile:'';

		echo "<div class='row'>
			<div class='col-md-2'></div>
		    <div class=' col-md-8'>
			<div class='box box-primary'>
				<div class='box-body'>";
		$form_att = array('class' => 'testclass', 'id' => 'addMenu');
		echo form_open_multipart($action, $form_att);

		if(!empty($id)){
			echo "<input type='hidden' name='id' value='".$id."'>";
		}

		echo"<div calss='form-group'>";

		echo form_label('Member Name :'); 
		echo form_input(array('id' => 'name', 'name' => 'name', 'class' => 'form-control', 'value' =>$name, 'required'=>'required'));
		echo "<br>";

		echo"</div><div calss='form-group'>";

		echo form_label('Position :'); 
		echo form_textarea(array('id' => 'pos', 'name' => 'pos', 'class' => 'form-control', 'rows' =>'3', 'value' =>$position, 'required'=>'required'));
		echo "<br>";

		echo form_label('Phone :'); 
		echo form_input(array('id' => 'phone', 'name' => 'phone', 'class' => 'form-control', 'value' =>$phone));
		echo "<br>";

		echo form_label('Email :'); 
		echo form_input(array('id' => 'email', 'name' => 'email', 'class' => 'form-control', 'value' =>$email));
		echo "<br>";

		echo form_label('Mobile :'); 
		echo form_input(array('id' => 'mobile', 'name' => 'mobile', 'class' => 'form-control', 'value' =>$mobile));
		echo "<br>";

		echo"<div class='form-group'>";

		echo form_label('Category :'); 
		echo "<select name='cate' class='form-control'>";
		foreach ($cate as $key) {
			$nSel = ($category == $key->slug)?'selected':'';
			echo "<option value='".$key->slug."' ".$nSel.">".$key->title."</option>";
		}

		echo "<option disabled>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Board Members</option>";
		foreach ($o_cate as $key) {
			$oSel = ($category == $key->slug)?'selected':'';
			echo "<option value='".$key->slug."' ".$oSel.">".$key->title."</option>";
		}
		echo "</select>";

	    if(isset($member)){
			echo"</div><div calss='form-group'>";
			echo form_label('Image :');
			echo "<div class='row'>
					<div class='col-md-4'>
						<img src = '".base_url()."uploads/images/members/".$image."' class='img-responsive'>
					</div>
				</div>";
	    	echo"</div><br/>";
		}else{
			echo"</div><div calss='form-group'>";
			echo form_label('Image :');
			echo "<input type = 'file' name='image' id='image' class='form-control' required>";
	    	echo"</div><br/>";
		}

		echo form_submit(array('id' => 'save', 'name' => 'save', 'value' => 'Save', 'class'=>'btn btn-primary'));

		echo form_close(); 
		echo "</div>
			</div>
		  </div>
		</div>";
	?>
	</section>
	<!-- /.content -->

</div>