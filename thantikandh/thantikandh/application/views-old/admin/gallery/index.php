<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<?php if(isset($images)){ ?>
	<!-- breadcrumb part -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url(); ?>admin/"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">Gallery Images</li>
		</ol>
	</section>
	<!-- breadcrumb ends -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-6 text-left">
				<h3>Gallery Images</h3>
			</div>
			<div class="col-md-6 text-right">
				<a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/gallery/add/'. $cat_id; ?>">Add Images </a>
			</div>
		</div>

	 	<div class="row">
		    <?php foreach ($images as $image): ?>
		    	<div class="col-md-3 col-xs-12 imgHolder" style="margin-bottom: 25px;">
		    		<img src="<?php echo base_url() . '/uploads/images/gallery/thumbnails/' . $image->title; ?>" class="img-responsive">
		    		<div class="del" id="delete" data-id="<?php echo  $image->id; ?>"><i class="fa fa-times"></i></div>
		    	</div>
			<?php endforeach; ?>
		</div>

	</section>
	<!-- /.content -->
<?php }
if(isset($cate)){ ?>

	<!-- breadcrumb part -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url(); ?>admin/"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">Photo Album</li>
		</ol>
	</section>
	<!-- breadcrumb ends -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-6 text-left">
				<h3>Photo Album</h3>
			</div>
			<div class="col-md-6 text-right">
				<a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/gallery/addCate'; ?>">Add Photo Album </a>
			</div>
		</div>

	 	<div class="row">
		    <?php foreach ($cate as $cat): ?>
		    	<div class="col-md-3 col-xs-12 imgHolder" style="margin-bottom: 25px;">
		    		<img src="<?php echo base_url() . 'uploads/images/galleryCate/thumbnails/' . $cat->image; ?>" class="img-responsive gal" data-id="<?php echo  $cat->id; ?>">
		    		<div class="delCate" id="delete" data-id="<?php echo  $cat->id; ?>"><i class="fa fa-times"></i></div>
		    		<div class="text-center"><?php echo  $cat->title; ?></div>
		    	</div>
			<?php endforeach; ?>
		</div>

	</section>
	<!-- /.content -->

<?php } ?>
</div>
<style>
	.del, .delCate {
	    position: absolute;
	    top: 0;
	    right: 0;
	    margin: 5px 21px;
	    background: #d41515;
	    padding: 0px 5px;
	    border-radius: 50%;
	    display: none;
	    cursor: pointer;
	}
	.imgHolder:hover .del, .imgHolder:hover .delCate {
	    display: block;
	}
	.gal{
		cursor: pointer;
	}
</style>
<script>
	$(document).ready(function(){
		$(".gal").click(function(){
		    window.location.href = '<?php echo base_url() . 'admin/gallery/galleryImages/' ?>'+$(this).attr('data-id');
		});

	    $(".del").click(function(){
		    $.ajax({
		    	url: "<?php echo base_url() . 'admin/gallery/deleteImage'; ?>", 
		    	data : {id:$(this).attr('data-id')},
		    	method:'POST',
		    	success: function(result){
		       		window.location.reload();
		    	}
			});
		});

		$(".delCate").click(function(){
		    $.ajax({
		    	url: "<?php echo base_url() . 'admin/gallery/deleteCateImage'; ?>", 
		    	data : {id:$(this).attr('data-id')},
		    	method:'POST',
		    	success: function(result){
		       		window.location.reload();
		    	}
			});
		});
	});
</script>