<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- breadcrumb part -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url(); ?>admin/"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">Add Budget</li>
		</ol>
	</section>
	<!-- breadcrumb ends -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-6 text-left">
				<h3>Add Budget</h3>
			</div>
			<div class="col-md-6 text-right">
				<a class="btn btn-sm btn-primary" href="<?php echo base_url() . 'admin/budget'; ?>">List Budget </a>
			</div>
		</div>

	<?php 
		$action = (isset($budget))?'admin/budget/updateBudget':'admin/budget/addBudget';

		$id = (isset($budget))?$budget->id:'';
		$title = (isset($budget))?$budget->title:'';
		$description = (isset($budget))?$budget->description:'';
		$amount = (isset($budget))?$budget->amount:'';
		$particular = (isset($budget))?unserialize($budget->particular):'';
		$date = (isset($budget))?$budget->date:'';

		echo "<div class='row'>
			<div class='col-md-2'></div>
		    <div class=' col-md-8'>
			<div class='box box-primary'>
				<div class='box-body'>";
		$form_att = array('class' => 'testclass', 'id' => 'addBudget');
		echo form_open_multipart($action, $form_att);

		if(!empty($id)){
			echo "<input type='hidden' name='id' value='".$id."'>";
		}

		echo"<div class='form-group'>";

		echo form_label('Budget Title :'); 
		echo form_input(array('id' => 'title', 'name' => 'title', 'class' => 'form-control', 'value' =>$title, 'required'=>'required'));
		echo "</div><br>";

		echo "<div class='row'>
				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('Budget Year :')."
						".form_input(array('id' => 'date', 'name' => 'date', 'class' => 'form-control', 'value' =>$date))."
					</div>
				</div>

				<div class='col-md-6'>
					<div class='form-group'>
						".form_label('Budget Amount :')."
						".form_input(array('id' => 'amount', 'name' => 'amount', 'class' => 'form-control', 'value' =>$amount))."
					</div>
				</div>

			</div>";

		echo"<div class='form-group'>";

		echo form_label('Short Description :'); 
		echo form_textarea(array('id' => 'desc', 'name' => 'desc', 'class' => 'form-control', 'rows' =>'3', 'value' =>$description, 'required'=>'required'));
		echo "<br>";

		if(!isset($budget)){
			echo '<div class="multi-field-wrapper">
			      <div>
			        <label>Particulars</label>
			        <button type="button" class="addField btn btn-xs btn-success add-field">Add</button>
			      </div>
			      
			      <div class="multi-fields">
			        <div class="multi-field">

			        	<input class="multiField" type="text" id="p" placeholder="Particular" name="particular[]">
				        <input class="multiField" type="text" id="a" placeholder="Amount" name="price[]">
				        <button type="button" class="removeField btn btn-xs btn-danger remove-field"><i class="fa fa-remove"></i></button>
			          
			        </div>
			      </div>
			    
			  </div><br/>';
		}else{
			echo '<div class="multi-field-wrapper">
			      <div>
			        <label>Particulars</label>
			        <button type="button" class="addField btn btn-xs btn-success add-field">Add</button>
			      </div>
			      
			      <div class="multi-fields">';

			        foreach ($particular as $key => $ind) {
			        	echo '<div class="multi-field">
				        		<input class="multiField" type="text" id="p" placeholder="Particular" name="particular[]" value="'.$key.'">
						        <input class="multiField" type="text" id="a" placeholder="Amount" name="price[]" value="'.$ind.'">
						        <button type="button" class="removeField btn btn-xs btn-danger remove-field"><i class="fa fa-remove"></i></button>
					          </div>';
			        }
			      
			          
			echo '</div>
			    
			  </div><br/>';
		}

		echo form_submit(array('id' => 'save', 'name' => 'save', 'value' => 'Save', 'class'=>'btn btn-primary'));

		echo form_close(); 
		echo "</div>
			</div>
		  </div>
		</div>";
	?>
	</section>
	<!-- /.content -->

</div>
<style>
	.multiField{
		width: 45%;
    	margin: 5px;
    	padding: 5px;
	}

	.addField{
		padding: 2px 7px;
   	 	margin: 10px;
	}

	.removeField{
		padding: 2px 7px;
	    margin-bottom: 4px;
	    font-size: 14px;
	}

</style>
<script>
$('.multi-field-wrapper').each(function() {
	var $wrapper = $('.multi-fields', this);
	$(".add-field", $(this)).click(function(e) {
	    $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find("input[type='text']").val("");
	});
	$('.multi-field .remove-field', $wrapper).click(function() {
	    if ($('.multi-field', $wrapper).length > 1){
	        $(this).parent('.multi-field').remove();
	    }
	});
});
</script>