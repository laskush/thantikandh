<?php 
  $CI =& get_instance();
  $CI->load->model('ContactModel');
  $CI->load->model('SocialModel');
  $social = $CI->SocialModel->getSocial();
  $contact = $CI->ContactModel->getContact();
?>
<section class="clearfix top-footer padder">
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <img src="<?php echo base_url(); ?>uploads/images/contact/<?php echo $contact->logo; ?>" class="img-responsive">
        <p>
          <?php echo $contact->briefNe; ?>
        </p>
      </div>
      <div class="col-sm-2">
        <div class="links">
        <h4 style="margin-left: 15px;">Samajik Links</h4>
        <ul>
          <?php 
            foreach ($social as $link) {
              echo '<li><i class="fa '.$link->icon.'" style="color: white;"></i> &nbsp;<a href="'.$link->linksrc.'">'.$link->nameNe.'</a></li>';
            }
          ?>
        </ul>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="service">
          <?php echo $contact->map; ?>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="other">
        <h4 style="margin-left: 20px;">Samparka</h4>
          <span style="color: #fff; text-align: justify;">
            <?php 
              echo '<b>' .$contact->nameNe . '</b><br>'; 
              echo $contact->addressNe . '<br>'; 
              echo '<i class="fa fa-phone"></i> &nbsp;'. $contact->contactNe . '<br>'; 
              echo '<i class="fa fa-envelope"></i> &nbsp;'. $contact->email; 
            ?>
          </span>
        </div>
      </div>
    </div>
    <hr class="footer-hr">
    <div class="row">
      <div class="col-sm-6">
        <p class="copyright">© 2018 - cadastral survey सबै अधिकार सुरक्षित</p>
      </div>
      <div class="col-sm-6">
      </div>
    </div>
  </div>
</section>
<a href="#0" class="cd-top">Top</a>
      <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
      <script src="<?php echo base_url(); ?>assets/front/js/lightbox-plus-jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/front/js/owl.carousel.js"></script> 
      <script src="<?php echo base_url(); ?>assets/front/js/owl.carousel.min.js"></script>
      
      <script src="<?php echo base_url(); ?>assets/front/js/main.js"></script>

<script>
  window.onscroll = function() {myFunction()};

  var navbar = document.getElementById("navbar");
  var sticky = navbar.offsetTop;

  function myFunction() {
    if (window.pageYOffset >= sticky) {
      navbar.classList.add("sticky")
    } else {
      navbar.classList.remove("sticky");
    }
  }
</script>
<script>
  //for owl carousel
  var owl = $('.owl-carousel');
  owl.owlCarousel({
    items:1,
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
  });
</script>
      <script>
       $('.owl-one').owlCarousel({
    $('.owl-two').owlCarousel({

      $(document).ready(function(){
    $('.owl-one').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        navText: [&#x27;next&#x27;,&#x27;prev&#x27;],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

    $('.owl-two').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        navText: [&#x27;next&#x27;,&#x27;prev&#x27;],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

});
      </script>
       <script>
filterSelection("all")
function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("column");
  if (c == "all") c = "";
  for (i = 0; i < x.length; i++) {
    w3RemoveClass(x[i], "show");
    if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
  }
}

function w3AddClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {element.className += " " + arr2[i];}
  }
}

function w3RemoveClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);     
    }
  }
  element.className = arr1.join(" ");
}


// Add active class to the current button (highlight it)
var btnContainer = document.getElementById("myBtnContainer");
var btns = btnContainer.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function(){
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}
</script>
<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}
</script>
</body>
</html>

