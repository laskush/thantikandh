	<!-- all section are included in this one section -->
	<section class="clearfix mainsection-bg">
		<div class="container padder">
			<div class="row">
				<!-- this division contain the actual content -->
				<div class="col-sm-8">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="<?php echo base_url()?>">घर</a></li>
					    <li class="breadcrumb-item active" aria-current="page">	<?php echo $article->titleNe;?></li>
					  </ol>
					</nav>
					<div class="about-us">
						<h2><?php echo $article->titleNe;?></h2>
						<hr class="h-r">
						<?php echo $article->contentNe;?>
						
					</div>
				</div>
				<!-- end of actual div -->

				
