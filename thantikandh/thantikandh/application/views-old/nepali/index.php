<!-- banner slider start -->
		<section class="clearfix bannersection content">
			<div class="owl-carousel owl-theme">
			<?php
				if(isset($slideImages)){
      				foreach ($slideImages as $img) {
      					echo '<div class="item">
					    	<img src="'.base_url().'uploads/images/slideshow/'.$img->image.'" class="img-responsive">
					    	<div class="banner-text">
						    	<h1>'.$img->title.'</h1>
						    	<p>'.$img->caption.'</p><br><br>
						    	<button class="btn btn-primary">Read More</button>
					    	</div>
					    </div>';
		      		}
				}
				?>
			</div>
		</section>
		<!-- banners slider end -->

		<!-- all section are included in this one section -->
		<section class="clearfix mainsection-bg">
			<div class="container padder">
				<div class="row">
					<!-- this division contain the actual content -->
					<div class="col-sm-8">
						<?php 
							if(isset($about)){
								echo '<div class="about-nitc">
									<h2>'.$about->titleNe.'</h2><br>
									<p>'.$about->contentNe.'</p>
									<button class="btn btn-primary">Read More</button>
								</div>';
							}
						?>
						
						<!-- message section start -->
						<div class="msg-director dir-div">
							<?php 
								if(isset($msg)){
									echo '<div class="about-nitc">
										<h2>'.$msg->titleNe.'</h2><br>
										<p>'.$msg->contentNe.'</p>
										<button class="btn btn-primary">Read More</button>
									</div>';
								}
							?>
						</div>
						<!-- end of message section -->
						<!-- panel section start -->
						<div class="padder">
								<ul class="nav nav-tabs">
							    <li class="active"><a data-toggle="tab" href="#home">समाचार</a></li>
							    <li><a data-toggle="tab" href="#menu1">प्रेस विमोचन</a></li>
							    <li><a data-toggle="tab" href="#menu2">कार्यक्रमहरू</a></li>
							  </ul>

							  <div class="tab-content">
							    <div id="home" class="tab-pane fade in active">
							      <div class="panel-news">
							      	<h3>समाचार</h3>
							      	<div class="row">
							      		
							      		<?php
							      			if(isset($news)){
							      				foreach ($news as $nws) {
							      					echo '<div class="col-sm-8">
							      					<p><i class="fa fa-arrow-circle-right"></i> <a href="'.base_url().'ne/news/'.$nws->slug.'">'.$nws->descriptionNe.'</a></p>
							      					</div>
							      					<div class="col-sm-4">
								      				<p><i class="fa fa-calendar"></i>'.$nws->st_date.'</p>
								      				</div>
								      				<br>';
							      				}
											}
										?>
										<div class="col-md-offset-8 col-md-4">
								      		<p class="viewall"><i class="fa fa-plus"></i> <a href="#">सबै हेर्नुहोस्
								      		</a></p>
							      		</div>
							      	</div>
							      </div>
							    </div>
							    <div id="menu1" class="tab-pane fade">
							      <h3>प्रेस विमोचन</h3>
							      <div class="row pressrelease">
							      	<?php
						      			if(isset($press)){
						      				foreach ($press as $prs) {
						      					echo '<div class="col-sm-8">
						      					<p><i class="fa fa-file"></i> <a href="'.base_url().'ne/news/'.$prs->slug.'">'.$prs->titleNe.'</a></p>
						      					</div>
						      					<div class="col-sm-4">
							      				<p><i class="fa fa-calendar"></i>'.$prs->st_date.'</p>
							      				</div>
							      				<br>';
						      				}
										}
									?>
							      	<div class="col-md-offset-8 col-md-4">
								      	<p class="viewall"><i class="fa fa-plus"></i> <a href="#">सबै हेर्नुहोस्</a></p>
							      	</div>
							      </div>
							    </div>
							    <div id="menu2" class="tab-pane fade">
							    	<div class="programs">
							    		<h3>कार्यक्रमहरू</h3>
						     			<div class="row">
						     				<?php
								      			if(isset($programs)){
								      				foreach ($programs as $prg) {
								      					echo '<div class="col-sm-8">
								      					<p><i class="fa fa-file"></i> <a href="'.base_url().'ne/news/'.$prg->slug.'">'.$prg->descriptionNe.'</a></p>
								      					</div>
								      					<div class="col-sm-4">
									      				<p><i class="fa fa-calendar"></i>'.$prg->st_date.'</p>
									      				</div>
									      				<br>';
								      				}
												}
											?>
								      		<div class="col-md-offset-8 col-md-4">
										      	<p class="viewall"><i class="fa fa-plus"></i> <a href="#">सबै हेर्नुहोस्
										      	</a></p>
									      	</div>
						     			</div>
							    	</div>
							     
							    </div>
							  </div>
							</div>
						<!-- end of panel section -->
						</div>
						<!-- end of actual div -->

					<!-- sidebar start -->
					<div class="col-sm-4">

						<!-- start member photo section -->
						<?php
			      			if(isset($members)){
			      				foreach ($members as $mem) {
			      					echo '<div class="latest-notice">
									<div class="panel-group">
									  <div class="panel panel-primary">
									      <div class="panel-heading"><i class="fa fa-user"></i>'.$mem->descriptionNe.'</div>
									      <div class="panel-body">
											<a href="#"><img src="'.base_url().'uploads/images/members/'.$mem->image.'" class="img-responsive"></a>
									      </div>
									      <div class="panel-footer"><strong>'.$mem->nameNe.'</strong></div>
									    </div>
									</div>
								</div>';
					      		}
							}
						?>
						<!-- end member photo section -->

						<!-- login div start -->
						<div class="sidemunu">
							<ul>
								<li class="margnbotm"><i class="fa fa-lock"></i> <a href="<?php echo base_url()?>member">सदस्य लगिन</a></li>
								<li class="margnbotm"><i class="fa fa-envelope"></i> <a href="#">ईमेल हेर्नुहोस्</a></li>
								<li class="margnbotm"><i class="fa fa-users"></i> <a href="<?php echo base_url()?>web/team">कर्मचारी सूची</a></li>
								<!-- <li class="margnbotm"><i class="fa fa-envelope"></i> <a href="#">Check Email</a></li>
								<li class="margnbotm"><i class="fa fa-users"></i> <a href="#">Staff List</a></li> -->
							</ul>
						</div>
						<!-- end login div -->

						<!-- start latest notice div -->
						<div class="latest-notice">
							<div class="panel-group">
							  <div class="panel panel-primary">
							      <div class="panel-heading">नवीनतम सूचना</div>
							      <div class="panel-body">
							      	<ul>
						      		<?php
						      			if(isset($notice)){
						      				foreach ($notice as $not) {
						      					echo '<li><i class="fa fa-arrow-circle-right"></i> <a href="'.base_url().'/news/'.$not->slug.'">'.$not->title.'</a></li>';
						      				}
										}
									?>
							      	</ul>
							      </div>
							    </div>
						</div>
					</div>
					<!-- end latest notive div -->
				</div>
				<!-- sidebar ends -->
			</div>
		</div>
	</section>
	<!-- main section ends -->
