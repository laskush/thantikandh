<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>Government Site</title>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/owl.theme.default.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front/css/bootstrap-dropdownhover.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/lightbox.min.css">

<script src="<?php echo base_url(); ?>assets/front/js/jquery.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script> 
</head>
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<body>

<!-- <style>#google_translate_element,.skiptranslate, .goog-tooltip{display:none !important; visibility: none !important;} .goog-text-highlight{background: none !important; box-shadow: 0px 0px 0px #33333305} body{top:0!important;}</style>

<div id="google_translate_element"></div> -->

 <section class="clearfix top-header padder">
  <div class="container">
    <div class="row">
      <div class="col-sm-5">
        <a href="<?php echo base_url('ne') ?>"><img src="<?php echo base_url(); ?>assets/front/images/logo.png" class="img-responsive"></a>
      </div>
      <div class="col-sm-7">
        <div class="language">
          <!-- <ul class="pull-right" style="list-style: none;     padding-left: 0;">
            <li style="display: inline-block;"><a href="#googtrans(en|en)" class="lang-en lang-select" data-lang="en"><img src="<?php echo base_url('assets/img/usa.png')?>" alt="USA" style="width:35px"></a></li>
            <li style="display: inline-block;"><a href="#googtrans(en|ne)" class="lang-ne lang-select" data-lang="es"><img src="<?php echo base_url('assets/img/nepal.png')?>" alt="Nepal"></a></li>
          </ul> -->
          <ul class="pull-right" style="list-style: none;     padding-left: 0;">
            <li style="display: inline-block;"><a href="<?php echo base_url('web')?>" class="lang-en lang-select"><img src="<?php echo base_url('assets/img/usa.png')?>" alt="USA" style="width:35px"></a></li>
            <li style="display: inline-block;"><a href="<?php echo base_url('ne')?>" class="lang-ne lang-select"><img src="<?php echo base_url('assets/img/nepal.png')?>" alt="Nepal"></a></li>
          </ul>
        </div>
        <form role="form">
        <div class="input-group">
          <input type="text" class="form-control empty" placeholder="Keywords"/>
          <div class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <i class="fa fa-search" aria-hidden="true"></i>
            </button>
          </div>
        </div>
      </form>
      </div>
    </div>
  </div>
 </section>

 <section class="clearfix bottom-header navpadder content" id="navbar">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <nav class="navbar navbar-default navbar-left navbar-expand-lg" id="navbars">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
               <div class="containertouch button navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" onclick="myFunction(this)">
                  <div class="bar1">Menu</div>
              </div>
            </div>
            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav content">
                <?php
                  if(!isset($parents)){
                   $CI =& get_instance();
                   $mod = $CI->load->model('MenuModel');
                   $parents = $CI->MenuModel->getParentMenu();
                  }
                   if(!empty($parents)) {
                    foreach ($parents as $parent) { ?>
                      <li id="pMenu-<?php echo $parent->id; ?>"><!-- class="dropdown" -->
                        <a href="<?php echo base_url('ne').'/'. $parent->linksrc?>" class="one-<?php echo $parent->id; ?>" ><?php echo $parent->nameNe?></a><!-- dropdown-toggle data-toggle="dropdown"-->
                        
                          <?php
                             $CI =& get_instance();
                             $CI->load->model('MenuModel'); 
                             $submenus= $this->MenuModel->getChildMenu($parent->id);
                             if(!empty($submenus)) {
                              echo '<script>
                                  $(".one-'.$parent->id.'").append("<b class=caret></b>");
                                  $(".one-'.$parent->id.'").attr("class","dropdown-toggle");
                                  $(".one-'.$parent->id.'").attr("data-toggle","dropdown");
                                  $("#pMenu-'.$parent->id.'").attr("class","dropdown");
                              </script>';
                              echo'<ul class="dropdown-menu multi-level">';
                              foreach ($submenus as $sub) { ?>
                                <li class="dropdown-submenu">
                                  <a href="<?php echo base_url('ne').'/'. $sub->linksrc?>" class="dropdown-toggle"><?php echo $sub->nameNe?></a> <!--  data-toggle="dropdown" -->
                                  <!-- Level 2 -->
                                  
                                    <?php
                                       $CI =& get_instance();
                                       $CI->load->model('MenuModel'); 
                                       $submenus2= $this->MenuModel->getChildMenu($sub->id);
                                       if(!empty($submenus2)) {
                                         echo '<script>
                                            $(".dropdown-toggle").attr("data-toggle","dropdown");
                                        </script>';
                                         echo'<ul class="dropdown-menu">';
                                        foreach ($submenus2 as $sub2) { ?>                 
                                          <li><a href="<?php echo base_url('ne').'/'. $sub2->linksrc?>"><?php echo $sub2->nameNe?></a></li>
                                      <?php }
                                      echo'</ul>';
                                       }
                                    ?>
                              </li>
                            <?php }
                              echo'</ul>';
                             }
                          ?>       
                      </li>   
                  <?php }
                   }
                ?>
                       
              </ul>
            </div>
            <!-- /.navbar-collapse --> 
        </nav>
      </div>
    </div>
  </div>
 </section>

<!-- <script>
  
  function googleTranslateElementInit() {
      new google.translate.TranslateElement({ 
          pageLanguage: 'en',
          autoDisplay: false
      }, 'google_translate_element');
  }

  function triggerHtmlEvent(element, eventName) {
    var event;
    if (document.createEvent) {
    event = document.createEvent('HTMLEvents');
    event.initEvent(eventName, true, true);
    element.dispatchEvent(event);
    } else {
    event = document.createEventObject();
    event.eventType = eventName;
    element.fireEvent('on' + event.eventType, event);
    }
  }

  jQuery('.lang-select').click(function() {
    var theLang = jQuery(this).attr('data-lang');
    jQuery('.goog-te-combo').val(theLang);

    window.location = jQuery(this).attr('href');
    location.reload();

  });
</script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> -->
