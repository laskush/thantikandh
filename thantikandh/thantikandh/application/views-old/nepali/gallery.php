
	<!-- all section are included in this one section -->
	<section class="clearfix mainsection-bg">
		<div class="container padder">
			<div class="row">
				<!-- this division contain the actual content -->
				<div class="col-sm-8">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="#">घर</a></li>
					    <li class="breadcrumb-item active" aria-current="page">ग्यालरी</li>
					  </ol>
					</nav>
					<div class="about-us">
						<h2>ग्यालरी</h2>
						<hr class="h-r">
							<!-- MAIN (Center website) -->
						<div class="main">
						<div id="myBtnContainer">
						  <button class="btn " onclick="filterSelection('all')"> Sabbai</button>
						  <?php
						  	foreach ($gallCate as $gal) {
						  		echo"<button class='btn' onclick=\"filterSelection('pic-".$gal->id."')\">".$gal->titleNe."</button> ";
						  	}
						  ?>
						</div>

						<!-- Portfolio Gallery Grid -->
						<div class="row">
							<?php
							  	foreach ($gallImg as $img) {
							  		echo'<div class="column pic-'.$img->gallery_cate.'">
									    <div class="content">
												<a class="example-image-link" href="'.base_url().'uploads/images/gallery/'. $img->title.'" data-lightbox="example-set" data-title=""><img class="example-image" src="'.base_url().'uploads/images/gallery/thumbnails/'. $img->title.'" alt="'.$img->description.'"/></a>
									    </div>
									  </div>';
							  	}
							  ?>
						<!-- END GRID -->
						</div>

						<!-- END MAIN -->
						</div>
						
					</div>
				</div>
				<!-- end of actual div -->

				