		<!-- all section are included in this one section -->
		<section class="clearfix mainsection-bg">
			<div class="container padder">
				<div class="row">
					<!-- this division contain the actual content -->
					<div class="col-sm-8">
						<nav aria-label="breadcrumb">
						  <ol class="breadcrumb">
						    <li class="breadcrumb-item"><a href="#">घर</a></li>
						    <li class="breadcrumb-item active" aria-current="page">हाम्रो समूह</li>
						  </ol>
						</nav>
						<div class="about-us team-section">
							<h2>हाम्रो टोली सदस्य</h2>
							<hr class="h-r">
							<div class="row">
							<?php 
								foreach ($members as $member) { ?>
									<div class="col-sm-4">
										<div class="column">
										    <div class="card">
										      <img src="<?php echo base_url(). 'uploads/images/members/' .$member->image; ?>" alt="Jane" style="width:100%">
										      <div class="containers">
										        <h3><?php echo $member->nameNe; ?></h3>
										        <p class="title"><?php echo $member->descriptionNe; ?></p>
										        <p><button class="button">Contact</button></p>
										      </div>
										    </div>
										</div>
									</div>
							<?php	
								}
							?>
							</div>
						</div>
						
					</div>
					<!-- end of actual div -->
