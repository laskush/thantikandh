<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cadastral Survey | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminLTE/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <!-- <link rel="stylesheet" href="css/font-awesome.min.css"> --><!--  -->
  <link rel="stylesheet" type="text/css" href="
  https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css

  ">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminLTE/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminLTE/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminLTE/css/_all-skins.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">

  <link href="http://hayageek.github.io/jQuery-Upload-File/4.0.11/uploadfile.css" rel="stylesheet">
  <!-- jQuery 3 -->
  <script src="<?php echo base_url(); ?>assets/adminLTE/js/jquery.min.js"></script>

  <!-- Bootstrap 3.3.7 -->
  <script src="<?php echo base_url(); ?>assets/adminLTE/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/adminLTE/js/adminlte.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js""></script>

  <script src="http://hayageek.github.io/jQuery-Upload-File/4.0.11/jquery.uploadfile.min.js"></script>

  <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/ckfinder/ckfinder.js"></script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<style>
   .fix-head{position: fixed; width: 100%;}
   .fix-side{//position: fixed;}
   .content-wrapper{margin-top: 50px;}
   .btn-sm{margin-top: 20px; padding-top:2px; font-size: 14px;}
   .action a{height: 30px; width: 30px; font-size: 15px; padding-left: 6px;}
   .panel-title a:hover, a:active, a:focus {color: #47333f;}
</style>
  <header class="main-header fix-head">
    <!-- Logo -->
    <a href="<?php echo base_url('admin'); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>C</b>Su</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Cadastrol</b> Survey</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url(); ?>assets/adminLTE/image/avatar2.png" class="user-image" alt="User Image">

              <?php
                if($this->session->has_userdata('fullname')){
                  $name = $this->session->userdata('fullname');
                }
                if($this->session->has_userdata('mem_fullname')){
                  $name = $this->session->userdata('mem_fullname');
                }
              ?>

              <span class="hidden-xs"><?php echo $name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url(); ?>assets/adminLTE/image/avatar2.png" class="img-circle" alt="User Image">

                <p>
                  <?php echo $name; ?>
                  <small>Administrator</small>
                </p>
              </li>

              <?php
              $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
              $uri_segments = explode('/', $uri_path);
              $user = $uri_segments[2] == "member"?"member":"admin";
              ?>
             
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url() .$user. "/auth/logout"; ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>
    </nav>
  </header>
