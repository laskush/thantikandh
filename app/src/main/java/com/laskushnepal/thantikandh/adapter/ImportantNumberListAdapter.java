package com.laskushnepal.thantikandh.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.laskushnepal.thantikandh.Interface.RecylerItemClickListner;
import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.model.MahatoPurnaNumber.ImportantNumberDTO;

public class ImportantNumberListAdapter extends RecyclerView.Adapter<ImportantNumberListAdapter.Holder> {

    Context mContext;
    String blockType;
    Integer pos;
    private LayoutInflater mLayoutInflater;
    private ArrayList<ImportantNumberDTO> mimportantNumberList = new ArrayList<>();
    ArrayList<String> multiple_numbers = new ArrayList<>();

    private RecylerItemClickListner clickListener;

    public ImportantNumberListAdapter(LayoutInflater inflater, Context mContext) {
        mLayoutInflater = inflater;
        this.mContext = mContext;

    }


    public ImportantNumberListAdapter(LayoutInflater inflater, Context mContext, String blockType, Integer pos) {
        mLayoutInflater = inflater;
        this.mContext = mContext;
        this.blockType = blockType;
        this.pos = pos;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.list_item_important_number, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bindImportantNumber(mimportantNumberList.get(position));
    }

    @Override
    public int getItemCount() {
        return mimportantNumberList.size();
    }

    public void setClickListener(RecylerItemClickListner itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public void addImportantNumberList(List<ImportantNumberDTO> importantNumberList) {
        mimportantNumberList.addAll(importantNumberList);
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //for ImportantNumber

        CardView important_number_cardView;
        LinearLayout important_number_ll;
        TextView txt_title, txt_number;
        private RecylerItemClickListner itemClickListner;


        public Holder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);

            important_number_cardView = (CardView) itemView.findViewById(R.id.important_number_cardView);
            important_number_ll = (LinearLayout) itemView.findViewById(R.id.important_number_ll);
            txt_title = (TextView) itemView.findViewById(R.id.important_number_title);
            txt_number = (TextView) itemView.findViewById(R.id.important_number_tv);

        }

        public void bindImportantNumber(final ImportantNumberDTO importantNumberDTO) {

            txt_title.setText(importantNumberDTO.getTitle());
            txt_number.setText(importantNumberDTO.getNumber());
            //ordinalIndexOf(importantNumberDTO.getNumber(),",",0);

            //Toast.makeText(mContext, "Size : " + multiple_numbers.size(), Toast.LENGTH_SHORT).show();
           /* important_number_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });*/

            important_number_cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(importantNumberDTO.getNumber().length() == 0){
                        Toast.makeText(mContext,mContext.getResources().getString(R.string.field_empty),Toast.LENGTH_SHORT).show();
                    } else if (importantNumberDTO.getNumber().contains(",")) {
                        multiple_numbers = new ArrayList<>();
                        multiple_numbers = ordinalIndexOf(importantNumberDTO.getNumber(), ",", 0);
                        makeAList(multiple_numbers);
                    } else {
                        multiple_numbers = new ArrayList<>();
                        multiple_numbers.add(importantNumberDTO.getNumber().substring(0, importantNumberDTO.getNumber().length()));
                        makeAList(multiple_numbers);
                    }

                    /*for(int i =0; i< multiple_numbers.size();i++){
                        Toast.makeText(mContext, "Size "+ (i+1) +" : "+multiple_numbers.get(i), Toast.LENGTH_SHORT).show();
                    }*/



                }
            });


        }

        //checking whether the string contain the character , and position of the the character , and finding the total numbers
        public ArrayList<String> ordinalIndexOf(String str, String substr, int n) {
            /*int pos = str.indexOf(substr);
            Toast.makeText(mContext,"Position : "+pos,Toast.LENGTH_SHORT).show();
        while (--str.length() > 0 && pos != -1)
            pos = str.indexOf(substr, pos + 1);
            //return pos;*/


            int length = str.length();
            int pre_index = 0;

            ArrayList<String> phoneNumber = new ArrayList<>();
            int index = str.indexOf(substr);
            while (index >= 0) {
                //Toast.makeText(mContext,"String : "+ str.substring(pre_index,index),Toast.LENGTH_SHORT).show();
                phoneNumber.add(str.substring(pre_index, index));
                pre_index = index + 1;
                if(str.substring(pre_index,length).contains(substr)){
                    index = str.indexOf(substr, pre_index);
                }else{
                    phoneNumber.add(str.substring(pre_index, length-1));
                    index = -1;
                }
            }

            return phoneNumber;
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }


        private void makeAList(final ArrayList<String> contact) {

            final Dialog dialog = new Dialog(mContext);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.contact_list_dialogbox);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.show();

            RecyclerView number_recyclerView = (RecyclerView) dialog.findViewById(R.id.contact_recyclerView);
            ImageView close = (ImageView) dialog.findViewById(R.id.close);

            ContactAdapter contactAdapter;
            contactAdapter = new ContactAdapter(dialog.getLayoutInflater(),mContext);
            contactAdapter.addContact(contact,"phone");
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);

            number_recyclerView.setLayoutManager(layoutManager);
            number_recyclerView.setItemAnimator(new DefaultItemAnimator());
            //important_number_recyclerView.addItemDecoration(new ItemOffsetDecoration(PostActivity.this, R.dimen.item_offset));
            number_recyclerView.setAdapter(contactAdapter);

            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
    }
}
