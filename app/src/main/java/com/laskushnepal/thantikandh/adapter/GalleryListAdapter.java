package com.laskushnepal.thantikandh.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import com.laskushnepal.thantikandh.Interface.RecylerItemClickListner;
import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.activity.Gallery.GalleryDetailActivity;
import com.laskushnepal.thantikandh.model.Gallery.ImageGalleryDTO;

public class GalleryListAdapter  extends  RecyclerView.Adapter<GalleryListAdapter.Holder> {

    Context mContext;
    String blockType;
    Integer pos;
    private LayoutInflater mLayoutInflater;
    private ArrayList<ImageGalleryDTO> mGalleryList = new ArrayList<>();

    private RecylerItemClickListner clickListener;

    public GalleryListAdapter(LayoutInflater inflater, Context mContext) {
        mLayoutInflater = inflater;
        this.mContext = mContext;

    }


    public GalleryListAdapter(LayoutInflater inflater, Context mContext, String blockType, Integer pos) {
        mLayoutInflater = inflater;
        this.mContext = mContext;
        this.blockType = blockType;
        this.pos = pos;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.list_item_gallery, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
            holder.bindGallery(mGalleryList.get(position));
    }

    @Override
    public int getItemCount() {
            return mGalleryList.size();
    }

    public void setClickListener(RecylerItemClickListner itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public void addGalleryList(List<ImageGalleryDTO> gallery) {
        mGalleryList.addAll(gallery);
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //for ImageGallery
        CardView gallery_cardView;
        ImageView singleimage;
        TextView txt_img_name;
        private RecylerItemClickListner itemClickListner;


        public Holder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);

                gallery_cardView = (CardView) itemView.findViewById(R.id.gallery_cardView);
                singleimage = (ImageView) itemView.findViewById(R.id.gallery_iv);
                txt_img_name = (TextView) itemView.findViewById(R.id.gallery_tv);

        }

        public void bindGallery(final ImageGalleryDTO imageGalleryDTO) {

                    //singleimage.setImageResource(imageGalleryDTO.getIcon());
                    Glide.with(mContext).load(imageGalleryDTO.getImage_location()+imageGalleryDTO.getImage())
                            .error(R.drawable.logo).into(singleimage);
                    txt_img_name.setText(imageGalleryDTO.getTitle());

                    gallery_cardView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent i = new Intent(mContext,GalleryDetailActivity.class);
                            i.putExtra("GalleryId",imageGalleryDTO.getId());
                            i.putExtra("GalleryTitle",imageGalleryDTO.getTitle());
                            mContext.startActivity(i);
                        }
                    });
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }


}
