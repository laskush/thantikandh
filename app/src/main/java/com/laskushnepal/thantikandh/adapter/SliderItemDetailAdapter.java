package com.laskushnepal.thantikandh.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import com.laskushnepal.thantikandh.Interface.RecylerItemClickListner;
import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.model.DocumentDTO;
import com.laskushnepal.thantikandh.model.Gallery.SingleGalleryDTO;
import com.laskushnepal.thantikandh.model.ImageDTO;
import com.laskushnepal.thantikandh.model.Slider.SliderDTO;
import com.laskushnepal.thantikandh.utils.DownloadTask;

public class SliderItemDetailAdapter extends RecyclerView.Adapter<SliderItemDetailAdapter.Holder> {

    Context mContext;
    String blockType;
    Integer pos;
    private LayoutInflater mLayoutInflater;
    private ArrayList<SliderDTO> msliderList = new ArrayList<>();
    private ArrayList<SingleGalleryDTO> msiSingleGalleryList = new ArrayList<>();
    private ArrayList<ImageDTO> imageDTOList = new ArrayList<>();
    private ArrayList<DocumentDTO> documentDTOList = new ArrayList<>();

    private RecylerItemClickListner clickListener;

    public SliderItemDetailAdapter(LayoutInflater inflater, Context mContext, String blockType) {
        mLayoutInflater = inflater;
        this.mContext = mContext;
        this.blockType = blockType;

    }


    public SliderItemDetailAdapter(LayoutInflater inflater, Context mContext, String blockType, Integer pos) {
        mLayoutInflater = inflater;
        this.mContext = mContext;
        this.blockType = blockType;
        this.pos = pos;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (blockType.equalsIgnoreCase("SliderGallery")) {
            view = mLayoutInflater.inflate(R.layout.list_item_slider_detail, parent, false);
        }  else if (blockType.equalsIgnoreCase("ImageGallery")) {
            view = mLayoutInflater.inflate(R.layout.list_item_slider_detail, parent, false);
        } else if (blockType.equalsIgnoreCase("SuchanaImageGallery")) {
            view = mLayoutInflater.inflate(R.layout.list_item_slider_detail, parent, false);
        } else if (blockType.equalsIgnoreCase("SuchanaDocumentGallery")) {
            view = mLayoutInflater.inflate(R.layout.list_item_documents, parent, false);
        }
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        if (blockType.equalsIgnoreCase("SliderGallery")) {

            holder.bindSliderGallery(msliderList.get(position));

        } else if (blockType.equalsIgnoreCase("ImageGallery")) {
            holder.bindImageGallery(msiSingleGalleryList.get(position));
            // holder.bindDocument(mDocumentList.get(position));
        }else if (blockType.equalsIgnoreCase("SuchanaImageGallery")) {
            holder.bindSuchanaImageGallery(imageDTOList.get(position));
            // holder.bindDocument(mDocumentList.get(position));
        }else if (blockType.equalsIgnoreCase("SuchanaDocumentGallery")) {
            holder.bindSuchanaDocumentGallery(documentDTOList.get(position));
            // holder.bindDocument(mDocumentList.get(position));
        }

    }

    @Override
    public int getItemCount() {

        if (blockType.equalsIgnoreCase("SliderGallery")) {
            return msliderList.size();
        }  else if (blockType.equalsIgnoreCase("ImageGallery")) {
            return msiSingleGalleryList.size();
        } else if (blockType.equalsIgnoreCase("SuchanaImageGallery")) {
            return imageDTOList.size();
        } else if (blockType.equalsIgnoreCase("SuchanaDocumentGallery")) {
            return documentDTOList.size();
        }
        return 0;
    }

    public void setClickListener(RecylerItemClickListner itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public void addSliderGallery(ArrayList<SliderDTO> slider) {
        msliderList.addAll(slider);
        notifyDataSetChanged();
    }

    public void addImageGallery(ArrayList<SingleGalleryDTO> singleGalleryList) {
        msiSingleGalleryList.addAll(singleGalleryList);
        notifyDataSetChanged();
    }

    public void addSuchanaImageGallery(ArrayList<ImageDTO> imageDTOArrayList) {
        imageDTOList.addAll(imageDTOArrayList);
        notifyDataSetChanged();
    }

    public void addSuchanaDocumentGallery(ArrayList<DocumentDTO> documentDTOArrayList) {
        documentDTOList.addAll(documentDTOArrayList);
        notifyDataSetChanged();
    }




    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //for SliderGallery
        ImageView singleimage;
        TextView txt_img_name;
        RelativeLayout overlay;
        private RecylerItemClickListner itemClickListner;


        TextView news_detail_document_name;
        ImageView document_download_iv;

        public Holder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);
            if (blockType.equalsIgnoreCase("SliderGallery")) {

                singleimage = (ImageView) itemView.findViewById(R.id.slider_detail_imageView);
                singleimage.setScaleType(ImageView.ScaleType.CENTER_CROP);
                txt_img_name = (TextView) itemView.findViewById(R.id.slider_detail_textView);

            }
            if (blockType.equalsIgnoreCase("ImageGallery")) {

                singleimage = (ImageView) itemView.findViewById(R.id.slider_detail_imageView);
                singleimage.setScaleType(ImageView.ScaleType.CENTER_CROP);
                txt_img_name = (TextView) itemView.findViewById(R.id.slider_detail_textView);
                overlay = (RelativeLayout) itemView.findViewById(R.id.overlay);
                overlay.setVisibility(View.GONE);

            }

            if (blockType.equalsIgnoreCase("SuchanaImageGallery")) {

                singleimage = (ImageView) itemView.findViewById(R.id.slider_detail_imageView);
                singleimage.setScaleType(ImageView.ScaleType.CENTER_CROP);
                txt_img_name = (TextView) itemView.findViewById(R.id.slider_detail_textView);
                txt_img_name.setVisibility(View.GONE);
                overlay = (RelativeLayout) itemView.findViewById(R.id.overlay);
                overlay.setVisibility(View.GONE);
            }

            if (blockType.equalsIgnoreCase("SuchanaDocumentGallery")) {
                news_detail_document_name = (TextView) itemView.findViewById(R.id.news_detail_document_name);
                document_download_iv = (ImageView) itemView.findViewById(R.id.document_download_iv);
            }

        }

        public void bindSliderGallery(final SliderDTO sliderDTO) {

            if (blockType.equalsIgnoreCase("SliderGallery")) {

                if (blockType.equalsIgnoreCase("SliderGallery")) {
                    Glide.with(mContext).load(sliderDTO.getLocation()+sliderDTO.getImage()).error(R.drawable.logo).into(singleimage);
                    //singleimage.setImageResource(sliderDTO.getIcon());
                    Log.d("SliderTitle : ",sliderDTO.getTitle());
                    txt_img_name.setText(sliderDTO.getTitle());
                }

            }
        }

        public void bindImageGallery(final SingleGalleryDTO singleGalleryDTO) {

            if (blockType.equalsIgnoreCase("ImageGallery")) {

                if (blockType.equalsIgnoreCase("ImageGallery")) {
                    Glide.with(mContext).load(singleGalleryDTO.getImage_location() + singleGalleryDTO.getTitle()).error(R.drawable.logo).into(singleimage);
                    //singleimage.setImageResource(singleGalleryDTO.getIcon());
                    txt_img_name.setVisibility(View.GONE);
                }

            }
        }


        public void bindSuchanaImageGallery(final ImageDTO imageDTO) {

            if (blockType.equalsIgnoreCase("SuchanaImageGallery")) {

                if (blockType.equalsIgnoreCase("SuchanaImageGallery")) {
                    Glide.with(mContext).load(imageDTO.getUrl() ).error(R.drawable.logo).into(singleimage);
                   // singleimage.setImageResource(imageDTO.getUrl());
                    txt_img_name.setVisibility(View.GONE);
                }

            }
        }

        public void bindSuchanaDocumentGallery(final DocumentDTO documentDTO) {

            if (blockType.equalsIgnoreCase("SuchanaDocumentGallery")) {

                if (blockType.equalsIgnoreCase("SuchanaDocumentGallery")) {
                    news_detail_document_name.setText(documentDTO.getName());
                    news_detail_document_name.setVisibility(View.VISIBLE);

                    document_download_iv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String URL = documentDTO.getURL();
                            new DownloadTask(mContext,URL);
                        }
                    });
                }

            }
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }
}
