package com.laskushnepal.thantikandh.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.laskushnepal.thantikandh.Interface.RecylerItemClickListner;
import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.activity.Wada_karyalaya.WodaEmployeeDetailActivity;
import com.laskushnepal.thantikandh.model.wodakaryala.WodaEmployeeDTO;

import java.util.ArrayList;
import java.util.List;

public class WodaEmployeeListAdapter extends RecyclerView.Adapter<WodaEmployeeListAdapter.Holder> {

    Context mContext;
    String blockType;
    Integer pos;
    private LayoutInflater mLayoutInflater;
    private ArrayList<WodaEmployeeDTO> meEmployeeList = new ArrayList<>();

    private RecylerItemClickListner clickListener;

    public WodaEmployeeListAdapter(LayoutInflater inflater, Context mContext) {
        mLayoutInflater = inflater;
        this.mContext = mContext;

    }


    public WodaEmployeeListAdapter(LayoutInflater inflater, Context mContext, String blockType, Integer pos) {
        mLayoutInflater = inflater;
        this.mContext = mContext;
        this.blockType = blockType;
        this.pos = pos;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.list_item_employee, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bindWodaEmployee(meEmployeeList.get(position));
    }

    @Override
    public int getItemCount() {
        return meEmployeeList.size();
    }

    public void setClickListener(RecylerItemClickListner itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public void addWodaEmployeeList(List<WodaEmployeeDTO> employeeList) {
        meEmployeeList.addAll(employeeList);
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //for ImportantNumber

        CardView employee_cardView;
        TextView list_employee_name,list_employee_designation,list_employee_section;
        private RecylerItemClickListner itemClickListner;


        public Holder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);

            employee_cardView = (CardView) itemView.findViewById(R.id.employee_cardView);
            list_employee_name = (TextView) itemView.findViewById(R.id.list_employee_name);
            list_employee_designation = (TextView) itemView.findViewById(R.id.list_employee_designation);
            list_employee_section = (TextView) itemView.findViewById(R.id.list_employee_section);

        }

        public void bindWodaEmployee(final WodaEmployeeDTO employeeDTO) {

            list_employee_name.setText(employeeDTO.getName());
            list_employee_designation.setText(employeeDTO.getDescription());
            list_employee_section.setText(employeeDTO.getDescription());
            employee_cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i =new Intent(mContext,WodaEmployeeDetailActivity.class);
                    i.putExtra("emp_name",employeeDTO.getName());
                    i.putExtra("emp_designation",employeeDTO.getDescription());
                    i.putExtra("emp_email",employeeDTO.getEmail());
                    i.putExtra("emp_contact",employeeDTO.getContact());
                    i.putExtra("emp_image_url",employeeDTO.getImage());
                    mContext.startActivity(i);


                }
            });

        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }
}
