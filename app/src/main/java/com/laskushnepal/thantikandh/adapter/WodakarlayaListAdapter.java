package com.laskushnepal.thantikandh.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.laskushnepal.thantikandh.Interface.RecylerItemClickListner;
import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.activity.Wada_karyalaya.WodaKaryalayaActivity;
import com.laskushnepal.thantikandh.activity.Wada_karyalaya.WodaKaryalayaEmployeeActivity;
import com.laskushnepal.thantikandh.model.wodakaryala.WodaDTO;

public class WodakarlayaListAdapter extends RecyclerView.Adapter<WodakarlayaListAdapter.Holder> {

    Context mContext;
    String blockType;
    Integer pos;
    private LayoutInflater mLayoutInflater;
    private ArrayList<WodaDTO> wodaList = new ArrayList<>();

    private RecylerItemClickListner clickListener;

    public WodakarlayaListAdapter(LayoutInflater inflater, Context mContext) {
        mLayoutInflater = inflater;
        this.mContext = mContext;

    }


    public WodakarlayaListAdapter(LayoutInflater inflater, Context mContext, String blockType, Integer pos) {
        mLayoutInflater = inflater;
        this.mContext = mContext;
        this.blockType = blockType;
        this.pos = pos;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.list_item_wodakaryalaya, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bindWoda(wodaList.get(position));
    }

    @Override
    public int getItemCount() {
        return wodaList.size();
    }

    public void setClickListener(RecylerItemClickListner itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public void addWodaList(List<WodaDTO> wodaDTOList) {
        wodaList.addAll(wodaDTOList);
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //for ImportantNumber

        CardView wodakaryalaya_cardView;
        TextView wodakaryalaya_textView;
        private RecylerItemClickListner itemClickListner;


        public Holder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);

            wodakaryalaya_cardView = (CardView) itemView.findViewById(R.id.wodakaryalaya_cardView);
            wodakaryalaya_textView = (TextView) itemView.findViewById(R.id.wodakaryalaya_textView);

        }

        public void bindWoda(final WodaDTO wodaDTO) {

            wodakaryalaya_textView.setText(wodaDTO.getName());


            wodakaryalaya_cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(mContext, WodaKaryalayaEmployeeActivity.class);
                    i.putExtra("id", wodaDTO.getId());
                    mContext.startActivity(i);
                }
            });


        }

        @Override
        public void onClick(View view) {

        }
    }
}
