package com.laskushnepal.thantikandh.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.laskushnepal.thantikandh.Interface.RecylerItemClickListner;
import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.activity.SuchanaJankari.SuchanaDetailActivity;
import com.laskushnepal.thantikandh.model.DocumentDTO;
import com.laskushnepal.thantikandh.model.ImageDTO;
import com.laskushnepal.thantikandh.model.News.NewsDTO;
import com.laskushnepal.thantikandh.utils.ParseUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SuchanaJankariListAdapter  extends  RecyclerView.Adapter<SuchanaJankariListAdapter.Holder>  {

    Context mContext;
    String blockType;
    Integer pos;
    private LayoutInflater mLayoutInflater;
    private ArrayList<NewsDTO> mnewsList = new ArrayList<>();
    String categoryTitle;
    ArrayList<ImageDTO> imageList = new ArrayList<>();
    ArrayList<DocumentDTO> documentList = new ArrayList<>();

    private RecylerItemClickListner clickListener;

    public SuchanaJankariListAdapter(LayoutInflater inflater, Context mContext) {
        mLayoutInflater = inflater;
        this.mContext = mContext;

    }


    public SuchanaJankariListAdapter(LayoutInflater inflater, Context mContext, String blockType, Integer pos) {
        mLayoutInflater = inflater;
        this.mContext = mContext;
        this.blockType = blockType;
        this.pos = pos;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.list_item_news, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bindNews(mnewsList.get(position));
    }

    @Override
    public int getItemCount() {
        return mnewsList.size();
    }

    public void setClickListener(RecylerItemClickListner itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public void addNewsList(List<NewsDTO> news,String title) {
        mnewsList.addAll(news);
        categoryTitle = title;
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //for ImageGallery
        CardView news_cardView;
        TextView news_title,news_date_tv ;


        public Holder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);

            news_cardView = (CardView) itemView.findViewById(R.id.news_cardView);
            news_title = (TextView) itemView.findViewById(R.id.news_title);
            news_date_tv = (TextView) itemView.findViewById(R.id.news_date_tv);
        }

        public void bindNews(final NewsDTO newsDTO) {

            news_title.setText(newsDTO.getTitle());
            news_date_tv.setText(mContext.getResources().getString(R.string.submit_date) + ParseUtil.parseDateString(newsDTO.getSt_date()));

            //getImageList();
           // getDocumentList();

          /*  documentList = new ArrayList<>();
            imageList = new ArrayList<>();

            String link1 = newsDTO.getImage().getImg1();
            if(link1.length()>0){
                //Toast.makeText(mContext,"link1 : "+ link1,Toast.LENGTH_SHORT).show();
                String documentName1 = link1.substring(link1.lastIndexOf("/"));

                String extension1 = link1.substring(link1.lastIndexOf("."));
                if(extension1.equalsIgnoreCase(".pdf")){
                  //  Toast.makeText(mContext,"Added to document",Toast.LENGTH_SHORT).show();
                    documentList.add(new DocumentDTO(documentName1,link1));
                }else{
                    //Toast.makeText(mContext,"Added to image",Toast.LENGTH_SHORT).show();
                    imageList.add(new ImageDTO(documentName1,link1));
                }
            }



            String link2 = newsDTO.getImage().getImg2();
            if(link2.length()>0){
                //Toast.makeText(mContext,"link2 : "+ link2,Toast.LENGTH_SHORT).show();
                String documentName2 = link2.substring(link2.lastIndexOf("/"));;
                String extension2 = link2.substring(link2.lastIndexOf("."));
                if(extension2.equalsIgnoreCase(".pdf")){
                    //Toast.makeText(mContext,"Added to document",Toast.LENGTH_SHORT).show();
                    documentList.add(new DocumentDTO(documentName2,link2));
                }else{
                    //Toast.makeText(mContext,"Added to image",Toast.LENGTH_SHORT).show();
                    imageList.add(new ImageDTO(documentName2,link2));
                }

            }

            String link3 = newsDTO.getImage().getImg3();
            if(link3.length()>0){
                //Toast.makeText(mContext,"link3 : "+ link3,Toast.LENGTH_SHORT).show();
                String documentName3 = link3.substring(link3.lastIndexOf("/"));
                String extension3 = link3.substring(link3.lastIndexOf("."));
                if(extension3.equalsIgnoreCase(".pdf")){
                    //Toast.makeText(mContext,"Added to document",Toast.LENGTH_SHORT).show();
                    documentList.add(new DocumentDTO(documentName3,link3));
                }else{
                    //Toast.makeText(mContext,"Added to image",Toast.LENGTH_SHORT).show();
                    imageList.add(new ImageDTO(documentName3,link3));
                }
            }*/



//            Toast.makeText(mContext,"imageList : "+ imageList.size(),Toast.LENGTH_SHORT).show();
//            Toast.makeText(mContext,"documentList : "+ documentList.size(),Toast.LENGTH_SHORT).show();

                news_cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(mContext,SuchanaDetailActivity.class);
                    i.putExtra("category",categoryTitle);
                    i.putExtra("title",newsDTO.getTitle());
                    i.putExtra("date",newsDTO.getSt_date());
                    i.putExtra("Image1", newsDTO.getImage().getImg1());
                    i.putExtra("Image2", newsDTO.getImage().getImg2());
                    i.putExtra("Image3", newsDTO.getImage().getImg3());
//                    i.putParcelableArrayListExtra("imageList",imageList);
//                    i.putParcelableArrayListExtra("documentList",documentList);

                    mContext.startActivity(i);
                }
            });
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

   /* private ArrayList<DocumentDTO> getDocumentList() {
        documentList = new ArrayList<>();
        documentList.add(new DocumentDTO("Dcoument 1"));
        documentList.add(new DocumentDTO("Cocument 2"));
        documentList.add(new DocumentDTO("Document 3"));

        return  documentList;
    }*/

   /* private ArrayList<ImageDTO> getImageList() {
        imageList = new ArrayList<>();
        imageList.add(new ImageDTO(mContext.getResources().getString(R.string.slider_title1),R.drawable.slider_image1));
        imageList.add(new ImageDTO(mContext.getResources().getString(R.string.slider_title2),R.drawable.slider_image2));
        imageList.add(new ImageDTO(mContext.getResources().getString(R.string.slider_title3),R.drawable.slider_image2));

        return imageList;
    }*/

}
