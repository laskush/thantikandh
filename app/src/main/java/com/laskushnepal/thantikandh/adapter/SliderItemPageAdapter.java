package com.laskushnepal.thantikandh.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.model.DocumentDTO;
import com.laskushnepal.thantikandh.model.Gallery.SingleGalleryDTO;
import com.laskushnepal.thantikandh.model.ImageDTO;
import com.laskushnepal.thantikandh.model.Slider.SliderDTO;
import com.laskushnepal.thantikandh.utils.PdfWebViewClient;

import java.util.ArrayList;
import java.util.List;

public class SliderItemPageAdapter extends PagerAdapter {

    //conatsnt tags
    public static String TAG = SliderItemPageAdapter.class.getSimpleName();

    //variables
    private List<SliderDTO> sliderList;
    private List<SingleGalleryDTO> singleGalleryList;
    private List<ImageDTO> imageDTOList;
    private List<DocumentDTO> documentDTOList;

    private LayoutInflater inflater;
    private Context context;
    private int type;
    private String type1 = "";

    //views
    View myImageLayout = null;

    public SliderItemPageAdapter(Context context, ArrayList<SliderDTO> sliders, int type) {
        this.context = context;
        this.sliderList = sliders;
        this.type = type;
        inflater = LayoutInflater.from(context);
    }

    public SliderItemPageAdapter(Context context, ArrayList<SingleGalleryDTO> singleGalleryList) {
        this.context = context;
        this.type = 1;
        this.singleGalleryList = singleGalleryList;
        inflater = LayoutInflater.from(context);
    }

    public SliderItemPageAdapter(Context context, ArrayList<ImageDTO> sliders, String type) {
        this.context = context;
        this.imageDTOList = sliders;
        this.type = 2;
        inflater = LayoutInflater.from(context);
    }

    public SliderItemPageAdapter(Context context, ArrayList<DocumentDTO> sliders, boolean type) {
        this.context = context;
        this.documentDTOList = sliders;
        this.type = 3;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        if (type == 0) {
            return sliderList.size();
        } else if (type == 1) {
            return singleGalleryList.size();
        } else if (type == 2) {
            return imageDTOList.size();
        } else if (type == 3) {
            return documentDTOList.size();
        }
        return type;
    }


    @Override
    public Object instantiateItem(ViewGroup view, int position) {

        if (type == 0) {

            myImageLayout = inflater.inflate(R.layout.pager_item_image, view, false);
            // PhotoView myImage = (PhotoView) myImageLayout.findViewById(R.id.image);
            ImageView myImage = (ImageView) myImageLayout.findViewById(R.id.image);
            TextView txt_title = (TextView) myImageLayout.findViewById(R.id.txt_title);
            txt_title.setText(sliderList.get(position).getTitle());
            final ProgressBar progressBar = (ProgressBar) myImageLayout.findViewById(R.id.activitiesProgressBar);
            String url = sliderList.get(position).getLocation() + sliderList.get(position).getImage();
            myImage.setScaleType(ImageView.ScaleType.FIT_XY);


            //myImage.setImageDrawable(sliderList.get(position).getIcon());
            txt_title.setText(sliderList.get(position).getTitle());

            Glide.with(context).load(url).error(R.drawable.logo).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    progressBar.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                   Log.d(TAG,"Visibility: "+progressBar.getVisibility());
                    progressBar.setVisibility(View.GONE);
                    return false;
                }
            })
//                    .placeholder(R.id.activitiesProgressBar)
                    .placeholder(R.drawable.logo)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE).into(myImage);

            view.addView(myImageLayout, 0);
        }
        if (type == 1) {

            myImageLayout = inflater.inflate(R.layout.pager_item_image, view, false);
            // PhotoView myImage = (PhotoView) myImageLayout.findViewById(R.id.image);
            ImageView myImage = (ImageView) myImageLayout.findViewById(R.id.image);
            TextView txt_title = (TextView) myImageLayout.findViewById(R.id.txt_title);
            //txt_title.setText(sliderList.get(position).getName());
            final ProgressBar progressBar = (ProgressBar) myImageLayout.findViewById(R.id.activitiesProgressBar);
            String url = singleGalleryList.get(position).getImage_location() + singleGalleryList.get(position).getTitle();
            myImage.setScaleType(ImageView.ScaleType.FIT_XY);

            //txt_title.setText(sliderList.get(position).getName());
            txt_title.setVisibility(View.GONE);

            Glide.with(context).load(url).error(R.drawable.logo).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    progressBar.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                   Log.d(TAG,"Visibility: "+progressBar.getVisibility());
                    progressBar.setVisibility(View.GONE);
                    return false;
                }
            })
//                    .placeholder(R.id.activitiesProgressBar)
                    .placeholder(R.drawable.logo)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE).into(myImage);

            view.addView(myImageLayout, 0);
        }

        if (type == 2) {

            myImageLayout = inflater.inflate(R.layout.pager_item_image, view, false);
            com.laskushnepal.thantikandh.testapp.utils.photoview.PhotoView myImage = (com.laskushnepal.thantikandh.testapp.utils.photoview.PhotoView) myImageLayout.findViewById(R.id.image);
            //ImageView myImage = (ImageView) myImageLayout.findViewById(R.id.image);
            TextView txt_title = (TextView) myImageLayout.findViewById(R.id.txt_title);

            final ProgressBar progressBar = (ProgressBar) myImageLayout.findViewById(R.id.activitiesProgressBar);
            // String url = singleGalleryList.get(position).getImage_location()+singleGalleryList.get(position).getTitle();
            myImage.setScaleType(ImageView.ScaleType.FIT_XY);

            txt_title.setVisibility(View.GONE);
           /* Glide.with(context).load(imageDTOList.get(position).getUrl()).error(R.drawable.logo).placeholder(R.drawable.logo)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE).into(myImage);
            progressBar.setVisibility(View.GONE);*/

            Glide.with(context).load(imageDTOList.get(position).getUrl()).error(R.drawable.logo).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    progressBar.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                   Log.d(TAG,"Visibility: "+progressBar.getVisibility());
                    progressBar.setVisibility(View.GONE);
                    return false;
                }
            })
//                    .placeholder(R.id.activitiesProgressBar)
                    .placeholder(R.drawable.logo)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE).into(myImage);

            view.addView(myImageLayout, 0);
        }


        if (type == 3) {

            myImageLayout = inflater.inflate(R.layout.pager_item_document, view, false);

            String googleDocsUrl = "http://docs.google.com/viewer?embedded=true&url=";
            final WebView web = (WebView) myImageLayout.findViewById(R.id.webView);
            web.clearCache(true);
            WebSettings settings = web.getSettings();
            settings.setJavaScriptEnabled(true);

            web.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
            PdfWebViewClient pdfWebViewClient = new PdfWebViewClient(context, web);
            String url = documentDTOList.get(position).getURL();
            pdfWebViewClient.loadPdfUrl(googleDocsUrl + Uri.encode(url));

            view.addView(myImageLayout, 0);
        }

        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

}
