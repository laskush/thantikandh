package com.laskushnepal.thantikandh.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.laskushnepal.thantikandh.Interface.RecylerItemClickListner;
import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.activity.SuchanaJankari.SuchanaGalleryActivity;
import com.laskushnepal.thantikandh.model.DocumentDTO;
import com.laskushnepal.thantikandh.utils.DownloadTask;

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.Holder>{

    Context mContext;
    String blockType;
    Integer pos;
    private LayoutInflater mLayoutInflater;
    private ArrayList<DocumentDTO> mDocumentList = new ArrayList<>();
    String CATEGORY;

    private RecylerItemClickListner clickListener;

    public DocumentAdapter(LayoutInflater inflater, Context mContext) {
        mLayoutInflater = inflater;
        this.mContext = mContext;
    }


    public DocumentAdapter(LayoutInflater inflater, Context mContext, String blockType, Integer pos) {
        mLayoutInflater = inflater;
        this.mContext = mContext;
        this.blockType = blockType;
        this.pos = pos;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = mLayoutInflater.inflate(R.layout.list_item_documents, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bindDocument(mDocumentList.get(position),position);
    }

    @Override
    public int getItemCount() {
        return mDocumentList.size();
    }

    public void setClickListener(RecylerItemClickListner itemClickListener) {
        this.clickListener = itemClickListener;
    }



    public void addDocument(ArrayList<DocumentDTO> documentList,String category) {
        mDocumentList.addAll(documentList);
        CATEGORY = category;
        notifyDataSetChanged();
    }


    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //for SliderGallery

        TextView news_detail_document_name;
        CardView news_detail_document_cardView;
        ImageView document_download_iv;
        private RecylerItemClickListner itemClickListner;


        public Holder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);

            news_detail_document_name = (TextView) itemView.findViewById(R.id.news_detail_document_name);
            document_download_iv = (ImageView) itemView.findViewById(R.id.document_download_iv);
            news_detail_document_cardView = (CardView) itemView.findViewById(R.id.news_detail_document_cardView);
        }


        public void bindDocument(final DocumentDTO documentDTO, final int position) {
            news_detail_document_name.setText(documentDTO.getName());
            news_detail_document_cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, SuchanaGalleryActivity.class);
                    i.putExtra("position",position);
                    i.putExtra("category",CATEGORY);
                    i.putParcelableArrayListExtra("documentList",(ArrayList<? extends Parcelable>) mDocumentList);
                    mContext.startActivity(i);
                }
            });

            document_download_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String URL = documentDTO.getURL();
                    new DownloadTask(mContext,URL);
                }
            });
        }


        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }
}
