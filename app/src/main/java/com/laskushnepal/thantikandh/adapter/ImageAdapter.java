package com.laskushnepal.thantikandh.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.bumptech.glide.Glide;
import com.laskushnepal.thantikandh.Interface.RecylerItemClickListner;
import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.activity.SuchanaJankari.SuchanaGalleryActivity;
import com.laskushnepal.thantikandh.model.ImageDTO;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.Holder> {

    Context mContext;
    String blockType;
    Integer pos;
    private LayoutInflater mLayoutInflater;
    private ArrayList<ImageDTO> mimageList = new ArrayList<>();
    String CATEGORY;


    private RecylerItemClickListner clickListener;

    public ImageAdapter(LayoutInflater inflater, Context mContext) {
        mLayoutInflater = inflater;
        this.mContext = mContext;
    }


    public ImageAdapter(LayoutInflater inflater, Context mContext, String blockType, Integer pos) {
        mLayoutInflater = inflater;
        this.mContext = mContext;
        this.blockType = blockType;
        this.pos = pos;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = mLayoutInflater.inflate(R.layout.list_item_slider_detail, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bindImage(mimageList.get(position),position);
    }

    @Override
    public int getItemCount() {
        return mimageList.size();
    }

    public void setClickListener(RecylerItemClickListner itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public void addSliderGallery(ArrayList<ImageDTO> image) {
        mimageList.addAll(image);
        notifyDataSetChanged();
    }

    public void addmage(ArrayList<ImageDTO> imageList,String category) {
        mimageList.addAll(imageList);
        CATEGORY = category;
        notifyDataSetChanged();
    }


    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //for SliderGallery
        CardView slider_detail_cardView;
        RelativeLayout overlay;
        ImageView singleimage;
        TextView txt_img_name;
        private RecylerItemClickListner itemClickListner;


        public Holder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);

            slider_detail_cardView = (CardView) itemView.findViewById(R.id.slider_detail_cardView);
            overlay = (RelativeLayout) itemView.findViewById(R.id.overlay);
            overlay.setVisibility(View.GONE);
            singleimage = (ImageView) itemView.findViewById(R.id.slider_detail_imageView);
            singleimage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            txt_img_name = (TextView) itemView.findViewById(R.id.slider_detail_textView);
            txt_img_name.setVisibility(View.GONE);
        }


        public void bindImage(final ImageDTO imageDTO, final int position) {

                //singleimage.setImageResource(imageDTO.getIcon());
                Glide.with(mContext).load(imageDTO.getUrl() ).error(R.drawable.logo).into(singleimage);
                txt_img_name.setText(imageDTO.getName());

            slider_detail_cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(mContext, SuchanaGalleryActivity.class);
                    i.putExtra("position",position);
                    i.putExtra("category",CATEGORY);
                    i.putParcelableArrayListExtra("imageList",(ArrayList<? extends Parcelable>) mimageList);
                    mContext.startActivity(i);
                }
            });

        }


        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }
}
