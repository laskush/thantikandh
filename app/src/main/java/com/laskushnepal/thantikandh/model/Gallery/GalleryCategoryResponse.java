package com.laskushnepal.thantikandh.model.Gallery;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GalleryCategoryResponse {

    @SerializedName("status")
    String status;

    @SerializedName("records")
    ArrayList<ImageGalleryDTO> imageGalleryList = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<ImageGalleryDTO> getImageGalleryList() {
        return imageGalleryList;
    }

    public void setImageGalleryList(ArrayList<ImageGalleryDTO> imageGalleryList) {
        this.imageGalleryList = imageGalleryList;
    }
}
