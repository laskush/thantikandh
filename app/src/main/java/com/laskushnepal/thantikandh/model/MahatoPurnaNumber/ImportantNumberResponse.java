package com.laskushnepal.thantikandh.model.MahatoPurnaNumber;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ImportantNumberResponse {

    @SerializedName("status")
    String status;

    @SerializedName("records")
    ArrayList<ImportantNumberDTO> importantNumberList = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<ImportantNumberDTO> getImportantNumberList() {
        return importantNumberList;
    }

    public void setImportantNumberList(ArrayList<ImportantNumberDTO> importantNumberList) {
        this.importantNumberList = importantNumberList;
    }
}
