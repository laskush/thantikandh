package com.laskushnepal.thantikandh.model.Gallery;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GalleryImageResponse {

    @SerializedName("status")
    String status;

    @SerializedName("records")
    ArrayList<SingleGalleryDTO> singleGalleryList = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<SingleGalleryDTO> getSingleGalleryList() {
        return singleGalleryList;
    }

    public void setSingleGalleryList(ArrayList<SingleGalleryDTO> singleGalleryList) {
        this.singleGalleryList = singleGalleryList;
    }
}
