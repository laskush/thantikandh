package com.laskushnepal.thantikandh.model.Slider;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SliderResponse {

    @SerializedName("status")
    String status;

    @SerializedName("records")
     ArrayList<SliderDTO> sliderList = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<SliderDTO> getSliderList() {
        return sliderList;
    }

    public void setSliderList(ArrayList<SliderDTO> sliderList) {
        this.sliderList = sliderList;
    }
}
