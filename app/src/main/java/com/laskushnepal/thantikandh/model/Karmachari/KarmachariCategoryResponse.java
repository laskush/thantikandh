package com.laskushnepal.thantikandh.model.Karmachari;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

public class KarmachariCategoryResponse {

    @SerializedName("status")
    String status;

    @SerializedName("records")
    ArrayList<KarmachariDTO> karmachariList = new ArrayList<>();


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<KarmachariDTO> getKarmachariList() {
        return karmachariList;
    }

    public void setKarmachariList(ArrayList<KarmachariDTO> karmachariList) {
        this.karmachariList = karmachariList;
    }
}
