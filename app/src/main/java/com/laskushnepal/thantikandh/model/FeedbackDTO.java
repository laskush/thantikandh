package com.laskushnepal.thantikandh.model;

import com.google.gson.annotations.SerializedName;

public class FeedbackDTO {

    //@SerializedName("name")
    String name;
    //@SerializedName("email")
    String email;
    //@SerializedName("mobile_no")
    String mobile_no;
    //@SerializedName("subject")
    String subject;
    //@SerializedName("description")
    String description;

    public FeedbackDTO() {
    }

    public FeedbackDTO(String name, String email, String mobile_no, String subject, String description) {
        this.name = name;
        this.email = email;
        this.mobile_no = mobile_no;
        this.subject = subject;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
