package com.laskushnepal.thantikandh.model;

import android.os.Parcel;
import android.os.Parcelable;

public class DocumentDTO implements Parcelable{

    String name;
    String URL;


    public DocumentDTO(String name, String URL) {
        this.name = name;
        this.URL = URL;
    }

    protected DocumentDTO(Parcel in) {
        name = in.readString();
        URL = in.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public static final Creator<DocumentDTO> CREATOR = new Creator<DocumentDTO>() {
        @Override
        public DocumentDTO createFromParcel(Parcel in) {
            return new DocumentDTO(in);
        }

        @Override
        public DocumentDTO[] newArray(int size) {
            return new DocumentDTO[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(URL);
    }
}
