package com.laskushnepal.thantikandh.model.News;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NewsResponse {

    @SerializedName("status")
    String status;

    @SerializedName("records")
    ArrayList<NewsDTO> newsList = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<NewsDTO> getNewsList() {
        return newsList;
    }

    public void setNewsList(ArrayList<NewsDTO> newsList) {
        this.newsList = newsList;
    }
}
