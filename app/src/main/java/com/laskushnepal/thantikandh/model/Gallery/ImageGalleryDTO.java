package com.laskushnepal.thantikandh.model.Gallery;

import com.google.gson.annotations.SerializedName;

public class ImageGalleryDTO{

        @SerializedName("id")
        private String id;

        @SerializedName("image")
        private String image;

        @SerializedName("title")
        private String title;

        @SerializedName("thumb_location")
        private String thumb_location;

        @SerializedName("image_location")
        private String image_location;

        @SerializedName("slug")
        private String slug;

        @SerializedName("sort")
        String sort;

        @SerializedName("status")
        private String status;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumb_location() {
        return thumb_location;
    }

    public void setThumb_location(String thumb_location) {
        this.thumb_location = thumb_location;
    }

    public String getImage_location() {
        return image_location;
    }

    public void setImage_location(String image_location) {
        this.image_location = image_location;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
