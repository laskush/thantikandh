package com.laskushnepal.thantikandh.model.Slider;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SliderDTO implements Parcelable {

    private String id;

    @SerializedName("title")
    private String title;
    @SerializedName("image")
    private String image;
    @SerializedName("caption")
    private String caption;
    @SerializedName("location")
    private String location;

    protected SliderDTO(Parcel in) {
        id = in.readString();
        title = in.readString();
        image = in.readString();
        caption = in.readString();
        location = in.readString();
    }

    public static final Creator<SliderDTO> CREATOR = new Creator<SliderDTO>() {
        @Override
        public SliderDTO createFromParcel(Parcel in) {
            return new SliderDTO(in);
        }

        @Override
        public SliderDTO[] newArray(int size) {
            return new SliderDTO[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(image);
        parcel.writeString(caption);
        parcel.writeString(location);
    }
}
