package com.laskushnepal.thantikandh.model.wodakaryala;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WodakaryalaResponse {


    @SerializedName("status")
    String status;

    @SerializedName("records")
    ArrayList<WodaDTO> wodaList = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<WodaDTO> getWodaList() {
        return wodaList;
    }

    public void setWodaList(ArrayList<WodaDTO> wodaList) {
        this.wodaList = wodaList;
    }
}