package com.laskushnepal.thantikandh.model.Karmachari;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EmployeeResponse {

    @SerializedName("status")
    String status;

    @SerializedName("records")
    ArrayList<EmployeeDTO> employeeList = new ArrayList<>();


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<EmployeeDTO> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(ArrayList<EmployeeDTO> employeeList) {
        this.employeeList = employeeList;
    }
}
