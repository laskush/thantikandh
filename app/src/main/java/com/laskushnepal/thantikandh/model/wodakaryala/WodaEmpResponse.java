package com.laskushnepal.thantikandh.model.wodakaryala;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WodaEmpResponse {

    @SerializedName("status")
    String status;

    @SerializedName("records")
    ArrayList<WodaEmployeeDTO> wodaEmployeeList = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<WodaEmployeeDTO> getWodaEmployeeList() {
        return wodaEmployeeList;
    }

    public void setWodaEmployeeList(ArrayList<WodaEmployeeDTO> wodaEmployeeList) {
        this.wodaEmployeeList = wodaEmployeeList;
    }
}
