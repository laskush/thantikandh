package com.laskushnepal.thantikandh.model.News;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NewsCategoryResponse {

    @SerializedName("status")
    String status;

    @SerializedName("records")
    ArrayList<NewsCategoryDTO> newsCategoryList = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<NewsCategoryDTO> getNewsCategoryList() {
        return newsCategoryList;
    }

    public void setNewsCategoryList(ArrayList<NewsCategoryDTO> newsCategoryList) {
        this.newsCategoryList = newsCategoryList;
    }
}
