package com.laskushnepal.thantikandh.model.News;

import com.google.gson.annotations.SerializedName;
import com.laskushnepal.thantikandh.model.ImageDTO;

public class NewsDTO {

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("slug")
    private String slug;

    @SerializedName("description")
    private String description;

    @SerializedName("content")
    private String content;

    @SerializedName("st_date")
    private String st_date;

    @SerializedName("end_date")
    private String end_date;


    @SerializedName("image")
    private NewsImage image;

    @SerializedName("sort")
    private String sort;

    @SerializedName("status")
    private String status;

    @SerializedName("category")
    private String category;

    @SerializedName("isNepali")
    private String isNepali;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public NewsImage getImage() {
        return image;
    }

    public void setImage(NewsImage image) {
        this.image = image;
    }

    public String getSt_date() {
        return st_date;
    }

    public void setSt_date(String st_date) {
        this.st_date = st_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIsNepali() {
        return isNepali;
    }

    public void setIsNepali(String isNepali) {
        this.isNepali = isNepali;
    }
}
