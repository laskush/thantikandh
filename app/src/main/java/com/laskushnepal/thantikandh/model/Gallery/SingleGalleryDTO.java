package com.laskushnepal.thantikandh.model.Gallery;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SingleGalleryDTO implements Parcelable {

    @SerializedName("id")
    String id;

    @SerializedName("gallery_cate")
    String gallery_cate;

    @SerializedName("description")
    String description;

    @SerializedName("title")
    String title;

    @SerializedName("thumb_location")
    private String thumb_location;

    @SerializedName("image_location")
    private String image_location;

    @SerializedName("status")
    private String status;

    @SerializedName("sort")
    private String sort;


    protected SingleGalleryDTO(Parcel in) {
        id = in.readString();
        gallery_cate = in.readString();
        description = in.readString();
        title = in.readString();
        thumb_location = in.readString();
        image_location = in.readString();
        status = in.readString();
        sort = in.readString();
    }

    public static final Creator<SingleGalleryDTO> CREATOR = new Creator<SingleGalleryDTO>() {
        @Override
        public SingleGalleryDTO createFromParcel(Parcel in) {
            return new SingleGalleryDTO(in);
        }

        @Override
        public SingleGalleryDTO[] newArray(int size) {
            return new SingleGalleryDTO[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGallery_cate() {
        return gallery_cate;
    }

    public void setGallery_cate(String gallery_cate) {
        this.gallery_cate = gallery_cate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumb_location() {
        return thumb_location;
    }

    public void setThumb_location(String thumb_location) {
        this.thumb_location = thumb_location;
    }

    public String getImage_location() {
        return image_location;
    }

    public void setImage_location(String image_location) {
        this.image_location = image_location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(gallery_cate);
        parcel.writeString(description);
        parcel.writeString(title);
        parcel.writeString(thumb_location);
        parcel.writeString(image_location);
        parcel.writeString(status);
        parcel.writeString(sort);
    }
}
