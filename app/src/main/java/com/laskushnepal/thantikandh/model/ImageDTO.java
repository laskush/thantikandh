package com.laskushnepal.thantikandh.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ImageDTO implements Parcelable {
    String name;
    String Url;

    public ImageDTO(String name, String url) {
        this.name = name;
        Url = url;
    }

    protected ImageDTO(Parcel in) {
        name = in.readString();
        Url = in.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public static final Creator<ImageDTO> CREATOR = new Creator<ImageDTO>() {
        @Override
        public ImageDTO createFromParcel(Parcel in) {
            return new ImageDTO(in);
        }

        @Override
        public ImageDTO[] newArray(int size) {
            return new ImageDTO[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(Url);
    }
}
