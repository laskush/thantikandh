package com.laskushnepal.thantikandh.model.MahatoPurnaNumber;

import com.google.gson.annotations.SerializedName;

public class ImportantNumberDTO {

    @SerializedName("name")
    String title;

    @SerializedName("number")
    String number;

    public ImportantNumberDTO(String title, String number) {
        this.title = title;
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
