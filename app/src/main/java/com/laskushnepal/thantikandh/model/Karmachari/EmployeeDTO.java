package com.laskushnepal.thantikandh.model.Karmachari;

import com.google.gson.annotations.SerializedName;

public class EmployeeDTO {

   @SerializedName("id")
   String id;
   @SerializedName("name")
   String name;
   @SerializedName("slug")
   String slug;
   @SerializedName("description")
   String description;
    @SerializedName("section")
    String section;
   @SerializedName("image")
   String image;
   @SerializedName("type")
   String type;
   @SerializedName("sort")
   String sort;
   @SerializedName("status")
    String status;

   @SerializedName("email")
   String email;

   @SerializedName("phone")
   String phone;

   @SerializedName("mobile")
   String mobile;

    public EmployeeDTO(String name, String description, String section, String image, String email, String phone, String mobile) {
        this.name = name;
        this.description = description;
        this.section = section;
        this.image = image;
        this.email = email;
        this.phone = phone;
        this.mobile = mobile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
