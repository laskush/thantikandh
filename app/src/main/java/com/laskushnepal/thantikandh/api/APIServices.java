package com.laskushnepal.thantikandh.api;

import com.laskushnepal.thantikandh.model.FeedbackDTO;
import com.laskushnepal.thantikandh.model.Gallery.GalleryCategoryResponse;
import com.laskushnepal.thantikandh.model.Gallery.GalleryImageResponse;
import com.laskushnepal.thantikandh.model.Karmachari.EmployeeResponse;
import com.laskushnepal.thantikandh.model.Karmachari.KarmachariCategoryResponse;
import com.laskushnepal.thantikandh.model.MahatoPurnaNumber.ImportantNumberResponse;
import com.laskushnepal.thantikandh.model.News.NewsCategoryResponse;
import com.laskushnepal.thantikandh.model.News.NewsResponse;
import com.laskushnepal.thantikandh.model.Slider.SliderResponse;
import com.laskushnepal.thantikandh.model.SujhapDTO;
import com.laskushnepal.thantikandh.model.wodakaryala.WodaEmpResponse;
import com.laskushnepal.thantikandh.model.wodakaryala.WodakaryalaResponse;
import com.laskushnepal.thantikandh.utils.AppText;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIServices {

    @Headers({
            "Content-Type: application/json"
    })


    @GET(AppText.SLIDER_URL)
    Call<SliderResponse> getSlider(
    );

    @GET(AppText.GALLERY_CATEGORY_URL)
    Call<GalleryCategoryResponse> getGalleryCategory(
    );

    @GET(AppText.IMAGE_GALLERY_URL + "{category_id}")
    Call<GalleryImageResponse> getImageGallery(
            @Path(value = "category_id", encoded = true) String userId
    );

    @GET(AppText.NEWS_CATEGORY_URL)
    Call<NewsCategoryResponse> getNewsCategory(
    );

    @GET(AppText.NEWS_URL + "{slug}")
    Call<NewsResponse> getNews(
            @Path(value = "slug", encoded = true) String slug
    );


    @GET(AppText.REPORT_CATEGORY_URL)
    Call<NewsCategoryResponse> getReportCategory(
    );

    @GET(AppText.REPORT_URL + "{slug}")
    Call<NewsResponse> getReport(
            @Path(value = "slug", encoded = true) String slug
    );

    @GET(AppText.BIDHUTIYA_CATEGORY_URL)
    Call<NewsCategoryResponse> getBidhutiyaCategory(
    );

    @GET(AppText.BIDHUTIYA_URL + "{slug}")
    Call<NewsResponse> getBidhutiya(
            @Path(value = "slug", encoded = true) String slug
    );


    @GET(AppText.KAREKARAM_CATEGORY_URL)
    Call<NewsCategoryResponse> getKarekaramCategory(
    );

    @GET(AppText.KAREKARAM_URL + "{slug}")
    Call<NewsResponse> getKarekaram(
            @Path(value = "slug", encoded = true) String slug
    );


    @GET(AppText.KARMACHARI_CATEGORY_URL)
    Call<KarmachariCategoryResponse> getKarmachariCategory(
    );

    @GET(AppText.KARMACHARI_URL + "{slug}")
    Call<EmployeeResponse> getKarmachari(
            @Path(value = "slug", encoded = true) String slug
    );

    @GET(AppText.JANPRATINIDHI_CATEGORY_URL)
    Call<KarmachariCategoryResponse> getJanPratinidhiCategory(
    );

    @GET(AppText.JANPRATINIDHI_URL + "{slug}")
    Call<EmployeeResponse> getJanPratinidhi(
            @Path(value = "slug", encoded = true) String slug
    );

    @GET(AppText.WODAKARYALAYA_URL)
    Call<WodakaryalaResponse> getWodaKaryalaya(
    );

    @GET(AppText.WODAKARYALAYA_EMPLOYEE_URL + "{id}")
    Call<WodaEmpResponse> getWodaKaryalayaEmployee(
            @Path(value = "id", encoded = true) String id
    );

    @GET(AppText.MAHATOPURNA_NUMBER_URL)
    Call<ImportantNumberResponse> getImportantNumbers(
    );



    @POST(AppText.SUJHAP_URL)
    //@Headers({ "Content-Type: application/json;charset=UTF-8"})
    @FormUrlEncoded
    Call<SujhapDTO> getSujhap(
            @Header("Authorization") String authorization,
            //@QueryMap Map<String, String> feedDetail
            /*@Field("name") String name,
            @Field("email") String email,
            @Field("mobile_no") String mobile_no,
            @Field("subject") String subject,
            @Field("description") String description*/

                @Body FeedbackDTO feedbackDTO
            );



    /*@Headers({
            "Content-Type: application/json"
    })

    @POST(AppText.LOGIN_URL)
    Call<LoginResponse> LoginRequest(
            @Body LoginRequest request
    );*/


   /* @POST(AppText.LOGIN_URL)
    Call<LoginResponse> LoginRequestWithDeviceId(
            @Header("deviceKey") String deviceKey,
            @Body LoginRequest request
    );*/




   /* @GET(AppText.FEED_URL)
    Call<FeedResponse> getfeedRequest(
            @Header("Authorization") String authorization,
            @QueryMap Map<String, String> page
    );*/


   /* @Multipart
    @POST(AppText.QUICKPOST_URL)
    Call<QuickPostImageResponse> getDocumentPostRequest(
            @Header("Authorization") String authorization,
            @Body DocumentPost request
    );*/




}
