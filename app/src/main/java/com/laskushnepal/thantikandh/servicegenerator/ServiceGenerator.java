package com.laskushnepal.thantikandh.servicegenerator;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.laskushnepal.thantikandh.BuildConfig;
import com.laskushnepal.thantikandh.utils.AppText;
import com.laskushnepal.thantikandh.utils.NetworkUtil;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

import static okhttp3.logging.HttpLoggingInterceptor.Level.HEADERS;
import static okhttp3.logging.HttpLoggingInterceptor.Level.NONE;

public class ServiceGenerator {
    private static final String CACHE_CONTROL = "Cache-Control";

    private static Context context;

    public ServiceGenerator(Context context){
        this.context = context;
    }

    static Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    public static Retrofit provideRetrofit(){
        return new Retrofit.Builder()
                .baseUrl(BASE_URl)
                .client(provideOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    private static OkHttpClient provideOkHttpClient(){
        return new OkHttpClient.Builder()
                .addInterceptor( provideHttpLoggingInterceptor() )
                .addInterceptor( provideOfflineCacheInterceptor() )
                .addNetworkInterceptor( provideCacheInterceptor() )
                .cache(provideCache())
                .build();
    }

    private static Cache provideCache ()
    {
        Cache cache = null;
        try
        {
            cache = new Cache( new File( context.getCacheDir(), "http-cache" ),
                    10 * 1024 * 1024 ); // 10 MB
            Timber.d(cache.toString());
            Timber.d(cache.directory().getAbsolutePath());
            Timber.d(cache.directory().getPath());
            Timber.d(cache.directory().getCanonicalPath());
            Timber.d(String.valueOf(cache.maxSize()));
        }
        catch (Exception e)
        {
            Timber.d("Could not create cache");
        }
        return cache;
    }

    private static HttpLoggingInterceptor provideHttpLoggingInterceptor ()
    {
        HttpLoggingInterceptor httpLoggingInterceptor =
                new HttpLoggingInterceptor( new HttpLoggingInterceptor.Logger()
                {
                    @Override
                    public void log (String message)
                    {
                        Timber.d( message );
                    }
                } );
        httpLoggingInterceptor.setLevel( BuildConfig.DEBUG ? HEADERS : NONE );
        return httpLoggingInterceptor;
    }


    public static Interceptor provideCacheInterceptor()
    {
        return new Interceptor()
        {
            @Override
            public Response intercept (Chain chain) throws IOException
            {
                Response response = chain.proceed( chain.request() );

                // re-write response header to force use of cache
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxAge(1, TimeUnit.MINUTES)
                        .build();

                return response.newBuilder()
                        .header( CACHE_CONTROL, cacheControl.toString() )
                        .build();
            }
        };
    }


    public static Interceptor provideOfflineCacheInterceptor()
    {
        return new Interceptor()
        {
            @Override
            public Response intercept (Chain chain) throws IOException
            {
                Request request = chain.request();

                if (!NetworkUtil.isInternetConnectionAvailable(context))
                {
                    CacheControl cacheControl = new CacheControl.Builder()
                            .maxStale( 7, TimeUnit.DAYS)
                            .build();

                    request = request.newBuilder()
                            .header("Content-Encoding","gzip")
                            .cacheControl( cacheControl )
                            .build();
                }

                return chain.proceed( request );
            }
        };
    }


    /*private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .addInterceptor( provideHttpLoggingInterceptor() )
            .addInterceptor( provideOfflineCacheInterceptor() )
            .addNetworkInterceptor( provideCacheInterceptor() )
            .cache(provideCache()).build();*/


    //Working code below without cache
    private static final String BASE_URl = AppText.BASE_URL;

   /* private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();*/

    /*private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URl)
                    .client(provideOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create());*/

    public  <S> S createService(Class<S> serviceClass) {
        return provideRetrofit().create(serviceClass);
    }

}
