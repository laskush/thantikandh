package com.laskushnepal.thantikandh.fragment.Karmachari;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.adapter.EmployeeListAdapter;
import com.laskushnepal.thantikandh.api.APIServices;
import com.laskushnepal.thantikandh.model.Karmachari.EmployeeDTO;
import com.laskushnepal.thantikandh.model.Karmachari.EmployeeResponse;
import com.laskushnepal.thantikandh.servicegenerator.RetrofitApiClient;
import com.laskushnepal.thantikandh.utils.NetworkUtil;
import com.laskushnepal.thantikandh.utils.PrefUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NewEmployeeFragment extends Fragment {

    //constant tags
    private static String TAG = NewEmployeeFragment.class.getSimpleName();

    //view
    ProgressBar news_progressBar;
    RecyclerView news_recyclerView;

    //instances
    private EmployeeListAdapter employeeListAdapter;
    LinearLayoutManager mManager;

    //variable
    private ArrayList<EmployeeDTO> employeeList= new ArrayList<>();
    int position;
    String categorySlug;


    public NewEmployeeFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public NewEmployeeFragment(int position,String slug){
        this.position = position;
        this.categorySlug=slug;

    }

    public static NewEmployeeFragment newInstance(String slug) {
        NewEmployeeFragment fragment = new NewEmployeeFragment();

        Bundle data = new Bundle();//create bundle instance
        data.putString("slug", slug);
        fragment.setArguments(data);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_suchana_jankari, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        categorySlug = getArguments().getString("slug");

        news_recyclerView = (RecyclerView) view.findViewById(R.id.news_recyclerView);
        news_progressBar = (ProgressBar) view.findViewById(R.id.news_progressBar);

        mManager = new LinearLayoutManager(getActivity());
        news_recyclerView.setLayoutManager(mManager);
        employeeListAdapter = new EmployeeListAdapter(getActivity().getLayoutInflater(),getContext());


        if(NetworkUtil.isInternetConnectionAvailable(getContext())){
            news_progressBar.setVisibility(View.VISIBLE);
            getEmployeeList();
        }else{
            showSnackbar(getString(R.string.no_internet),false);
            employeeList = PrefUtils.getKarmachariList(getContext(),PrefUtils.KARMACHARI_RESPONSE+categorySlug);
            if(employeeList != null){
                setEmployeeList(employeeList);
            }
        }


    }

    private void getEmployeeList() {

       /* showProgressDialog(false);

        employeeList.add(new EmployeeDTO("",getContext().getResources().getString(R.string.emp_name1),getContext().getResources().getString(R.string.emp_des1)
                ,getContext().getResources().getString(R.string.emp_section1),getContext().getResources().getString(R.string.emp_email1),
                getContext().getResources().getString(R.string.emp_office_no1),getContext().getResources().getString(R.string.emp_mobile_no1)));

        employeeList.add(new EmployeeDTO("",getContext().getResources().getString(R.string.emp_name2),getContext().getResources().getString(R.string.emp_des2)
                ,"",getContext().getResources().getString(R.string.emp_email2),
                getContext().getResources().getString(R.string.emp_office_no2),getContext().getResources().getString(R.string.emp_mobile_no2)));

        employeeList.add(new EmployeeDTO("",getContext().getResources().getString(R.string.emp_name3),getContext().getResources().getString(R.string.emp_des3)
                ,getContext().getResources().getString(R.string.emp_section3),getContext().getResources().getString(R.string.emp_email3),
                getContext().getResources().getString(R.string.emp_office_no3),getContext().getResources().getString(R.string.emp_mobile_no3)));*/


        final Retrofit retrofit = new RetrofitApiClient(getContext()).getAdapter();
        final APIServices apiServices = retrofit.create(APIServices.class);

        apiServices.getKarmachari(categorySlug).enqueue(new Callback<EmployeeResponse>() {

            @Override
            public void onResponse(Call<EmployeeResponse> call, Response<EmployeeResponse> response) {
                try {

                    if (response.body() != null) {
                        EmployeeResponse employeeResponse = response.body();
                        Log.d("Response : ", String.valueOf(response.body()));
                        employeeList = employeeResponse.getEmployeeList();
                        PrefUtils.saveKarmachariList(getContext(),employeeList,PrefUtils.KARMACHARI_RESPONSE+categorySlug);
                        news_progressBar.setVisibility(View.GONE);
                        if (employeeList != null) {
                            setEmployeeList(employeeList);
                        }

                    } else {
                        news_progressBar.setVisibility(View.GONE);
                        showSnackbar(getResources().getString(R.string.no_data),false);
                        //Toast.makeText(getContext(), getContext().getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    }


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<EmployeeResponse> call, Throwable t) {
                Log.d("Failure : ", t.getMessage());
                news_progressBar.setVisibility(View.GONE);
                //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void setEmployeeList(List<EmployeeDTO> employeeList) {

        employeeListAdapter.addEmployeeList(employeeList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());

        news_recyclerView.setLayoutManager(layoutManager);
        news_recyclerView.setItemAnimator(new DefaultItemAnimator());
        //important_number_recyclerView.addItemDecoration(new ItemOffsetDecoration(PostActivity.this, R.dimen.item_offset));
        news_recyclerView.setAdapter(employeeListAdapter);
    }


    void showSnackbar(String message,boolean action){

        if(action == true){

            Snackbar.make(news_recyclerView, message,
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.try_again), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Request the permission
                    //Toast.makeText(getContext(), "Action Clicked", Toast.LENGTH_SHORT).show();
                    getEmployeeList();
                }
            }).show();
        }else{

            Snackbar.make(news_recyclerView, message, Snackbar.LENGTH_LONG).show();
        }

    }
}
