package com.laskushnepal.thantikandh.fragment.KarekaramPariyojana;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.adapter.SuchanaJankariListAdapter;
import com.laskushnepal.thantikandh.api.APIServices;
import com.laskushnepal.thantikandh.model.News.NewsDTO;
import com.laskushnepal.thantikandh.model.News.NewsResponse;
import com.laskushnepal.thantikandh.servicegenerator.RetrofitApiClient;
import com.laskushnepal.thantikandh.utils.NetworkUtil;
import com.laskushnepal.thantikandh.utils.PrefUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class KaryakaramFragment extends Fragment {

    //views
    ProgressBar news_progressBar;
    RecyclerView news_recyclerView;

    //instances
    SuchanaJankariListAdapter suchanaJankariListAdapter;

    //variable
    int position;
    String categorySlug,categoryTitle;
    ArrayList<NewsDTO> newsList = new ArrayList<>();
    Bundle bundle;

    public KaryakaramFragment(){

    }

    @SuppressLint("ValidFragment")
    public KaryakaramFragment(int position,String slug){
        this.position = position;
        this.categorySlug=slug;

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_suchana_jankari,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        bundle = savedInstanceState;
        categorySlug = getArguments().getString("slug");
        categoryTitle = getArguments().getString("category");

        news_recyclerView = (RecyclerView) view.findViewById(R.id.news_recyclerView);
        news_progressBar = (ProgressBar) view.findViewById(R.id.news_progressBar);

        if(NetworkUtil.isInternetConnectionAvailable(getContext())){
            news_progressBar.setVisibility(View.VISIBLE);
            getNewsList();
        }else{
            showSnackbar(getContext().getResources().getString(R.string.no_internet));
            newsList = PrefUtils.getKarekaramList(getContext(),PrefUtils.KAREKARAM_RESPONSE+categorySlug);
            if(newsList != null){
                setNewsList(newsList);
            }
        }

    }

    private void getNewsList() {

        final Retrofit retrofit = new RetrofitApiClient(getContext()).getAdapter();
        final APIServices apiServices = retrofit.create(APIServices.class);

        apiServices.getKarekaram(categorySlug).enqueue(new Callback<NewsResponse>() {

            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                try {

                    if (response.body() != null) {
                        NewsResponse newsResponse = response.body();
                        Log.d("News Response : ", String.valueOf(response.body()));
                        news_progressBar.setVisibility(View.GONE);
                        newsList = newsResponse.getNewsList();
                        PrefUtils.saveKarekaramList(getContext(),newsList,PrefUtils.KAREKARAM_RESPONSE+categorySlug);
                        if(newsList != null){
                            setNewsList(newsList);
                        }else{
                            showSnackbar(getResources().getString(R.string.no_data));
                            //Toast.makeText(getContext(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        news_progressBar.setVisibility(View.GONE);
                        showSnackbar(getResources().getString(R.string.no_data));
                        //Toast.makeText(getContext(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    }


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                news_progressBar.setVisibility(View.GONE);
                Log.d("Failure : ", t.getMessage());
                //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setNewsList(ArrayList<NewsDTO> newsList) {

        news_recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        news_recyclerView.setLayoutManager(layoutManager);
        suchanaJankariListAdapter = new SuchanaJankariListAdapter(getLayoutInflater(bundle), getContext());
        suchanaJankariListAdapter.addNewsList(newsList,categoryTitle);
        news_recyclerView.setAdapter(suchanaJankariListAdapter);
    }

    void showSnackbar(String message){
        Snackbar.make(news_recyclerView, message, Snackbar.LENGTH_LONG).show();
    }
}
