package com.laskushnepal.thantikandh.utils;


import android.util.Log;

import com.laskushnepal.thantikandh.BuildConfig;

public class AppLog {

    public static void showLog(String tag, String message){
        if(BuildConfig.DEBUG) Log.d(tag, message);
    }
}
