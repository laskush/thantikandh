package com.laskushnepal.thantikandh.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class ParseUtil {

    public static String parseDateString(String sourceDate) {
        String result = "";

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = dateFormat.parse(sourceDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);

            switch (month) {
                case 0:
                    // result = "January "+ day + ", " + year ;
                    result = day + " Jan" + ", " + year;
                    break;

                case 1:
                    //result = "February "+ day + ", " + year ;
                    result = day + " Feb" + ", " + year;
                    break;

                case 2:
                    //result = "March "+ day + ", " + year ;
                    result = day + " Mar" + ", " + year;
                    break;

                case 3:
                    //  result = "April "+ day + ", " + year ;
                    result = day + " Apr" + ", " + year;
                    break;
                case 4:
                    //  result = "May "+ day + ", " + year ;
                    result = day + " May" + ", " + year;
                    break;
                case 5:
                    // result = "June "+ day + ", " + year ;
                    result = day + " Jun" + ", " + year;
                    break;
                case 6:
                    //result = "July "+ day + ", " + year ;
                    result = day + " Jul" + ", " + year;
                    break;
                case 7:
                    // result = "August "+ day + ", " + year ;
                    result = day + " Aug" + ", " + year;
                    break;
                case 8:
                    //result = "September "+ day + ", " + year ;
                    result = day + "Sep" + ", " + year;
                    break;
                case 9:
                    // result = "October "+ day + ", " + year ;
                    result = day + " Oct" + ", " + year;
                    break;
                case 10:
                    //result = "November "+ day + ", " + year ;
                    result = day + " Nov" + ", " + year;
                    break;
                case 11:
                    //result = "December "+ day + ", " + year ;
                    result = day + " Dec" + ", " + year;
                    break;

                default:
                    break;
            }


        } catch (ParseException e) {
            e.printStackTrace();
            result = "N/A";
        }

        return result;

    }


}
