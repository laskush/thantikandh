package com.laskushnepal.thantikandh.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {


    /**find if there is a working internet connection available in device*/
    public static boolean isInternetConnectionAvailable(Context context) {

        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI")) {
                    if (ni.isConnected()) haveConnectedWifi = true;
                }
                if (ni.getTypeName().equalsIgnoreCase("MOBILE")) {
                    if (ni.isConnected()) haveConnectedMobile = true;
                }

            }
        } catch (Exception e) {
            e.getStackTrace();
        }

        return haveConnectedWifi || haveConnectedMobile;

    }


    public static boolean isNetAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public static String getFontStyle(String htmlBodyContents){
        return "<html>" +
                "<head>" +
                "<style type=\"text/css\">" +
                "@font-face{ " +
                "font-family: \"Roboto\";" +
                "src: url('file:///android_asset/fonts/Roboto-Light.ttf');}" +
                "body {" +
                "font-family: \"Roboto\";" +
                "line-height: \"normal\";}" +
                "</style>" +
                "</head>" +
                "<body>" +
                htmlBodyContents +
                "</body>" +
                "</html>";
    }


}
