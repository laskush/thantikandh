package com.laskushnepal.thantikandh.utils;


public class AppText {

    //public static final String BASE_URL = "http://jes.com.np/thantikandhmun/";
    public static final String BASE_URL = "http://laskush.com/thantikandh/";



    public static final String SLIDER_URL = "api/fetchSlideshow";
    public static final String GALLERY_CATEGORY_URL = "api/fetchGalleryCategory";
    public static final String IMAGE_GALLERY_URL = "api/fetchGalleryImage/";
    public static final String NEWS_CATEGORY_URL = "api/fetchNewsCategory";
    public static final String NEWS_URL = "api/fetchNews/";

    public static final String REPORT_CATEGORY_URL = "api/fetchReportCategory";
    public static final String REPORT_URL = "api/fetchReport/";

    public static final String BIDHUTIYA_CATEGORY_URL = "api/fetchBidutiyaCategory";
    public static final String BIDHUTIYA_URL = "api/fetchBidutiya/";

    public static final String KAREKARAM_CATEGORY_URL = "api/fetchProgramCategory";
    public static final String KAREKARAM_URL = "api/fetchProgram/";

    public static final String KARMACHARI_CATEGORY_URL = "api/fetchMembersCategory";
    public static final String KARMACHARI_URL = "api/fetchMember/";

    public static final String JANPRATINIDHI_CATEGORY_URL = "api/fetchOtherMembers";
    public static final String JANPRATINIDHI_URL = "api/fetchMember/";

    public static final String WODAKARYALAYA_URL = "api/fetchorganization/";
    public static final String WODAKARYALAYA_EMPLOYEE_URL = "api/fetchOrganizationMember/";
    public static final String MAHATOPURNA_NUMBER_URL = "api/fetchNumbers/";

    public static final String SUJHAP_URL = "api/sendMail/";

    public static String URL_INSURANCE_PLANS=BASE_URL+"get_plans";
    public static String URL_NEWS=BASE_URL+"api/fetchNews";
    public static String URL_SERVICES=BASE_URL+"api/fetchService";
    public static String URL_ORGANIZATION=BASE_URL+"api/fetchOrganization";
    public static String URL_PLAN=BASE_URL+"api/fetchPlan";
    public static String URL_ABOUT=BASE_URL+"api/fetchAbout";
    public static String URL_GALLERY=BASE_URL+"api/fetchGalleryCategory";
    public static String URL_MEMBER=BASE_URL+"api/fetchmember";
    public static String URL_FINANCE=BASE_URL+"get_statement";
    public static String URL_BRANCH=BASE_URL+"get_branches";
    public static String SLIDER_IMAGE_PATH=BASE_URL+"+uploads/images/slideshow/";
    public static String URL_BUDGET=BASE_URL+"api/fetchBudget";

    public static final String BOD_CATEGORY_ID="1";
    public static final String EMPLOYEE_CATEGORY_ID="2";





}
