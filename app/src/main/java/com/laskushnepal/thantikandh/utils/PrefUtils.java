package com.laskushnepal.thantikandh.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.laskushnepal.thantikandh.model.Gallery.ImageGalleryDTO;
import com.laskushnepal.thantikandh.model.Gallery.SingleGalleryDTO;
import com.laskushnepal.thantikandh.model.Karmachari.EmployeeDTO;
import com.laskushnepal.thantikandh.model.Karmachari.KarmachariDTO;
import com.laskushnepal.thantikandh.model.MahatoPurnaNumber.ImportantNumberDTO;
import com.laskushnepal.thantikandh.model.News.NewsCategoryDTO;
import com.laskushnepal.thantikandh.model.News.NewsDTO;
import com.laskushnepal.thantikandh.model.Slider.SliderDTO;
import com.laskushnepal.thantikandh.model.wodakaryala.WodaDTO;
import com.laskushnepal.thantikandh.model.wodakaryala.WodaEmployeeDTO;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Rajan Shrestha on 10/27/2017.
 */

public class PrefUtils {
    public static final String SHARED_PREF_LIVELY = "lively";

    public static final String SLIDER_RESPONSE = "slider_response";

    public static final String GALLERY_CATEGORY_RESPONSE = "gallery_category_response";
    public static final String GALLERY__RESPONSE = "gallery_response";

    public static final String NEWS_CATEGORY_RESPONSE = "news_category_response";
    public static final String NEWS_RESPONSE = "news_response";

    public static final String PRATIBHEDAN_CATEGORY_RESPONSE = "pratibhedan_category_response";
    public static final String PRATIBHEDAN_RESPONSE = "pratibhedan_response";

    public static final String KARMACHARI_CATEGORY_RESPONSE = "karmachari_category_response";
    public static final String KARMACHARI_RESPONSE = "karmachari_response";

    public static final String JANPRATINIDHI_CATEGORY_RESPONSE = "janpratinidhi_category_response";
    public static final String JANPRATINIDHI_RESPONSE = "janpratinidhi_response";

    public static final String BIDHUTIYA_CATEGORY_RESPONSE = "bidhutiya_category_response";
    public static final String BIDHUTIYA_RESPONSE = "bidhutiya_response";

    public static final String KAREKARAM_CATEGORY_RESPONSE = "karekaram_category_response";
    public static final String KAREKARAM_RESPONSE = "karekaram_response";

    public static final String MAHATOPURNA_RESPONSE = "mahatopurna_response";

    public static final String WODA_RESPONSE = "woda_response";
    public static final String WODA_EMPLOYESS_RESPONSE = "woda_employee_response";


    public static void saveSliderList(Context context, ArrayList<SliderDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<SliderDTO> getSliderList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<SliderDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public static void saveGalleryCategoryList(Context context, ArrayList<ImageGalleryDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<ImageGalleryDTO> getGalleryCategoryList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<ImageGalleryDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public static void saveGalleryList(Context context, ArrayList<SingleGalleryDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<SingleGalleryDTO> getGalleryList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<SingleGalleryDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void saveNewsCategoryList(Context context, ArrayList<NewsCategoryDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<NewsCategoryDTO> getNewsCategoryList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<NewsCategoryDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public static void saveNewsList(Context context, ArrayList<NewsDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<NewsDTO> getNewsList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<NewsDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void savePradibhedanCategoryList(Context context, ArrayList<NewsCategoryDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<NewsCategoryDTO> getPradibhedanCategoryList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<NewsCategoryDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void savePradibhedanList(Context context, ArrayList<NewsDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<NewsDTO> getPradibhedanList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<NewsDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public static void saveKarmachariCategoryList(Context context, ArrayList<KarmachariDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<KarmachariDTO> getKarmachariCategoryList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<KarmachariDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public static void saveKarmachariList(Context context, ArrayList<EmployeeDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<EmployeeDTO> getKarmachariList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<EmployeeDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public static void saveWodaKarmachariList(Context context, ArrayList<WodaEmployeeDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<WodaEmployeeDTO> getWodaKarmachariList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<WodaEmployeeDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void saveJanPratinidhiCategoryList(Context context, ArrayList<KarmachariDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<KarmachariDTO> getJanPratinidhiCategoryList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<KarmachariDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public static void saveJanPratinidhiList(Context context, ArrayList<EmployeeDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<EmployeeDTO> getJanPratinidhiList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<EmployeeDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public static void saveBidhutiyaCategoryList(Context context, ArrayList<NewsCategoryDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<NewsCategoryDTO> getBidhutiyaCategoryList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<NewsCategoryDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void saveBidhutiyaList(Context context, ArrayList<NewsDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<NewsDTO> getBidhutiyaList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<NewsDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public static void saveKarekaramCategoryList(Context context, ArrayList<NewsCategoryDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<NewsCategoryDTO> getKarekaramCategoryList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<NewsCategoryDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void saveKarekaramList(Context context, ArrayList<NewsDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<NewsDTO> getKarekaramList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<NewsDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void saveImportantNumberList(Context context, ArrayList<ImportantNumberDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public static ArrayList<ImportantNumberDTO> getImportantNumberList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<ImportantNumberDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void saveWodaList(Context context, ArrayList<WodaDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public static ArrayList<WodaDTO> getWodaList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<WodaDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

}
