package com.laskushnepal.thantikandh.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.testapp.utils.photoview.PhotoView;

public class TouchImageDetailActivity extends AppCompatActivity {

    PhotoView touch_imageView;
    Intent i;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touch_image_detail);

        i = getIntent();
        String image = i.getStringExtra("image");
        touch_imageView = (PhotoView) findViewById(R.id.touch_imageView);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if(image.equalsIgnoreCase("1")){
            touch_imageView.setImageDrawable(getResources().getDrawable(R.drawable.thantikadh_maps));
        }else if(image.equalsIgnoreCase("2")){
            touch_imageView.setImageDrawable(getResources().getDrawable(R.drawable.ward1));
        }else if(image.equalsIgnoreCase("3")){
            touch_imageView.setImageDrawable(getResources().getDrawable(R.drawable.ward2));
        }else if(image.equalsIgnoreCase("4")){
            touch_imageView.setImageDrawable(getResources().getDrawable(R.drawable.ward3));
        }else if(image.equalsIgnoreCase("5")){
            touch_imageView.setImageDrawable(getResources().getDrawable(R.drawable.ward4));
        }else if(image.equalsIgnoreCase("6")){
            touch_imageView.setImageDrawable(getResources().getDrawable(R.drawable.ward5));
        }else if(image.equalsIgnoreCase("7")){
            touch_imageView.setImageDrawable(getResources().getDrawable(R.drawable.ward6));
        }

        //Glide.with(TouchImageDetailActivity.this).load(getResources().getDrawable(R.drawable.map1)).into(touch_imageView);
    }
}
