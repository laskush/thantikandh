package com.laskushnepal.thantikandh.activity.Sujhap;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.api.APIServices;
import com.laskushnepal.thantikandh.model.FeedbackDTO;
import com.laskushnepal.thantikandh.model.SujhapDTO;
import com.laskushnepal.thantikandh.servicegenerator.RetrofitApiClient;
import com.laskushnepal.thantikandh.utils.AppText;
import com.laskushnepal.thantikandh.utils.NetworkUtil;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SujhapActivity extends AppCompatActivity {

    //view
    Toolbar toolbar;
    TextView toolbar_tv;
    EditText feedback_name,feedback_email,feedback_number,feedback_subject,feedback_description;
    TextView feedback_send;

    //variables
    String name,email,number,subject,description;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sujhap_pratikriya);

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getResources().getString(R.string.sujhap));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        feedback_name = (EditText) findViewById(R.id.feedback_name);
        feedback_email = (EditText) findViewById(R.id.feedback_email);
        feedback_number = (EditText) findViewById(R.id.feedback_number);
        feedback_subject = (EditText) findViewById(R.id.feedback_subject);
        feedback_description = (EditText) findViewById(R.id.feedback_description);
        feedback_send = (TextView) findViewById(R.id.feedback_send);

        feedback_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = feedback_name.getText().toString();
                email = feedback_email.getText().toString();
                number = feedback_number.getText().toString();
                subject = feedback_subject.getText().toString();
                description = feedback_description.getText().toString();

                if(name.isEmpty() || email.isEmpty() || number.isEmpty() || subject.isEmpty() || description.isEmpty()){

                    if(name.isEmpty()){
                        feedback_name.setError(getString(R.string.required));
                    }else if (email.isEmpty()){
                        feedback_email.setError(getString(R.string.required));
                    }else if (number.isEmpty()){
                        feedback_number.setError(getString(R.string.required));
                    }else if (subject.isEmpty()){
                        feedback_subject.setError(getString(R.string.required));
                    }else if (description.isEmpty()){
                        feedback_description.setError(getString(R.string.required));
                    }
                }else{
                    if(!isValidEmail(email)){
                        feedback_email.setError(getString(R.string.valid_mail));
                    }else{

                        if(NetworkUtil.isInternetConnectionAvailable(SujhapActivity.this)){
                           new HttpAsyncTask().execute(AppText.BASE_URL + AppText.SUJHAP_URL);

                        }else{
                            showSnackbar(getString(R.string.no_internet));
                        }


                    }
                }

            }
        });

    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating number
    private boolean isValidNumber(String pass) {
        if (pass != null && pass.length() >= 4) {
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(true);
        }

        @Override
        protected String doInBackground(String... urls) {

            return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {


            if (result.equalsIgnoreCase("")) {
                showProgressDialog(false);
            } else if (result != null) {
                showProgressDialog(false);
                Log.d("PostActivity  Class", "Result:::" + result);
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("status")) {
                        String status = jsonObject.getString("status");
                    }

                    if (jsonObject.has("message")) {

                        if(jsonObject.getString("message").equalsIgnoreCase("Feedback Sent Successfully")){
                            String message = jsonObject.getString("message");
                            Toast.makeText(SujhapActivity.this, message, Toast.LENGTH_SHORT).show();
                            feedback_name.setText("");
                            feedback_email.setText("");
                            feedback_number.setText("");
                            feedback_subject.setText("");
                            feedback_description.setText("");
                        }


                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }


        private void showProgressDialog(boolean shouldShow) {
            if (shouldShow) {
                if (progressDialog == null) {
                    progressDialog = new ProgressDialog(SujhapActivity.this);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setCancelable(false);
                    progressDialog.setMessage(getString(R.string.sujhap_sending));
                    progressDialog.show();
                }
            } else {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
        }
    }

    public String POST(String url) {

        InputStream inputStream = null;
        String result = "";


        try {

            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(url);

            String json = "";

            // 3. build jsonObject
            JSONObject jsonObject = new JSONObject();

           /* jsonObject.put("subject", "Testig Feedback");
            jsonObject.put("name", "Rajan shrestha");
            jsonObject.put("email", "rajanshresthance@gmail.com");
            jsonObject.put("description", "Desc feedback");
            jsonObject.put("mobile_no", "9876543201");*/

            jsonObject.put("subject", subject);
            jsonObject.put("name", name);
            jsonObject.put("email", email);
            jsonObject.put("description", description);
            jsonObject.put("mobile_no", number);

            // 4. convert JSONObject to JSON to String
            json = jsonObject.toString();

            Log.d("SUJHAP ACTICITY", "SEND DATA :: " + json);

            // ** Alternative way to convert Person object to JSON string usin Jackson Lib
            // ObjectMapper mapper = new ObjectMapper();
            // json = mapper.writeValueAsString(person);

            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);

            // 6. set httpPost Entity
            httpPost.setEntity(se);

            // 7. Set some headers to inform server about the type of the content
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", "jayess_shakya");

            Log.d("HTTPPOST :::", String.valueOf(httpPost));

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // 10. convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
            e.printStackTrace();
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    void showSnackbar(String message){
        Snackbar.make(findViewById(R.id.toolbar_iv), message, Snackbar.LENGTH_LONG).show();
    }

}
