package com.laskushnepal.thantikandh.activity.Wada_karyalaya;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.adapter.WodakarlayaListAdapter;
import com.laskushnepal.thantikandh.api.APIServices;
import com.laskushnepal.thantikandh.model.wodakaryala.WodaDTO;
import com.laskushnepal.thantikandh.model.wodakaryala.WodakaryalaResponse;
import com.laskushnepal.thantikandh.servicegenerator.RetrofitApiClient;
import com.laskushnepal.thantikandh.utils.NetworkUtil;
import com.laskushnepal.thantikandh.utils.PrefUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class WodaKaryalayaActivity extends AppCompatActivity {

    //view
    TextView toolbar_tv;
    Toolbar toolbar;
    RecyclerView wodakaryalaya_recyclerView;

    //Variables
    ProgressDialog progressDialog;
    ArrayList<WodaDTO> wodaList = new ArrayList<>();
    LinearLayoutManager mManager;

    //instances
    WodakarlayaListAdapter wodakarlayaListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wodakaryalaya);

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getResources().getString(R.string.woda_karyalayaharu));
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        wodakaryalaya_recyclerView = (RecyclerView)  findViewById(R.id.wodakaryalaya_recyclerView);

        mManager = new LinearLayoutManager(WodaKaryalayaActivity.this);
        wodakaryalaya_recyclerView.setLayoutManager(mManager);
        wodakarlayaListAdapter = new WodakarlayaListAdapter(getLayoutInflater(),WodaKaryalayaActivity.this);

        if(NetworkUtil.isInternetConnectionAvailable(WodaKaryalayaActivity.this)){
            showProgressDialog(true);
            getWodaList();
        }else{
            showSnackbar(getString(R.string.no_internet),false);
            wodaList = PrefUtils.getWodaList(WodaKaryalayaActivity.this,PrefUtils.WODA_RESPONSE);
            if (wodaList != null) {
                setWodaList(wodaList);
            }

        }
    }


    private void getWodaList() {

        final Retrofit retrofit = new RetrofitApiClient(WodaKaryalayaActivity.this).getAdapter();
        final APIServices apiServices = retrofit.create(APIServices.class);

        apiServices.getWodaKaryalaya().enqueue(new Callback<WodakaryalaResponse>() {

            @Override
            public void onResponse(Call<WodakaryalaResponse> call, Response<WodakaryalaResponse> response) {
                try {

                    if (response.body() != null) {
                        WodakaryalaResponse wodakaryalaResponse = response.body();
                        Log.d("Response : ", String.valueOf(response.body()));
                        wodaList = wodakaryalaResponse.getWodaList();
                        PrefUtils.saveWodaList(WodaKaryalayaActivity.this,wodaList,PrefUtils.WODA_RESPONSE);
                        showProgressDialog(false);
                        if (wodaList != null) {
                            setWodaList(wodaList);
                        }

                    } else {
                        showSnackbar(getString(R.string.no_data),false);
                        showProgressDialog(false);
                        //Toast.makeText(WodaKaryalayaActivity.this, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    }


                } catch (NullPointerException e) {
                    //showProgressDialog(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WodakaryalaResponse> call, Throwable t) {
                Log.d("Failure : ", t.getMessage());
                showSnackbar(getString(R.string.load_failed),true);
                showProgressDialog(false);
                Toast.makeText(WodaKaryalayaActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void setWodaList(List<WodaDTO> wodaList) {

        wodakarlayaListAdapter.addWodaList(wodaList);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(WodaKaryalayaActivity.this,2);

        wodakaryalaya_recyclerView.setLayoutManager(layoutManager);
        wodakaryalaya_recyclerView.setItemAnimator(new DefaultItemAnimator());
        //important_number_recyclerView.addItemDecoration(new ItemOffsetDecoration(PostActivity.this, R.dimen.item_offset));
        wodakaryalaya_recyclerView.setAdapter(wodakarlayaListAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
            super.onBackPressed();
    }



    private void showProgressDialog(boolean shouldShow) {
        if(shouldShow){
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(WodaKaryalayaActivity.this);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(true);
                progressDialog.setMessage(getResources().getString(R.string.loading_data));
                progressDialog.show();
            }
        }else{
            if(progressDialog!=null){
                progressDialog.dismiss();
                progressDialog=null;
            }
        }

    }



    void showSnackbar(String message,boolean action){

        if(action == true){

            Snackbar.make(findViewById(R.id.toolbar_iv), message,
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.try_again), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Request the permission
                    getWodaList();
                    //Toast.makeText(WodaKaryalayaActivity.this, "Action Clicked", Toast.LENGTH_SHORT).show();
                }
            }).show();
        }else{

            Snackbar.make(findViewById(R.id.toolbar_iv), message,
                    Snackbar.LENGTH_LONG).show();
        }

    }
}
