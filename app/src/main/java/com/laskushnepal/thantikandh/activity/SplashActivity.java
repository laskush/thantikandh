package com.laskushnepal.thantikandh.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.api.APIServices;
import com.laskushnepal.thantikandh.model.Slider.SliderDTO;
import com.laskushnepal.thantikandh.model.Slider.SliderResponse;
import com.laskushnepal.thantikandh.servicegenerator.RetrofitApiClient;
import com.laskushnepal.thantikandh.utils.NetworkUtil;
import com.laskushnepal.thantikandh.utils.PrefUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Rajan Shrestha on 11/15/2017.
 */

public class SplashActivity extends AppCompatActivity {


    private static String TAG = SplashActivity.class.getSimpleName();
    private static int SPLASH_TIME = 2000;
    ArrayList<SliderDTO> sliderList = new ArrayList<>();


    // Animation
    Animation animBlink1, animBlink2;

    ImageView logo_iv;
    TextView gaupalika_tv1,gaupalika_tv2,gaupalika_tv3,gaupalika_tv4;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        logo_iv = (ImageView) findViewById(R.id.logo_iv);
        gaupalika_tv1 = (TextView) findViewById(R.id.gaupalika_tv1);
        gaupalika_tv2 = (TextView) findViewById(R.id.gaupalika_tv2);
        gaupalika_tv3 = (TextView) findViewById(R.id.gaupalika_tv3);
        gaupalika_tv4 = (TextView) findViewById(R.id.gaupalika_tv4);

        // load the animation
        animBlink2 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);

        gaupalika_tv1.startAnimation(animBlink2);
        gaupalika_tv2.startAnimation(animBlink2);
        gaupalika_tv3.startAnimation(animBlink2);
        gaupalika_tv4.startAnimation(animBlink2);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                /*Intent i =new Intent(SplashActivity.this,MainActivity.class);
                startActivity(i);
                finish();*/

                if(NetworkUtil.isInternetConnectionAvailable(SplashActivity.this)){
                    getSlider();
                }else{

                    showSnackbar(getString(R.string.no_internet));
                    sliderList = PrefUtils.getSliderList(SplashActivity.this,PrefUtils.SLIDER_RESPONSE);
                    if(sliderList!= null){
                        Intent i = new Intent(SplashActivity.this,MainActivity.class);
                        i.putParcelableArrayListExtra("sliderlist", (ArrayList<? extends Parcelable>)sliderList);
                        startActivity(i);
                        finish();
                    }
                }


                /*final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 5s = 5000ms

                        Intent i =new Intent(SplashActivity.this,MainActivity.class);
                        i.putParcelableArrayListExtra("sliderlist", (ArrayList<? extends Parcelable>)sliderList);
                        startActivity(i);

                    }
                }, 2000);*/


            }
        }, SPLASH_TIME);
    }


    private void getSlider() {

        final Retrofit retrofit = new RetrofitApiClient(SplashActivity.this).getAdapter();
        final APIServices apiServices = retrofit.create(APIServices.class);

        apiServices.getSlider().enqueue(new Callback<SliderResponse>() {

            @Override
            public void onResponse(Call<SliderResponse> call, Response<SliderResponse> response) {
                try {

                    if (response.body() != null) {
                        SliderResponse sliderResponse = response.body();
                        Log.d("Response : ", String.valueOf(response.body()));
                        //Toast.makeText(getContext(), "Response : "+ response.body(), Toast.LENGTH_SHORT).show();
                        sliderList = sliderResponse.getSliderList();
                        PrefUtils.saveSliderList(SplashActivity.this,sliderList,PrefUtils.SLIDER_RESPONSE);
                        if(sliderList!=null){

                            Intent i =new Intent(SplashActivity.this,MainActivity.class);
                            i.putParcelableArrayListExtra("sliderlist", (ArrayList<? extends Parcelable>)sliderList);
                            startActivity(i);
                            finish();
                        }

                    } else {
                        //Toast.makeText(MainActivity.this, "Response : null", Toast.LENGTH_SHORT).show();
                        showSnackbar(getString(R.string.no_data));
                    }


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SliderResponse> call, Throwable t) {
                Log.d("Failure : ", t.getMessage());
                Toast.makeText(SplashActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    void showSnackbar(String message){
        Snackbar.make(findViewById(R.id.toolbar_iv), message, Snackbar.LENGTH_LONG).show();
    }

}
