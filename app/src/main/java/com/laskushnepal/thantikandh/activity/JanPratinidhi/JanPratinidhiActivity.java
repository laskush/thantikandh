package com.laskushnepal.thantikandh.activity.JanPratinidhi;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.api.APIServices;
import com.laskushnepal.thantikandh.fragment.JanPratinidhi.JanPratinidhiTabFragment;
import com.laskushnepal.thantikandh.model.Karmachari.KarmachariCategoryResponse;
import com.laskushnepal.thantikandh.model.Karmachari.KarmachariDTO;
import com.laskushnepal.thantikandh.servicegenerator.RetrofitApiClient;
import com.laskushnepal.thantikandh.utils.NetworkUtil;
import com.laskushnepal.thantikandh.utils.PrefUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class JanPratinidhiActivity extends AppCompatActivity {

    //constant tags
    private static String TAG = JanPratinidhiActivity.class.getSimpleName();

    //view
    Toolbar toolbar;
    TextView toolbar_tv;
    TabLayout employee_TabLayout;
    ViewPager employee_ViewPager;

    //variables
    ArrayList<KarmachariDTO> karmachariList = new ArrayList<>();
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_karmachari);


        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getResources().getString(R.string.jan_pratinidhi));

        toolbar =  (Toolbar)  findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        employee_TabLayout = (TabLayout) findViewById(R.id.employee_TabLayout);
        employee_ViewPager = (ViewPager) findViewById(R.id.employee_ViewPager);

        if(NetworkUtil.isInternetConnectionAvailable(JanPratinidhiActivity.this)){
            showProgressDialog(true);
            getJanPratinidhiTabs();
        }else{
            showSnackbar(getString(R.string.no_internet),false);
            karmachariList = PrefUtils.getJanPratinidhiCategoryList(JanPratinidhiActivity.this,PrefUtils.JANPRATINIDHI_CATEGORY_RESPONSE);
            if (karmachariList != null) {

                employee_ViewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager(), karmachariList));
                employee_TabLayout.setupWithViewPager(employee_ViewPager);
            }
        }

    }

    private void getJanPratinidhiTabs() {

        final Retrofit retrofit = new RetrofitApiClient(JanPratinidhiActivity.this).getAdapter();
        final APIServices apiServices = retrofit.create(APIServices.class);

        apiServices.getJanPratinidhiCategory().enqueue(new Callback<KarmachariCategoryResponse>() {

            @Override
            public void onResponse(Call<KarmachariCategoryResponse> call, Response<KarmachariCategoryResponse> response) {
                try {

                    if (response.body() != null) {
                        KarmachariCategoryResponse karmachariCategoryResponse = response.body();
                        Log.d("Response : ", String.valueOf(response.body()));
                        karmachariList = karmachariCategoryResponse.getKarmachariList();
                        PrefUtils.saveJanPratinidhiCategoryList(JanPratinidhiActivity.this,karmachariList,PrefUtils.JANPRATINIDHI_CATEGORY_RESPONSE);
                        showProgressDialog(false);
                        if (karmachariList != null) {

                            employee_ViewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager(), karmachariList));
                            employee_TabLayout.setupWithViewPager(employee_ViewPager);
                        }

                    } else {
                        showSnackbar(getString(R.string.no_data),false);
                        showProgressDialog(false);
                        //Toast.makeText(JanPratinidhiActivity.this, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    }


                } catch (NullPointerException e) {
                    //showProgressDialog(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<KarmachariCategoryResponse> call, Throwable t) {
                Log.d("Failure : ", t.getMessage());
                showSnackbar(getString(R.string.load_failed),true);
                showProgressDialog(false);
                Toast.makeText(JanPratinidhiActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

   /* class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new JanPratinidhiTabFragment();
                case 1:
                    return new PadhadhikariTabFragment();
            }
            return null;

        }

        @Override
        public int getCount() {
            return 2;

        }

        @Override
        public CharSequence getPageTitle(int position) {
            // return tabList[position];
            switch (position){
                case 0:
                    return getResources().getString(R.string.jan_pratinidhi_title);
                case 1:
                    return getResources().getString(R.string.padhadikari_title);
            }
            return  null;
        }
    }*/


    //for dynamic tabs

    class MyPagerAdapter extends FragmentPagerAdapter {

        private ArrayList<KarmachariDTO> tabList;

        public MyPagerAdapter(FragmentManager fm,ArrayList<KarmachariDTO> tabList) {
            super(fm);
            this.tabList = tabList;
        }


        @Override
        public Fragment getItem(int position) {

            if(position <= tabList.size()){
                //position,tabList.get(position).getSlug()
                JanPratinidhiTabFragment janPratinidhiTabFragment = new JanPratinidhiTabFragment();
                Bundle data = new Bundle();//create bundle instance
                data.putString("slug", tabList.get(position).getSlug());
                janPratinidhiTabFragment.setArguments(data);
                janPratinidhiTabFragment.setRetainInstance(true);
                return janPratinidhiTabFragment;
            }

            // TODO Auto-generated method stub
            return null;

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabList.get(position).getTitle();

        }

        @Override
        public int getCount() {
            return tabList.size();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showProgressDialog(boolean shouldShow) {
        if(shouldShow){
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(JanPratinidhiActivity.this);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(true);
                progressDialog.setMessage(getResources().getString(R.string.loading_data));
                progressDialog.show();
            }
        }else{
            if(progressDialog!=null){
                progressDialog.dismiss();
                progressDialog=null;
            }
        }

    }

    void showSnackbar(String message,boolean action){

        if(action == true){

            Snackbar.make(findViewById(R.id.toolbar_iv), message,
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.try_again), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Request the permission
                    //Toast.makeText(JanPratinidhiActivity.this, "Action Clicked", Toast.LENGTH_SHORT).show();
                    getJanPratinidhiTabs();
                }
            }).show();
        }else{

            Snackbar.make(findViewById(R.id.toolbar_iv), message,
                    Snackbar.LENGTH_LONG).show();
        }

    }
}
