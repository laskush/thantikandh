package com.laskushnepal.thantikandh.activity.SuchanaJankari;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.laskushnepal.thantikandh.Interface.RecylerItemClickListner;
import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.activity.Gallery.GalleryDetailActivity;
import com.laskushnepal.thantikandh.adapter.SliderItemDetailAdapter;
import com.laskushnepal.thantikandh.adapter.SliderItemPageAdapter;
import com.laskushnepal.thantikandh.model.DocumentDTO;
import com.laskushnepal.thantikandh.model.ImageDTO;
import com.laskushnepal.thantikandh.utils.HackyViewPager;

public class SuchanaGalleryActivity extends AppCompatActivity implements RecylerItemClickListner {

    //Constant Tags
    public final String TAG = GalleryDetailActivity.class.getName();

    //Views
    private Toolbar toolbar;
    private HackyViewPager gallery_viewPager;
    private ImageView image_gallery_ImageView;
    private RecyclerView image_gallery_recyclerView;
    private TextView toolbar_tv,gallery_detail_tv;


    //Instances
    SliderItemPageAdapter sliderItemPageAdapter;
    SliderItemDetailAdapter sliderItemDetailAdapter;

    //variables
    Intent i;
    ArrayList<ImageDTO> imageList = new ArrayList<>();
    ArrayList<DocumentDTO> documentyList = new ArrayList<>();
    String galleryId;
    String galleryTitle;
    int position;
    String category;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_detail);

        i = getIntent();
        position = i.getIntExtra("position",0);
        category = i.getStringExtra("category");
        imageList  = i.getParcelableArrayListExtra("imageList");
        documentyList  = i.getParcelableArrayListExtra("documentList");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ActionBar actionBar = getSupportActionBar();
        setTitle("");

        actionBar.setDisplayHomeAsUpEnabled(true);
        //actionBar.setDefaultDisplayHomeAsUpEnabled(true);

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(category);

        gallery_viewPager = (HackyViewPager) findViewById(R.id.gallery_viewPager);
        image_gallery_ImageView = (ImageView) findViewById(R.id.image_gallery_ImageView);
        image_gallery_recyclerView = (RecyclerView) findViewById(R.id.image_detail_gallery_recyclerView);
        gallery_detail_tv = (TextView) findViewById(R.id.gallery_detail_tv);
        gallery_detail_tv.setVisibility(View.GONE);

        setSuchanaGalleyList(imageList,position);
        setSuchanaDocumentList(documentyList,position);

    }


    private void setSuchanaGalleyList(ArrayList<ImageDTO> imageDTOList,int position) {

        if (imageDTOList!= null) {

            gallery_viewPager.setAdapter(new SliderItemPageAdapter(SuchanaGalleryActivity.this, imageDTOList,"2"));
            gallery_viewPager.setCurrentItem(position);

            gallery_viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                        feeditemdetail_RecyclerView.scrollToPosition(position);
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });



            if (imageDTOList.size() == 1) {

                image_gallery_recyclerView.setVisibility(View.GONE);
            } else {
                try {

                    image_gallery_recyclerView.setHasFixedSize(true);
                    image_gallery_recyclerView.setLayoutManager(new LinearLayoutManager(SuchanaGalleryActivity.this, LinearLayoutManager.HORIZONTAL, false));
                    //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
                    sliderItemDetailAdapter = new SliderItemDetailAdapter(getLayoutInflater(), SuchanaGalleryActivity.this, "SuchanaImageGallery", position);
                    sliderItemDetailAdapter.addSuchanaImageGallery(imageDTOList);
                    image_gallery_recyclerView.setAdapter(sliderItemDetailAdapter);
                    sliderItemDetailAdapter.setClickListener(this);


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setSuchanaDocumentList(ArrayList<DocumentDTO> documentDTOList,int position) {

        if (documentDTOList!= null) {

            gallery_viewPager.setAdapter(new SliderItemPageAdapter(SuchanaGalleryActivity.this, documentDTOList,true));
            gallery_viewPager.setCurrentItem(position);

            gallery_viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                        feeditemdetail_RecyclerView.scrollToPosition(position);
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });


            if (documentyList.size() == 1) {

                image_gallery_recyclerView.setVisibility(View.GONE);
            } else {
                try {

                    image_gallery_recyclerView.setHasFixedSize(true);
                    image_gallery_recyclerView.setLayoutManager(new LinearLayoutManager(SuchanaGalleryActivity.this, LinearLayoutManager.HORIZONTAL, false));
                    //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
                    sliderItemDetailAdapter = new SliderItemDetailAdapter(getLayoutInflater(), SuchanaGalleryActivity.this, "SuchanaDocumentGallery", position);
                    sliderItemDetailAdapter.addSuchanaDocumentGallery(documentDTOList);
                    image_gallery_recyclerView.setAdapter(sliderItemDetailAdapter);
                    sliderItemDetailAdapter.setClickListener(this);


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onClick(View view, int position) {
        gallery_viewPager.setCurrentItem(position);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
