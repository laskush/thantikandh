package com.laskushnepal.thantikandh.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.activity.BidhutiyaSushasan.BidhutiyaSushasanActivity;
import com.laskushnepal.thantikandh.activity.Gallery.GalleryActivity;
import com.laskushnepal.thantikandh.activity.HamroBhare.HamroBhareActivity;
import com.laskushnepal.thantikandh.activity.JanPratinidhi.JanPratinidhiActivity;
import com.laskushnepal.thantikandh.activity.KarekaramPariyojana.KarekaramPariyojanaActivty;
import com.laskushnepal.thantikandh.activity.Karmachari.KarmachariActivity;
import com.laskushnepal.thantikandh.activity.MahatoPurnaNumber.MahatoPurnaNumberActivity;
import com.laskushnepal.thantikandh.activity.Parichya.ParichyaActivity;
import com.laskushnepal.thantikandh.activity.Pratibhedan.PratibhedanActivity;
import com.laskushnepal.thantikandh.activity.Slider.SliderDetailActivity;
import com.laskushnepal.thantikandh.activity.SuchanaJankari.SuchanaJankariActivity;
import com.laskushnepal.thantikandh.activity.Sujhap.SujhapActivity;
import com.laskushnepal.thantikandh.activity.Wada_karyalaya.WodaKaryalayaActivity;
import com.laskushnepal.thantikandh.api.APIServices;
import com.laskushnepal.thantikandh.model.Slider.SliderDTO;
import com.laskushnepal.thantikandh.model.Slider.SliderResponse;
import com.laskushnepal.thantikandh.servicegenerator.RetrofitApiClient;
import com.laskushnepal.thantikandh.utils.NetworkUtil;
import com.laskushnepal.thantikandh.utils.PrefUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener ,BaseSliderView.OnSliderClickListener,ViewPagerEx.OnPageChangeListener{


    //Constant Tags
    public final String TAG_FRAGMENT_DASHBOARD = MainActivity.class.getName();

    //Views
    private AppBarLayout appBarLayout;
    private ImageView toolbar_iv;
    private TextView toolbar_tv;
    private FrameLayout container;
    private Toolbar toolbar;
    CardView introduction_cardView,woda_karyalayaharu_cardView,jan_pratinidhi_cardView,Karmachari_cardView,
            karyakram_pariyojana_cardView, bidhutiya_sushasan_cardView,suchana_jhankari_cardView,
            pratibhedan_cardView,gallery_cardView,mahattopurna_number_cardView;

    private PagerIndicator custom_image_indicator;
    private SliderLayout view_image_slider;

    //Instances
    private FragmentManager fragmentManager;
    private Fragment lastUsedFragment = new Fragment();

    private ActionBarDrawerToggle toggle;

    //variables
    Intent i;
    ArrayList<SliderDTO> sliderList = new ArrayList<>();
    Bundle bundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bundle = savedInstanceState;
        i = getIntent();
        sliderList = i.getParcelableArrayListExtra("sliderlist");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        //toolbar.setNavigationIcon(R.drawable.menu);

        //to set the title in the toolbar
//        ActionBar actionBar = getSupportActionBar();
//        setTitle("");

        appBarLayout = (AppBarLayout) findViewById(R.id.appBar);
        appBarLayout.setVisibility(View.VISIBLE);

        toolbar_iv = (ImageView) findViewById(R.id.toolbar_iv);
        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getString(R.string.app_name));

        introduction_cardView = (CardView) findViewById(R.id.introduction_cardView);
        woda_karyalayaharu_cardView =(CardView) findViewById(R.id.woda_karyalayaharu_cardView);
        jan_pratinidhi_cardView = (CardView) findViewById(R.id.jan_pratinidhi_cardView);
        Karmachari_cardView = (CardView) findViewById(R.id.Karmachari_cardView);
        karyakram_pariyojana_cardView = (CardView) findViewById(R.id.karyakram_pariyojana_cardView);
        bidhutiya_sushasan_cardView = (CardView) findViewById(R.id.bidhutiya_sushasan_cardView);
        suchana_jhankari_cardView = (CardView) findViewById(R.id.suchana_jhankari_cardView);
        pratibhedan_cardView= (CardView) findViewById(R.id.pratibhedan_cardView);
        gallery_cardView= (CardView) findViewById(R.id.gallery_cardView);
        mahattopurna_number_cardView= (CardView) findViewById(R.id.mahattopurna_number_cardView);;

        DefineClick();
        toolbar_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        view_image_slider = (SliderLayout) findViewById(R.id.view_image_slider);
        custom_image_indicator = (PagerIndicator) findViewById(R.id.custom_image_indicator);


        if(NetworkUtil.isInternetConnectionAvailable(MainActivity.this)){
            //getSlider();
            createSlider(sliderList);
        }else{

            showSnackbar(getString(R.string.no_internet));
            //sliderList = PrefUtils.getSliderList(MainActivity.this,PrefUtils.SLIDER_RESPONSE);
            if(sliderList!= null){
                createSlider(sliderList);
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_drawer);
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void DefineClick() {

        introduction_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(MainActivity.this, ParichyaActivity.class);
                startActivity(i);
            }
        });

        woda_karyalayaharu_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(MainActivity.this, WodaKaryalayaActivity.class);
                startActivity(i);
            }
        });

        jan_pratinidhi_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, JanPratinidhiActivity.class);
                startActivity(i);
            }
        });

        Karmachari_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, KarmachariActivity.class);
                startActivity(i);
            }
        });

        karyakram_pariyojana_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, KarekaramPariyojanaActivty.class);
                startActivity(i);
            }
        });

        bidhutiya_sushasan_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, BidhutiyaSushasanActivity.class);
                startActivity(i);
            }
        });

        suchana_jhankari_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, SuchanaJankariActivity.class);
                startActivity(i);
            }
        });

        pratibhedan_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, PratibhedanActivity.class);
                startActivity(i);
            }
        });

        gallery_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, GalleryActivity.class);
                startActivity(i);
            }
        });

        mahattopurna_number_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, MahatoPurnaNumberActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.parichay) {

            Intent i = new Intent(MainActivity.this, ParichyaActivity.class);
            startActivity(i);

        } else if (id == R.id.wadaa_karyalay) {
            Intent i = new Intent(MainActivity.this, WodaKaryalayaActivity.class);
            startActivity(i);
        }else if(id == R.id.jan_pratinidhi){
            Intent i = new Intent(MainActivity.this, JanPratinidhiActivity.class);
            startActivity(i);

        }else if(id == R.id.karmachari){
            Intent i = new Intent(MainActivity.this, KarmachariActivity.class);
            startActivity(i);

        }else if(id == R.id.karyakram_tatha_pariyojana){
            Intent i = new Intent(MainActivity.this, KarekaramPariyojanaActivty.class);
            startActivity(i);

        }else if(id == R.id.bidutiya_sushasan_sewa){
            Intent i = new Intent(MainActivity.this, BidhutiyaSushasanActivity.class);
            startActivity(i);

        }else if(id == R.id.suchana_tatha_jankari){
            Intent i = new Intent(MainActivity.this, SuchanaJankariActivity.class);
            startActivity(i);

        /*  ArrayList<ImageDTO>   imageList = new ArrayList<>();
            imageList.add(new ImageDTO(getResources().getString(R.string.slider_title1),R.drawable.slider_image1));
            imageList.add(new ImageDTO(getResources().getString(R.string.slider_title2),R.drawable.slider_image2));
            imageList.add(new ImageDTO(getResources().getString(R.string.slider_title3),R.drawable.slider_image2));

            Intent i = new Intent(MainActivity.this, SuchanaGalleryActivity.class);
            i.putExtra("position",0);
            i.putExtra("category","news-one");
            i.putParcelableArrayListExtra("imageList",(ArrayList<? extends Parcelable>) imageList);
            startActivity(i);  */

        }else if(id == R.id.pratibedan){


        }else if(id == R.id.gallery){
            Intent i = new Intent(MainActivity.this, GalleryActivity.class);
            startActivity(i);

        }else if(id == R.id.mahatwa_purna_number_haru){
            Intent i = new Intent(MainActivity.this, MahatoPurnaNumberActivity.class);
            startActivity(i);

        }else if(id == R.id.sujhap_tatha_pratikriya){
            Intent i = new Intent(MainActivity.this, SujhapActivity.class);
            startActivity(i);

        }else if(id == R.id.hamro_bhare){
            Intent i = new Intent(MainActivity.this, HamroBhareActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void getSlider() {

       /* APIServices service = new ServiceGenerator(getContext()).createService(APIServices.class);
        Call<SliderResponse> sliderData = service.getSlider();
        sliderData.enqueue(new Callback<SliderResponse>() {
            @Override
            public void onResponse(Call<SliderResponse> call, Response<SliderResponse> response) {
                if(response.body()!=null) {

                    SliderResponse sliderResponse = response.body();
                    Log.d("Response : ", String.valueOf(response.body()));
                    Toast.makeText(getContext(), "Response : "+ response.body(), Toast.LENGTH_SHORT).show();
                    sliderList = sliderResponse.getSliderList();

                    *//*Log.d("List : ", String.valueOf(sliderResponse.getSliderList().size()));
         *//*

                    if(sliderList!= null){
                        createSlider(sliderList);
                    }
                }else{
                    Toast.makeText(getContext(), "Response : null", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<SliderResponse> call, Throwable t) {
                Log.d("Failure : ", t.getMessage());
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });*/


        final Retrofit retrofit = new RetrofitApiClient(MainActivity.this).getAdapter();
        final APIServices apiServices = retrofit.create(APIServices.class);

        apiServices.getSlider().enqueue(new Callback<SliderResponse>() {

            @Override
            public void onResponse(Call<SliderResponse> call, Response<SliderResponse> response) {
                try {

                    /*if(response.raw().cacheResponse() != null){
                        Toast.makeText(MainActivity.this, "Cache response ", Toast.LENGTH_SHORT).show();
                    }

                    if(response.raw().networkResponse() != null){
                        Toast.makeText(MainActivity.this, "Network response ", Toast.LENGTH_SHORT).show();

                    }*/
                    if (response.body() != null) {
                        SliderResponse sliderResponse = response.body();
                        Log.d("Response : ", String.valueOf(response.body()));
                        //Toast.makeText(getContext(), "Response : "+ response.body(), Toast.LENGTH_SHORT).show();
                        sliderList = sliderResponse.getSliderList();
                        PrefUtils.saveSliderList(MainActivity.this,sliderList,PrefUtils.SLIDER_RESPONSE);

                        createSlider(sliderList);
                       /* if(sliderList!= null){

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    // Do something after 5s = 5000ms
                                    createSlider(sliderList);
                                }
                            }, 1000);

                        }*/

                    } else {
                       //Toast.makeText(MainActivity.this, "Response : null", Toast.LENGTH_SHORT).show();
                        showSnackbar(getString(R.string.no_data));
                    }


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SliderResponse> call, Throwable t) {
                Log.d("Failure : ", t.getMessage());
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        /*sliderList.add(new SliderDTO(getContext().getResources().getString(R.string.slider_title4), R.drawable.slider_image4));
        sliderList.add(new SliderDTO(getContext().getResources().getString(R.string.slider_title1), R.drawable.slider_image1));
        sliderList.add(new SliderDTO(getContext().getResources().getString(R.string.slider_title2), R.drawable.slider_image2));
        sliderList.add(new SliderDTO(getContext().getResources().getString(R.string.slider_title3), R.drawable.slider_image3));*/

    }

    /* method to create the slider*/
    public void createSlider(List<SliderDTO> sliderList) {
        for(int i=0;i<sliderList.size();i++){
            TextSliderView textSliderView = new TextSliderView(MainActivity.this);

            textSliderView
                    .description(sliderList.get(i).getTitle())
                    .image(sliderList.get(i).getLocation()+sliderList.get(i).getImage())
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putInt("position",i);
            view_image_slider.addSlider(textSliderView);
        }

        view_image_slider.setPresetTransformer(SliderLayout.Transformer.DepthPage);
        //mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        view_image_slider.setDuration(5000);
        //view_image_slider.addOnPageChangeListener((ViewPagerEx.OnPageChangeListener) getContext());
        view_image_slider.setCustomIndicator(custom_image_indicator);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        int position = slider.getBundle().getInt("position");

        Intent i = new Intent(MainActivity.this, SliderDetailActivity.class);
        i.putExtra("position",position);
        i.putExtra("blockType","SliderGallery");
        i.putParcelableArrayListExtra("SliderGallery", (ArrayList<? extends Parcelable>) sliderList);
        startActivity(i);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    void showSnackbar(String message){
            Snackbar.make(findViewById(R.id.toolbar_iv), message, Snackbar.LENGTH_LONG).show();
    }
}
