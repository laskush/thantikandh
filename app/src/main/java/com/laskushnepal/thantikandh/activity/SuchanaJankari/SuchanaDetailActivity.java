package com.laskushnepal.thantikandh.activity.SuchanaJankari;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import com.laskushnepal.thantikandh.Interface.RecylerItemClickListner;
import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.adapter.DocumentAdapter;
import com.laskushnepal.thantikandh.adapter.ImageAdapter;
import com.laskushnepal.thantikandh.model.DocumentDTO;
import com.laskushnepal.thantikandh.model.ImageDTO;
import com.laskushnepal.thantikandh.model.News.NewsImage;
import com.laskushnepal.thantikandh.utils.ParseUtil;

public class SuchanaDetailActivity extends AppCompatActivity implements RecylerItemClickListner {


    //views
    private Toolbar toolbar;
    private TextView toolbar_tv;
    TextView  news_detail_title,news_detail_date_tv;
    RelativeLayout news_detail_image_gallery_rl,news_detail_document_gallery_rl;
    TextView news_detail_image_text,no_image_detail_text,news_detail_document_text,no_document_detail_text;
    RecyclerView news_detail_image_recyclerView,news_detail_document_recyclerView;

    //variables
    Intent i;
    String image1,image2,image3;
    String title,date,category;
    ArrayList<ImageDTO> imageList = new ArrayList<>();
    ArrayList<DocumentDTO> documentList = new ArrayList<>();

    //instances
    ImageAdapter imageAdapter;
    DocumentAdapter documentAdapter;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suchana_detail);

        i = getIntent();
        category = i.getStringExtra("category");
        title = i.getStringExtra("title");
        date = i.getStringExtra("date");
        image1 = i.getStringExtra("Image1");
        image2 = i.getStringExtra("Image2");
        image3 = i.getStringExtra("Image3");

        imageList = new ArrayList<>();
        documentList = new ArrayList<>();

        String link1 = image1;
        if(link1.length()>0){
            //Toast.makeText(SuchanaDetailActivity.this,"link1 : "+ link1,Toast.LENGTH_SHORT).show();
            String documentName1 = link1.substring(link1.lastIndexOf("/")+1);

            String extension1 = link1.substring(link1.lastIndexOf("."));
            if(extension1.equalsIgnoreCase(".pdf")){
                //  Toast.makeText(mContext,"Added to document",Toast.LENGTH_SHORT).show();
                documentList.add(new DocumentDTO(documentName1,link1));
            }else{
                //Toast.makeText(mContext,"Added to image",Toast.LENGTH_SHORT).show();
                imageList.add(new ImageDTO(documentName1,link1));
            }
        }



        String link2 = image2;
        if(link2.length()>0){
            //Toast.makeText(SuchanaDetailActivity.this,"link2 : "+ link2,Toast.LENGTH_SHORT).show();
            String documentName2 = link2.substring(link2.lastIndexOf("/")+1);;
            String extension2 = link2.substring(link2.lastIndexOf("."));
            if(extension2.equalsIgnoreCase(".pdf")){
                //Toast.makeText(mContext,"Added to document",Toast.LENGTH_SHORT).show();
                documentList.add(new DocumentDTO(documentName2,link2));
            }else{
                //Toast.makeText(mContext,"Added to image",Toast.LENGTH_SHORT).show();
                imageList.add(new ImageDTO(documentName2,link2));
            }

        }

        String link3 = image3;
        if(link3.length()>0){
            //Toast.makeText(SuchanaDetailActivity.this,"link3 : "+ link3,Toast.LENGTH_SHORT).show();
            String documentName3 = link3.substring(link3.lastIndexOf("/")+1);
            String extension3 = link3.substring(link3.lastIndexOf("."));
            if(extension3.equalsIgnoreCase(".pdf")){
                //Toast.makeText(mContext,"Added to document",Toast.LENGTH_SHORT).show();
                documentList.add(new DocumentDTO(documentName3,link3));
            }else{
                //Toast.makeText(mContext,"Added to image",Toast.LENGTH_SHORT).show();
                imageList.add(new ImageDTO(documentName3,link3));
            }
        }

//        imageList = i.getParcelableArrayListExtra("imageList");
//        documentList = i.getParcelableArrayListExtra("documentList");


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(category);

        news_detail_title = (TextView) findViewById(R.id.news_detail_title);
        news_detail_date_tv = (TextView) findViewById(R.id.news_detail_date_tv);

        news_detail_image_gallery_rl = (RelativeLayout) findViewById(R.id.news_detail_image_gallery_rl);
        news_detail_document_gallery_rl = (RelativeLayout) findViewById(R.id.news_detail_document_gallery_rl);

        news_detail_image_text = (TextView) findViewById(R.id.news_detail_image_text);
        no_image_detail_text = (TextView) findViewById(R.id.no_image_detail_text);
        news_detail_document_text = (TextView) findViewById(R.id.news_detail_document_text);
        no_document_detail_text = (TextView) findViewById(R.id.no_document_detail_text);

        news_detail_image_recyclerView = (RecyclerView) findViewById(R.id.news_detail_image_recyclerView);
        news_detail_document_recyclerView = (RecyclerView) findViewById(R.id.news_detail_document_recyclerView);


        news_detail_title.setText(title);
        news_detail_date_tv.setText(getString(R.string.submit_date) + ParseUtil.parseDateString(date));


        //setting images
        news_detail_image_recyclerView.setHasFixedSize(true);
        news_detail_image_recyclerView.setLayoutManager(new LinearLayoutManager(SuchanaDetailActivity.this, LinearLayoutManager.HORIZONTAL, false));
        imageAdapter = new ImageAdapter(getLayoutInflater(), SuchanaDetailActivity.this);
        imageAdapter.addmage(imageList,category);
        news_detail_image_recyclerView.setAdapter(imageAdapter);
        imageAdapter.setClickListener(this);


        //setting documents
        news_detail_document_recyclerView.setHasFixedSize(true);
        news_detail_document_recyclerView.setLayoutManager(new LinearLayoutManager(SuchanaDetailActivity.this, LinearLayoutManager.HORIZONTAL, false));
        documentAdapter = new DocumentAdapter(getLayoutInflater(), SuchanaDetailActivity.this);
        documentAdapter.addDocument(documentList,category);
        news_detail_document_recyclerView.setAdapter(documentAdapter);
        documentAdapter.setClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view, int position) {
    }
}
