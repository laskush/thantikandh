package com.laskushnepal.thantikandh.activity.Karmachari;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.adapter.ContactAdapter;

public class EmployeeDetailActivity extends AppCompatActivity{

    //Constant tags
    public static final int PERMISSIONS_MULTIPLE_REQUEST = 123;

    //views
    private ImageView toolbar_backbtn_iv;
    private Toolbar toolbar;
    private TextView toolbar_tv;
    ImageView employee_imageView;
    TextView detail_employee_name,detail_employee_designation,detail_employee_section,
            detail_employee_email,detail_employee_mobile_no,detail_employee_office_no;

    LinearLayout detail_employess_email_ll,detail_employess_mobile_ll,detail_employess_phone_ll;

    //variables
    Intent i;
    String empName,empDesignation,empSection,empEmail,empOffice,empMobile,empImageURL;
    ArrayList<String> mobileNumberList = new ArrayList<>();
    ArrayList<String> officeNumberList = new ArrayList<>();
    ArrayList<String> emailList = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_detail);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestRuntimePermission();
        }

        i = getIntent();
        empName = i.getStringExtra("emp_name");
        empDesignation = i.getStringExtra("emp_designation");
        empSection = i.getStringExtra("emp_section");
        empEmail = i.getStringExtra("emp_email");
        empOffice = i.getStringExtra("emp_office_no");
        empMobile = i.getStringExtra("emp_mobile_no");
        empImageURL = i.getStringExtra("emp_image_url");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ActionBar actionBar = getSupportActionBar();
        setTitle("");

        actionBar.setDisplayHomeAsUpEnabled(true);
        //actionBar.setDefaultDisplayHomeAsUpEnabled(true);

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getString(R.string.Karmachari));


        employee_imageView = (ImageView) findViewById(R.id.employee_imageView);
        detail_employee_name = (TextView) findViewById(R.id.detail_employee_name);
        detail_employee_designation = (TextView) findViewById(R.id.detail_employee_designation);
        detail_employee_section = (TextView) findViewById(R.id.detail_employee_section);
        detail_employee_email = (TextView) findViewById(R.id.detail_employee_email);
        detail_employee_mobile_no = (TextView) findViewById(R.id.detail_employee_mobile_no);
        detail_employee_office_no = (TextView) findViewById(R.id.detail_employee_office_no);

        detail_employess_email_ll = (LinearLayout) findViewById(R.id.detail_employess_email_ll);
        detail_employess_mobile_ll = (LinearLayout) findViewById(R.id.detail_employess_mobile_ll);
        detail_employess_phone_ll = (LinearLayout) findViewById(R.id.detail_employess_phone_ll);

        defineClick();

        //setting image in imageView

        detail_employee_name.setText(empName);
        detail_employee_designation.setText(empDesignation);
        detail_employee_section.setText(empSection);
        detail_employee_email.setText(empEmail);
        detail_employee_mobile_no.setText(empMobile);
        detail_employee_office_no.setText(empOffice);

        Glide.with(EmployeeDetailActivity.this).load(empImageURL).error(R.drawable.logo).into(employee_imageView);

    }

    private void defineClick() {

        detail_employess_email_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(empEmail.length() == 0){
                    showSnackbar(getResources().getString(R.string.no_email));
                    //Toast.makeText(EmployeeDetailActivity.this,getResources().getString(R.string.no_email),Toast.LENGTH_SHORT).show();
                } else if (empEmail.contains(",")) {
                    emailList = new ArrayList<>();
                    emailList = getEmail(empEmail,",");
                    showEmailDialog(emailList,getString(R.string.email_dialog_title),"email");


                } else {
                    emailList = new ArrayList<>();
                    emailList.add(empEmail.substring(0, empEmail.length()));
                    showEmailDialog(emailList,getString(R.string.email_dialog_title),"email");
                }

            }
        });

        detail_employess_mobile_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(empMobile.length() == 0){
                    //Toast.makeText(EmployeeDetailActivity.this,getResources().getString(R.string.no_mobile_no),Toast.LENGTH_SHORT).show();
                    showSnackbar(getResources().getString(R.string.no_mobile_no));
                } else if (empMobile.contains(",")) {
                   // Toast.makeText(EmployeeDetailActivity.this,"Contain ,",Toast.LENGTH_SHORT).show();
                    mobileNumberList = new ArrayList<>();
                    mobileNumberList = getMobile(empMobile,",");

                    showEmailDialog(mobileNumberList,getString(R.string.mobile_dialog_title),"mobile");
                } else {
                    mobileNumberList = new ArrayList<>();
                    mobileNumberList.add(empMobile.substring(0, empMobile.length()));

                    showEmailDialog(mobileNumberList,getString(R.string.mobile_dialog_title),"mobile");
                }

            }
        });

        detail_employess_phone_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(empOffice.length() == 0){
                    //Toast.makeText(EmployeeDetailActivity.this,getResources().getString(R.string.no_phone_no),Toast.LENGTH_SHORT).show();
                    showSnackbar(getResources().getString(R.string.no_phone_no));
                } else if (empOffice.contains(",")) {
                    officeNumberList = new ArrayList<>();
                    officeNumberList = getPhone(empMobile,",");
                    showEmailDialog(officeNumberList,getString(R.string.phone_dialog_title),"phone");
                } else {
                    officeNumberList = new ArrayList<>();
                    officeNumberList.add(empOffice.substring(0, empOffice.length()));
                    showEmailDialog(officeNumberList,getString(R.string.phone_dialog_title),"phone");
                }

            }
        });


    }

    private void showEmailDialog(ArrayList<String> emailList,String dialog_title,String type) {

        final Dialog dialog = new Dialog(EmployeeDetailActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.contact_list_dialogbox);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();


        TextView title = (TextView) dialog.findViewById(R.id.dialogList_title);
        title.setText(dialog_title);
        RecyclerView number_recyclerView = (RecyclerView) dialog.findViewById(R.id.contact_recyclerView);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);

        ContactAdapter contactAdapter;
        contactAdapter = new ContactAdapter(dialog.getLayoutInflater(),EmployeeDetailActivity.this);
        contactAdapter.addContact(emailList,type);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(EmployeeDetailActivity.this);

        number_recyclerView.setLayoutManager(layoutManager);
        number_recyclerView.setItemAnimator(new DefaultItemAnimator());
        number_recyclerView.setAdapter(contactAdapter);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    //checking whether the string contain the character , and position of the the character , and finding the total numbers
    public ArrayList<String> getEmail(String email, String substr) {

        int length = email.length();
        int pre_index = 0;

        ArrayList<String> emailAddress = new ArrayList<>();
        int index = email.indexOf(substr);
        while (index >= 0) {
            //Toast.makeText(mContext,"String : "+ str.substring(pre_index,index),Toast.LENGTH_SHORT).show();
            emailAddress.add(email.substring(pre_index, index));
            pre_index = index + 1;
            if(email.substring(pre_index,length).contains(substr)){
                index = email.indexOf(substr, pre_index);
            }else{
                emailAddress.add(email.substring(pre_index, length-1));
                index = -1;
            }
        }

        return emailAddress;
    }



    public ArrayList<String> getMobile(String mobile, String substr) {

        int length = mobile.length();
        int pre_index = 0;

        ArrayList<String> mobileNumber = new ArrayList<>();
        int index = mobile.indexOf(substr);

        while (index >= 0) {
            mobileNumber.add(mobile.substring(pre_index, index));
            pre_index = index + 1;
            if(mobile.substring(pre_index,length).contains(substr)){
                index = mobile.indexOf(substr, pre_index);
            }else{
                mobileNumber.add(mobile.substring(pre_index, length-1));
                index = -1;
            }


        }

        return mobileNumber;
    }


    public ArrayList<String> getPhone(String phone, String substr) {

        int length = phone.length();
        int pre_index = 0;

        ArrayList<String> phoneNumber = new ArrayList<>();
        int index = phone.indexOf(substr);

        while (index >= 0) {
            phoneNumber.add(phone.substring(pre_index, index));
            pre_index = index + 1;
            if(phone.substring(pre_index,length).contains(substr)){
                index = phone.indexOf(substr, pre_index);
            }else{
                phoneNumber.add(phone.substring(pre_index, length-1));
                index = -1;
            }
        }

        return phoneNumber;
    }


    @TargetApi(Build.VERSION_CODES.M)
    public void requestRuntimePermission() {

       /* if (ContextCompat.checkSelfPermission(EmployeeDetailActivity.this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(
                    new String[]{Manifest.permission.CALL_PHONE},
                    PERMISSIONS_MULTIPLE_REQUEST);

        }*/
        if (ContextCompat.checkSelfPermission(EmployeeDetailActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED &&

                ContextCompat.checkSelfPermission(EmployeeDetailActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED &&

                ContextCompat.checkSelfPermission(EmployeeDetailActivity.this,
                        Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {


            requestPermissions(
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CALL_PHONE},
                    PERMISSIONS_MULTIPLE_REQUEST);

        }else {
            // write your logic code if permission already granted
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

            super.onBackPressed();
    }

    void showSnackbar(String message){


            Snackbar.make(findViewById(R.id.toolbar_iv), message,
                    Snackbar.LENGTH_LONG).show();

    }

}
