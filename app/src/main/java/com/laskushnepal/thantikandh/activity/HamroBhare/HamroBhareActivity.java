package com.laskushnepal.thantikandh.activity.HamroBhare;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.laskushnepal.thantikandh.R;

public class HamroBhareActivity extends AppCompatActivity {

    //view
    Toolbar toolbar;
    TextView toolbar_tv;

    CardView rajan_cardView,jayess_cardView,dinesh_CardView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hamro_bhare);

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getResources().getString(R.string.about_us));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        rajan_cardView = (CardView) findViewById(R.id.rajan_cardView);
        jayess_cardView = (CardView) findViewById(R.id.jayess_cardView);
        dinesh_CardView = (CardView) findViewById(R.id.dinesh_cardView);

        rajan_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent( android.content.Intent.ACTION_SEND);
                intent.setType("plain/text");

                intent.putExtra( android.content.Intent.EXTRA_EMAIL, new String[] { getString(R.string.rajan_email) });
                //intent.putExtra(Intent.EXTRA_EMAIL, "rajanshresthance@gmail.com");
                //intent.putExtra( android.content.Intent.EXTRA_SUBJECT, "Subject");
                //intent.putExtra( android.content.Intent.EXTRA_TEXT, "I'm email body.");

                startActivity(Intent.createChooser(intent, "Send Email"));
            }
        });

        jayess_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent( android.content.Intent.ACTION_SEND);
                intent.setType("plain/text");

                intent.putExtra( android.content.Intent.EXTRA_EMAIL, new String[] { getString(R.string.jayess_email) });
                //intent.putExtra(Intent.EXTRA_EMAIL, "rajanshresthance@gmail.com");
                //intent.putExtra( android.content.Intent.EXTRA_SUBJECT, "Subject");
                //intent.putExtra( android.content.Intent.EXTRA_TEXT, "I'm email body.");

                startActivity(Intent.createChooser(intent, "Send Email"));
            }
        });

        dinesh_CardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent( android.content.Intent.ACTION_SEND);
                intent.setType("plain/text");

                intent.putExtra( android.content.Intent.EXTRA_EMAIL, new String[] { getString(R.string.dinesh_email) });
                //intent.putExtra(Intent.EXTRA_EMAIL, "rajanshresthance@gmail.com");
                //intent.putExtra( android.content.Intent.EXTRA_SUBJECT, "Subject");
                //intent.putExtra( android.content.Intent.EXTRA_TEXT, "I'm email body.");

                startActivity(Intent.createChooser(intent, "Send Email"));
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
