package com.laskushnepal.thantikandh.activity.Wada_karyalaya;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.adapter.EmployeeListAdapter;
import com.laskushnepal.thantikandh.adapter.WodaEmployeeListAdapter;
import com.laskushnepal.thantikandh.api.APIServices;
import com.laskushnepal.thantikandh.fragment.Karmachari.NewEmployeeFragment;
import com.laskushnepal.thantikandh.model.Karmachari.EmployeeDTO;
import com.laskushnepal.thantikandh.model.Karmachari.EmployeeResponse;
import com.laskushnepal.thantikandh.model.wodakaryala.WodaEmpResponse;
import com.laskushnepal.thantikandh.model.wodakaryala.WodaEmployeeDTO;
import com.laskushnepal.thantikandh.servicegenerator.RetrofitApiClient;
import com.laskushnepal.thantikandh.utils.NetworkUtil;
import com.laskushnepal.thantikandh.utils.PrefUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class WodaKaryalayaEmployeeActivity extends AppCompatActivity {

    //constant tags
    private static String TAG = WodaKaryalayaEmployeeActivity.class.getSimpleName();

    //view
    ProgressBar news_progressBar;
    RecyclerView news_recyclerView;
    private Toolbar toolbar;
    private ImageView toolbar_iv;
    private TextView toolbar_tv;

    //instances
    private WodaEmployeeListAdapter employeeListAdapter;
    LinearLayoutManager mManager;

    //variable
    private ArrayList<WodaEmployeeDTO> employeeList= new ArrayList<>();
    int position;
    String wodaId;
    Intent i;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wodakaryalaya_employee);
        i = getIntent();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getString(R.string.Karmachari));

        wodaId = i.getStringExtra("id");
        //Toast.makeText(this, "ID : "+ wodaId, Toast.LENGTH_SHORT).show();

        news_recyclerView = (RecyclerView) findViewById(R.id.news_recyclerView);
        news_progressBar = (ProgressBar) findViewById(R.id.news_progressBar);

        mManager = new LinearLayoutManager(WodaKaryalayaEmployeeActivity.this);
        news_recyclerView.setLayoutManager(mManager);
        employeeListAdapter = new WodaEmployeeListAdapter(getLayoutInflater(),WodaKaryalayaEmployeeActivity.this);


        if(NetworkUtil.isInternetConnectionAvailable(WodaKaryalayaEmployeeActivity.this)){
            news_progressBar.setVisibility(View.VISIBLE);
            getEmployeeList();
        }else{
            showSnackbar(getString(R.string.no_internet),false);
            employeeList = PrefUtils.getWodaKarmachariList(WodaKaryalayaEmployeeActivity.this,PrefUtils.WODA_EMPLOYESS_RESPONSE+wodaId);
            if(employeeList != null){
                setEmployeeList(employeeList);
            }
        }
    }


    private void getEmployeeList() {

        final Retrofit retrofit = new RetrofitApiClient(WodaKaryalayaEmployeeActivity.this).getAdapter();
        final APIServices apiServices = retrofit.create(APIServices.class);

        apiServices.getWodaKaryalayaEmployee(wodaId).enqueue(new Callback<WodaEmpResponse>() {

            @Override
            public void onResponse(Call<WodaEmpResponse> call, Response<WodaEmpResponse> response) {
                try {

                    if (response.body() != null) {
                        WodaEmpResponse wodaEmpResponse = response.body();
                        Log.d("Response : ", String.valueOf(response.body()));
                        employeeList = wodaEmpResponse.getWodaEmployeeList();
                        //Toast.makeText(WodaKaryalayaEmployeeActivity.this, "Response : "+ wodaEmpResponse.getStatus(), Toast.LENGTH_SHORT).show();
                        PrefUtils.saveWodaKarmachariList(WodaKaryalayaEmployeeActivity.this,employeeList,PrefUtils.WODA_EMPLOYESS_RESPONSE+wodaId);
                        news_progressBar.setVisibility(View.GONE);
                        if (employeeList != null) {
                            setEmployeeList(employeeList);
                        }

                    } else {
                        showSnackbar(getString(R.string.no_data),false);
                        news_progressBar.setVisibility(View.GONE);
                       // Toast.makeText(WodaKaryalayaEmployeeActivity.this, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    }


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WodaEmpResponse> call, Throwable t) {
                Log.d("Failure : ", t.getMessage());
                news_progressBar.setVisibility(View.GONE);
                Toast.makeText(WodaKaryalayaEmployeeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void setEmployeeList(List<WodaEmployeeDTO> employeeList) {

        employeeListAdapter.addWodaEmployeeList(employeeList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(WodaKaryalayaEmployeeActivity.this);

        news_recyclerView.setLayoutManager(layoutManager);
        news_recyclerView.setItemAnimator(new DefaultItemAnimator());
        //important_number_recyclerView.addItemDecoration(new ItemOffsetDecoration(PostActivity.this, R.dimen.item_offset));
        news_recyclerView.setAdapter(employeeListAdapter);
    }


    void showSnackbar(String message,boolean action){

        if(action == true){

            Snackbar.make(news_recyclerView, message,
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.try_again), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Request the permission
                    //Toast.makeText(WodaKaryalayaEmployeeActivity.this, "Action Clicked", Toast.LENGTH_SHORT).show();
                    getEmployeeList();
                }
            }).show();
        }else{

            Snackbar.make(news_recyclerView, message, Snackbar.LENGTH_LONG).show();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
