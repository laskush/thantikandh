package com.laskushnepal.thantikandh.activity.Gallery;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.model.Gallery.GalleryImageResponse;
import com.laskushnepal.thantikandh.Interface.RecylerItemClickListner;
import com.laskushnepal.thantikandh.adapter.SliderItemDetailAdapter;
import com.laskushnepal.thantikandh.adapter.SliderItemPageAdapter;
import com.laskushnepal.thantikandh.api.APIServices;
import com.laskushnepal.thantikandh.model.Gallery.SingleGalleryDTO;
import com.laskushnepal.thantikandh.servicegenerator.RetrofitApiClient;
import com.laskushnepal.thantikandh.utils.NetworkUtil;
import com.laskushnepal.thantikandh.utils.PrefUtils;
import com.laskushnepal.thantikandh.utils.HackyViewPager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class GalleryDetailActivity extends AppCompatActivity implements RecylerItemClickListner {

    //Constant Tags
    public final String TAG = GalleryDetailActivity.class.getName();

    //Views
    private Toolbar toolbar;
    private HackyViewPager gallery_viewPager;
    private ImageView image_gallery_ImageView;
    private RecyclerView image_gallery_recyclerView;
    private TextView toolbar_tv,gallery_detail_tv;


    //Instances
    SliderItemPageAdapter sliderItemPageAdapter;
    SliderItemDetailAdapter sliderItemDetailAdapter;

    //variables
    Intent i;
    ArrayList<SingleGalleryDTO> singleGalleryList;
    String galleryId;
    String galleryTitle;
    int position;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_detail);

        i = getIntent();
        galleryId = i.getStringExtra("GalleryId");
        galleryTitle = i.getStringExtra("GalleryTitle");


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ActionBar actionBar = getSupportActionBar();
        setTitle("");

        actionBar.setDisplayHomeAsUpEnabled(true);
        //actionBar.setDefaultDisplayHomeAsUpEnabled(true);

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getString(R.string.gallery));

        gallery_viewPager = (HackyViewPager) findViewById(R.id.gallery_viewPager);
        image_gallery_ImageView = (ImageView) findViewById(R.id.image_gallery_ImageView);
        image_gallery_recyclerView = (RecyclerView) findViewById(R.id.image_detail_gallery_recyclerView);
        gallery_detail_tv = (TextView) findViewById(R.id.gallery_detail_tv);

        gallery_detail_tv.setText(galleryTitle);

        if(NetworkUtil.isInternetConnectionAvailable(GalleryDetailActivity.this)){
            getsingleGalleyList();
        }else{
            showSnackbar(getString(R.string.no_internet),false);
            singleGalleryList = PrefUtils.getGalleryList(GalleryDetailActivity.this,PrefUtils.GALLERY__RESPONSE+galleryId);
            if(singleGalleryList!= null){
                setsingleGalleyList(singleGalleryList);
            }
        }


    }

    private void getsingleGalleyList() {

        final Retrofit retrofit = new RetrofitApiClient(GalleryDetailActivity.this).getAdapter();
        final APIServices apiServices = retrofit.create(APIServices.class);

        apiServices.getImageGallery(galleryId).enqueue(new Callback<GalleryImageResponse>() {

            @Override
            public void onResponse(Call<GalleryImageResponse> call, Response<GalleryImageResponse> response) {
                try {

                    if (response.body() != null) {
                        GalleryImageResponse galleryImageResponse = response.body();
                        Log.d("Response : ", String.valueOf(response.body()));
                        singleGalleryList = galleryImageResponse.getSingleGalleryList();
                        PrefUtils.saveGalleryList(GalleryDetailActivity.this,singleGalleryList,PrefUtils.GALLERY__RESPONSE+galleryId);
                        if(singleGalleryList!= null){
                            setsingleGalleyList(singleGalleryList);
                        }

                    } else {
                        showSnackbar(getString(R.string.no_data),false);
                        //Toast.makeText(GalleryDetailActivity.this, getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    }


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GalleryImageResponse> call, Throwable t) {
                showSnackbar(getString(R.string.load_failed),true);
                Log.d("Failure : ", t.getMessage());
                //Toast.makeText(GalleryDetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void setsingleGalleyList(ArrayList<SingleGalleryDTO> singleGalleryList) {

        if (singleGalleryList!= null) {

            gallery_viewPager.setAdapter(new SliderItemPageAdapter(GalleryDetailActivity.this, singleGalleryList));
            gallery_viewPager.setCurrentItem(position);

            gallery_viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                        feeditemdetail_RecyclerView.scrollToPosition(position);
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });



            if (singleGalleryList.size() == 1) {

                image_gallery_recyclerView.setVisibility(View.GONE);
            } else {
                try {

                    image_gallery_recyclerView.setHasFixedSize(true);
                    image_gallery_recyclerView.setLayoutManager(new LinearLayoutManager(GalleryDetailActivity.this, LinearLayoutManager.HORIZONTAL, false));
                    //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
                    sliderItemDetailAdapter = new SliderItemDetailAdapter(getLayoutInflater(), GalleryDetailActivity.this, "ImageGallery", position);
                    sliderItemDetailAdapter.addImageGallery(singleGalleryList);
                    image_gallery_recyclerView.setAdapter(sliderItemDetailAdapter);
                    sliderItemDetailAdapter.setClickListener(this);


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onClick(View view, int position) {
            gallery_viewPager.setCurrentItem(position);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    void showSnackbar(String message,boolean action){

        if(action == true){

            Snackbar.make(findViewById(R.id.toolbar_iv), message,
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.try_again), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Request the permission
                    //Toast.makeText(GalleryDetailActivity.this, "Action Clicked", Toast.LENGTH_SHORT).show();
                    getsingleGalleyList();
                }
            }).show();
        }else{

            Snackbar.make(findViewById(R.id.toolbar_iv), message,
                    Snackbar.LENGTH_LONG).show();
        }

    }
}
