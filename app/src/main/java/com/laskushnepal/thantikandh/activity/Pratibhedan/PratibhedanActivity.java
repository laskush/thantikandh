package com.laskushnepal.thantikandh.activity.Pratibhedan;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.activity.Parichya.ParichyaActivity;
import com.laskushnepal.thantikandh.api.APIServices;
import com.laskushnepal.thantikandh.fragment.Pratibhedan.PratibhedanFragment;
import com.laskushnepal.thantikandh.model.News.NewsCategoryDTO;
import com.laskushnepal.thantikandh.model.News.NewsCategoryResponse;
import com.laskushnepal.thantikandh.servicegenerator.RetrofitApiClient;
import com.laskushnepal.thantikandh.utils.NetworkUtil;
import com.laskushnepal.thantikandh.utils.PrefUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PratibhedanActivity extends AppCompatActivity{

    //Constant Tags
    private final String TAG = ParichyaActivity.class.getSimpleName();

    //view
    Toolbar toolbar;
    TextView toolbar_tv;
    TabLayout news_TabLayout;
    ViewPager news_ViewPager;

    //Instances

    //Variables
    ArrayList<NewsCategoryDTO> newsCategoryList = new ArrayList<>();
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suchana_jankari);

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getResources().getString(R.string.pratibhedan));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        news_TabLayout = (TabLayout) findViewById(R.id.news_TabLayout);
        news_ViewPager = (ViewPager) findViewById(R.id.news_ViewPager);

        if(NetworkUtil.isInternetConnectionAvailable(PratibhedanActivity.this)){
            showProgressDialog(true);
            getPratibhedanTabs();
        }else{
            showSnackbar(getString(R.string.no_internet),false);
            newsCategoryList = PrefUtils.getPradibhedanCategoryList(PratibhedanActivity.this,PrefUtils.PRATIBHEDAN_CATEGORY_RESPONSE);
            if (newsCategoryList != null) {
                news_ViewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager(), newsCategoryList));
                news_TabLayout.setupWithViewPager(news_ViewPager);
            }
        }


//        news_ViewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
//        news_TabLayout.setupWithViewPager(news_ViewPager);
    }

    private void getPratibhedanTabs() {

        final Retrofit retrofit = new RetrofitApiClient(PratibhedanActivity.this).getAdapter();
        final APIServices apiServices = retrofit.create(APIServices.class);

        apiServices.getReportCategory().enqueue(new Callback<NewsCategoryResponse>() {

            @Override
            public void onResponse(Call<NewsCategoryResponse> call, Response<NewsCategoryResponse> response) {
                try {

                    if (response.body() != null) {
                        NewsCategoryResponse newsCategoryResponse = response.body();
                        Log.d("Response : ", String.valueOf(response.body()));
                        newsCategoryList = newsCategoryResponse.getNewsCategoryList();
                        PrefUtils.savePradibhedanCategoryList(PratibhedanActivity.this,newsCategoryList,PrefUtils.PRATIBHEDAN_CATEGORY_RESPONSE);
                        showProgressDialog(false);
                        if (newsCategoryList != null) {
                            news_ViewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager(), newsCategoryList));
                            news_TabLayout.setupWithViewPager(news_ViewPager);
                        }

                    } else {
                        showSnackbar(getString(R.string.no_data),false);
                        showProgressDialog(false);
                        //Toast.makeText(PratibhedanActivity.this, "Response : null", Toast.LENGTH_SHORT).show();
                    }


                } catch (NullPointerException e) {
                    showProgressDialog(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<NewsCategoryResponse> call, Throwable t) {
                Log.d("Failure : ", t.getMessage());
                showSnackbar(getString(R.string.load_failed),true);
                showProgressDialog(false);
                Toast.makeText(PratibhedanActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

   /* class MyPagerAdapter extends FragmentPagerAdapter {

        private ArrayList<NewsCategoryDTO> tabList;

        public MyPagerAdapter(FragmentManager fm, ArrayList<NewsCategoryDTO> mtabList) {
            super(fm);
            this.tabList = mtabList;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                if (position <= tabList.size()) {

                    *//*SuchanaJankariFragment suchanaJankariFragment = new SuchanaJankariFragment(position, "");
                    suchanaJankariFragment.setRetainInstance(true);
                    return suchanaJankariFragment;
*//*

                    return null;
                }
                return null;

            }

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabList.get(position).getTitle();

        }

        @Override
        public int getCount() {
            return tabList.size();

        }

    }*/


    class MyPagerAdapter extends FragmentPagerAdapter {

        private ArrayList<NewsCategoryDTO> tabList;

        public MyPagerAdapter(FragmentManager fm, ArrayList<NewsCategoryDTO> tabList) {
            super(fm);
            this.tabList = tabList;
        }


        @Override
        public Fragment getItem(int position) {

            if(position <= tabList.size()){
                PratibhedanFragment pratibhedanFragment = new PratibhedanFragment();
                Bundle data = new Bundle();//create bundle instance
                data.putString("slug", tabList.get(position).getSlug());
                data.putString("category", tabList.get(position).getTitle());
                pratibhedanFragment.setArguments(data);
                pratibhedanFragment.setRetainInstance(true);
                return pratibhedanFragment;
            }

            // TODO Auto-generated method stub
            return null;

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabList.get(position).getTitle();

        }

        @Override
        public int getCount() {
            return tabList.size();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void showProgressDialog(boolean shouldShow) {
        if(shouldShow){
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(PratibhedanActivity.this);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(true);
                progressDialog.setMessage(getResources().getString(R.string.loading_data));
                progressDialog.show();
            }
        }else{
            if(progressDialog!=null){
                progressDialog.dismiss();
                progressDialog=null;
            }
        }

    }

    void showSnackbar(String message,boolean action){

        if(action == true){

            Snackbar.make(findViewById(R.id.toolbar_iv), message,
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.try_again), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Request the permission
                    //Toast.makeText(PratibhedanActivity.this, "Action Clicked", Toast.LENGTH_SHORT).show();
                    getPratibhedanTabs();
                }
            }).show();
        }else{

            Snackbar.make(findViewById(R.id.toolbar_iv), message,
                    Snackbar.LENGTH_LONG).show();
        }

    }

}
