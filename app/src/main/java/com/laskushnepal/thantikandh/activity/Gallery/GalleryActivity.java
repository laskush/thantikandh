package com.laskushnepal.thantikandh.activity.Gallery;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.Interface.RecylerItemClickListner;
import com.laskushnepal.thantikandh.adapter.GalleryListAdapter;
import com.laskushnepal.thantikandh.api.APIServices;
import com.laskushnepal.thantikandh.model.Gallery.GalleryCategoryResponse;
import com.laskushnepal.thantikandh.model.Gallery.ImageGalleryDTO;
import com.laskushnepal.thantikandh.servicegenerator.RetrofitApiClient;
import com.laskushnepal.thantikandh.utils.NetworkUtil;
import com.laskushnepal.thantikandh.utils.PrefUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class GalleryActivity extends AppCompatActivity implements RecylerItemClickListner{

    //constant tags
    private static String TAG=GalleryActivity.class.getSimpleName();

    //view
    Toolbar toolbar;
    TextView toolbar_tv;
    RecyclerView image_gallery_recyclerView;

    //instances
    private GalleryListAdapter galleryListAdapter;
    LinearLayoutManager mManager;

    //variable
    private ArrayList<ImageGalleryDTO> galleryList= new ArrayList<>();
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);


        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getResources().getString(R.string.gallery));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        image_gallery_recyclerView = (RecyclerView) findViewById(R.id.image_gallery_recyclerView);

        mManager = new LinearLayoutManager(GalleryActivity.this);
        image_gallery_recyclerView.setLayoutManager(mManager);
        galleryListAdapter = new GalleryListAdapter(getLayoutInflater(),GalleryActivity.this);

        if(NetworkUtil.isInternetConnectionAvailable(GalleryActivity.this)){
            showProgressDialog(true);
            getGalleryList();
        }else{
            showSnackbar(getString(R.string.no_internet),false);
            galleryList = PrefUtils.getGalleryCategoryList(GalleryActivity.this,PrefUtils.GALLERY_CATEGORY_RESPONSE);
            if(galleryList!= null){
                setGalleryList(galleryList);
            }
        }
       /* getGalleryList();
        setGalleryList(galleryList);*/
    }

    private void getGalleryList() {

        final Retrofit retrofit = new RetrofitApiClient(GalleryActivity.this).getAdapter();
        final APIServices apiServices = retrofit.create(APIServices.class);

        apiServices.getGalleryCategory().enqueue(new Callback<GalleryCategoryResponse>() {

            @Override
            public void onResponse(Call<GalleryCategoryResponse> call, Response<GalleryCategoryResponse> response) {
                try {

                    if (response.body() != null) {
                        GalleryCategoryResponse galleryCategoryResponse = response.body();
                        Log.d("Response : ", String.valueOf(response.body()));
                        //Toast.makeText(getContext(), "Response : "+ response.body(), Toast.LENGTH_SHORT).show();
                        galleryList = galleryCategoryResponse.getImageGalleryList();
                        PrefUtils.saveGalleryCategoryList(GalleryActivity.this,galleryList,PrefUtils.GALLERY_CATEGORY_RESPONSE);
                        showProgressDialog(false);
                        if(galleryList!= null){
                           setGalleryList(galleryList);
                        }

                    } else {
                        showSnackbar(getString(R.string.no_data),false);
                        showProgressDialog(false);
                        //Toast.makeText(GalleryActivity.this, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    }


                } catch (NullPointerException e) {
                    showProgressDialog(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GalleryCategoryResponse> call, Throwable t) {
                showSnackbar(getString(R.string.load_failed),true);
                showProgressDialog(false);
                Log.d("Failure : ", t.getMessage());
                //Toast.makeText(GalleryActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


       /* galleryList.add(new ImageGalleryDTO(getContext().getResources().getString(R.string.slider_title1), R.drawable.slider_image1));
        galleryList.add(new ImageGalleryDTO(getContext().getResources().getString(R.string.slider_title2), R.drawable.slider_image2));
        galleryList.add(new ImageGalleryDTO(getContext().getResources().getString(R.string.slider_title3), R.drawable.slider_image3));*/

    }

    private void setGalleryList(List<ImageGalleryDTO> galleryDTOList){


        galleryListAdapter.addGalleryList(galleryDTOList);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(GalleryActivity.this, 2);

        image_gallery_recyclerView.setLayoutManager(layoutManager);
        image_gallery_recyclerView.setItemAnimator(new DefaultItemAnimator());
        //image_gallery_recyclerView.addItemDecoration(new ItemOffsetDecoration(PostActivity.this, R.dimen.item_offset));
        image_gallery_recyclerView.setAdapter(galleryListAdapter);
    }


    @Override
    public void onClick(View view, int position) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showProgressDialog(boolean shouldShow) {
        if(shouldShow){
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(GalleryActivity.this);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(true);
                progressDialog.setMessage(getResources().getString(R.string.loading_data));
                progressDialog.show();
            }
        }else{
            if(progressDialog!=null){
                progressDialog.dismiss();
                progressDialog=null;
            }
        }

    }

    void showSnackbar(String message,boolean action){

        if(action == true){

            Snackbar.make(findViewById(R.id.toolbar_iv), message,
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.try_again), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Request the permission
                    //Toast.makeText(GalleryActivity.this, "Action Clicked", Toast.LENGTH_SHORT).show();
                    getGalleryList();
                }
            }).show();
        }else{
            Snackbar.make(findViewById(R.id.toolbar_iv), message,
                    Snackbar.LENGTH_LONG).show();
        }
    }
}
