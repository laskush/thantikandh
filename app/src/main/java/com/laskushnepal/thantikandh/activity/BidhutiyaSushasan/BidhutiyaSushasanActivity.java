package com.laskushnepal.thantikandh.activity.BidhutiyaSushasan;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.api.APIServices;
import com.laskushnepal.thantikandh.fragment.BidhutiyaSushasan.BidhutiyaFramgment;
import com.laskushnepal.thantikandh.model.News.NewsCategoryDTO;
import com.laskushnepal.thantikandh.model.News.NewsCategoryResponse;
import com.laskushnepal.thantikandh.servicegenerator.RetrofitApiClient;
import com.laskushnepal.thantikandh.utils.NetworkUtil;
import com.laskushnepal.thantikandh.utils.PrefUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class BidhutiyaSushasanActivity extends AppCompatActivity {


    //constant tags
    private static String TAG = BidhutiyaSushasanActivity.class.getSimpleName();

    //view
    Toolbar toolbar;
    TextView toolbar_tv;
    TabLayout news_TabLayout;
    ViewPager news_ViewPager;

    //variables
    ArrayList<NewsCategoryDTO> newsCategoryList = new ArrayList<>();
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suchana_jankari);

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getResources().getString(R.string.bidhutiya_sushasan));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        news_TabLayout = (TabLayout) findViewById(R.id.news_TabLayout);
        news_ViewPager = (ViewPager) findViewById(R.id.news_ViewPager);

        if(NetworkUtil.isInternetConnectionAvailable(BidhutiyaSushasanActivity.this)){
            showProgressDialog(true);
            getBidhutiyaTabs();
        }else{
            showSnackbar(getString(R.string.no_internet),false);
            newsCategoryList = PrefUtils.getBidhutiyaCategoryList(BidhutiyaSushasanActivity.this,PrefUtils.BIDHUTIYA_CATEGORY_RESPONSE);
            if (newsCategoryList != null) {
                news_ViewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager(), newsCategoryList));
                news_TabLayout.setupWithViewPager(news_ViewPager);
            }
        }
//        news_ViewPager.setAdapter( new MyPagerAdapter(getSupportFragmentManager()));
//        news_TabLayout.setupWithViewPager(news_ViewPager);

            /*for (int i = 0; i < employee_TabLayout.getTabCount(); i++) {

                if(i == 0){

                }else{

                    TabLayout.Tab tab = employee_TabLayout.getTabAt(i);
                    RelativeLayout relativeLayout = (RelativeLayout)
                            LayoutInflater.from(getContext()).inflate(R.layout.tab_layout, employee_TabLayout, false);

                    TextView tabTextView = (TextView) relativeLayout.findViewById(R.id.tab_title);
                    //socialModeTabLayout.addView(tabTextView,new LinearLayout.LayoutParams(WRAP_CONTENT,MATCH_PARENT,1));
                    tabTextView.setText(tab.getText());
                    tab.setCustomView(relativeLayout);
                    //tab.select();
                }

            }*/
    }

    private void getBidhutiyaTabs() {

        final Retrofit retrofit = new RetrofitApiClient(BidhutiyaSushasanActivity.this).getAdapter();
        final APIServices apiServices = retrofit.create(APIServices.class);

        apiServices.getBidhutiyaCategory().enqueue(new Callback<NewsCategoryResponse>() {

            @Override
            public void onResponse(Call<NewsCategoryResponse> call, Response<NewsCategoryResponse> response) {
                try {

                    if (response.body() != null) {
                        NewsCategoryResponse newsCategoryResponse = response.body();
                        Log.d("Response : ", String.valueOf(response.body()));
                        newsCategoryList = newsCategoryResponse.getNewsCategoryList();
                        PrefUtils.saveBidhutiyaCategoryList(BidhutiyaSushasanActivity.this,newsCategoryList,PrefUtils.BIDHUTIYA_CATEGORY_RESPONSE);
                        showProgressDialog(false);
                        if (newsCategoryList != null) {
                            news_ViewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager(), newsCategoryList));
                            news_TabLayout.setupWithViewPager(news_ViewPager);
                        }

                    } else {
                        showSnackbar(getString(R.string.no_data),false);
                        showProgressDialog(false);
                        Toast.makeText(BidhutiyaSushasanActivity.this, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    }


                } catch (NullPointerException e) {
                    showProgressDialog(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<NewsCategoryResponse> call, Throwable t) {
                Log.d("Failure : ", t.getMessage());
                showSnackbar(getString(R.string.load_failed),true);
                showProgressDialog(false);
                //Toast.makeText(BidhutiyaSushasanActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    class MyPagerAdapter extends FragmentPagerAdapter {

        private ArrayList<NewsCategoryDTO> tabList;

        public MyPagerAdapter(FragmentManager fm,ArrayList<NewsCategoryDTO> tabList) {
            super(fm);
            this.tabList = tabList;
        }


        @Override
        public Fragment getItem(int position) {

            if(position <= tabList.size()){
                //position,tabList.get(position).getSlug()
                BidhutiyaFramgment bidhutiyaFramgment = new BidhutiyaFramgment();
                Bundle data = new Bundle();//create bundle instance
                data.putString("slug", tabList.get(position).getSlug());
                data.putString("category", tabList.get(position).getTitle());
                bidhutiyaFramgment.setArguments(data);
                bidhutiyaFramgment.setRetainInstance(true);
                return bidhutiyaFramgment;
            }

            // TODO Auto-generated method stub
            return null;

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabList.get(position).getTitle();

        }

        @Override
        public int getCount() {
            return tabList.size();
        }
    }

    private void showProgressDialog(boolean shouldShow) {
        if(shouldShow){
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(BidhutiyaSushasanActivity.this);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(true);
                progressDialog.setMessage(getResources().getString(R.string.loading_data));
                progressDialog.show();
            }
        }else{
            if(progressDialog!=null){
                progressDialog.dismiss();
                progressDialog=null;
            }
        }

    }

    void showSnackbar(String message,boolean action){

        if(action == true){
            Snackbar.make(findViewById(R.id.toolbar_iv), message, Snackbar.LENGTH_LONG).setAction(getString(R.string.try_again), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Request the permission
                    //Toast.makeText(BidhutiyaSushasanActivity.this, "Action Clicked", Toast.LENGTH_SHORT).show();
                    getBidhutiyaTabs();
                }
            }).show();
        }else{

            Snackbar.make(findViewById(R.id.toolbar_iv), message, Snackbar.LENGTH_LONG).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
