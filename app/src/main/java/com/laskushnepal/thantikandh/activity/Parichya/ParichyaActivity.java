package com.laskushnepal.thantikandh.activity.Parichya;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.activity.TouchImageDetailActivity;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class ParichyaActivity extends AppCompatActivity {


    //Constant Tags
    private final String TAG = ParichyaActivity.class.getSimpleName();
    public static final int PERMISSIONS_MULTIPLE_REQUEST = 123;

    //Views
    ImageView map1,map2,map3,map4,map5,map6,map7;
    TextView toolbar_tv;
    Toolbar toolbar;
    TextView gaupalika_description_tv,gaupalika_telephone_tv,gaupalika_email_tv,gaupalika_websitelink_tv,
             gaupalika_location_tv,gaupalika_officetime_tv;

    LinearLayout gaupalika_telephone_ll,gaupalika_email_ll,gaupalika_websitelink_ll;

    //variables
    int[] maps =  {R.drawable.map1,R.drawable.map2,R.drawable.map3,R.drawable.map4};

    //Instances

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parichya);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestRuntimePermission();
        }

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getResources().getString(R.string.introduction));
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        gaupalika_description_tv = (TextView) findViewById(R.id.gaupalika_description_tv);
        gaupalika_telephone_tv = (TextView) findViewById(R.id.gaupalika_telephone_tv);
        gaupalika_email_tv = (TextView) findViewById(R.id.gaupalika_email_tv);
        gaupalika_websitelink_tv = (TextView) findViewById(R.id.gaupalika_websitelink_tv);
        gaupalika_location_tv = (TextView) findViewById(R.id.gaupalika_location_tv);
        gaupalika_officetime_tv = (TextView) findViewById(R.id.gaupalika_officetime_tv);

        gaupalika_telephone_ll = (LinearLayout) findViewById(R.id.gaupalika_telephone_ll);
        gaupalika_email_ll = (LinearLayout) findViewById(R.id.gaupalika_email_ll);
        gaupalika_websitelink_ll = (LinearLayout) findViewById(R.id.gaupalika_websitelink_ll);

        map1 = (ImageView) findViewById(R.id.map1);
        map2 = (ImageView) findViewById(R.id.map2);
        map3 = (ImageView) findViewById(R.id.map3);
        map4 = (ImageView) findViewById(R.id.map4);
        map5 = (ImageView) findViewById(R.id.map5);
        map6 = (ImageView) findViewById(R.id.map6);
        map7 = (ImageView) findViewById(R.id.map7);

        defineClick();
    }

    private void defineClick() {

        gaupalika_telephone_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String no = null;
                try {
                    no = URLEncoder.encode(gaupalika_telephone_tv.getText().toString(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Intent callIntent = new Intent("android.intent.action.DIAL");
                callIntent.setData(Uri.parse("tel:" + no));
                if (ActivityCompat.checkSelfPermission(ParichyaActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(callIntent);

            }
        });


        gaupalika_email_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent( android.content.Intent.ACTION_SEND);
                intent.setType("plain/text");

                intent.putExtra( android.content.Intent.EXTRA_EMAIL, new String[] { gaupalika_email_tv.getText().toString() });
                //intent.putExtra(Intent.EXTRA_EMAIL, "rajanshresthance@gmail.com");
                //intent.putExtra( android.content.Intent.EXTRA_SUBJECT, "Subject");
                //intent.putExtra( android.content.Intent.EXTRA_TEXT, "I'm email body.");

                startActivity(Intent.createChooser(intent, "Send Email"));
            }
        });

        gaupalika_websitelink_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              /*  CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                CustomTabsIntent customTabsIntent = builder.build();
                builder.setToolbarColor(getResources().getColor(R.color.colorPrimary));
                customTabsIntent.launchUrl(ParichyaActivity.this,Uri.parse(gaupalika_websitelink_tv.getText().toString()));*/

             /*   Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(gaupalika_websitelink_tv.getText().toString()));
                startActivity(browserIntent);*/

               String url = "http://thantikandhmun.gov.np/";
               /*  CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                builder.setToolbarColor(getResources().getColor(R.color.colorPrimary));
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(ParichyaActivity.this, Uri.parse(url));*/

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);

            }
        });

        map1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i =new Intent(ParichyaActivity.this, TouchImageDetailActivity.class);
                i.putExtra("image","1");
                startActivity(i);
            }
        });

        map2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(ParichyaActivity.this, TouchImageDetailActivity.class);
                i.putExtra("image","2");
                startActivity(i);
            }
        });


        map3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i =new Intent(ParichyaActivity.this, TouchImageDetailActivity.class);
                i.putExtra("image","3");
                startActivity(i);
            }
        });


        map4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i =new Intent(ParichyaActivity.this, TouchImageDetailActivity.class);
                i.putExtra("image","4");
                startActivity(i);
            }
        });

        map5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i =new Intent(ParichyaActivity.this, TouchImageDetailActivity.class);
                i.putExtra("image","5");
                startActivity(i);
            }
        });

        map6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i =new Intent(ParichyaActivity.this, TouchImageDetailActivity.class);
                i.putExtra("image","6");
                startActivity(i);
            }
        });

        map7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i =new Intent(ParichyaActivity.this, TouchImageDetailActivity.class);
                i.putExtra("image","7");
                startActivity(i);
            }
        });
    }


    @TargetApi(Build.VERSION_CODES.M)
    public void requestRuntimePermission() {

        if (ContextCompat.checkSelfPermission(ParichyaActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED &&

                ContextCompat.checkSelfPermission(ParichyaActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED &&

                ContextCompat.checkSelfPermission(ParichyaActivity.this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {


            requestPermissions(
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CALL_PHONE},
                    PERMISSIONS_MULTIPLE_REQUEST);

        } else {
            // write your logic code if permission already granted
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
