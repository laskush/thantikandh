package com.laskushnepal.thantikandh.activity.MahatoPurnaNumber;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.laskushnepal.thantikandh.Interface.RecylerItemClickListner;
import com.laskushnepal.thantikandh.R;
import com.laskushnepal.thantikandh.adapter.ImportantNumberListAdapter;
import com.laskushnepal.thantikandh.api.APIServices;
import com.laskushnepal.thantikandh.model.MahatoPurnaNumber.ImportantNumberDTO;
import com.laskushnepal.thantikandh.model.MahatoPurnaNumber.ImportantNumberResponse;
import com.laskushnepal.thantikandh.servicegenerator.RetrofitApiClient;
import com.laskushnepal.thantikandh.utils.NetworkUtil;
import com.laskushnepal.thantikandh.utils.PrefUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MahatoPurnaNumberActivity extends AppCompatActivity implements RecylerItemClickListner {

    //constant tags
    private static String TAG = MahatoPurnaNumberActivity.class.getSimpleName();
    public static final int PERMISSIONS_MULTIPLE_REQUEST = 123;

    //view
    Toolbar toolbar;
    TextView toolbar_tv;
    RecyclerView important_number_recyclerView;

    //instances
    private ImportantNumberListAdapter importantNumberListAdapter;
    LinearLayoutManager mManager;

    //variable
    private ArrayList<ImportantNumberDTO> importantNumberList= new ArrayList<>();
    ProgressDialog progressDialog;
    Bundle bundle;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahatopurna_number);
        bundle = savedInstanceState;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestRuntimePermission();
        }


        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getResources().getString(R.string.mahattopurna_number));

        toolbar =  (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        important_number_recyclerView = (RecyclerView) findViewById(R.id.important_number_recyclerView);

        mManager = new LinearLayoutManager(MahatoPurnaNumberActivity.this);
        important_number_recyclerView.setLayoutManager(mManager);
        importantNumberListAdapter = new ImportantNumberListAdapter(getLayoutInflater(),MahatoPurnaNumberActivity.this);

        if(NetworkUtil.isInternetConnectionAvailable(MahatoPurnaNumberActivity.this)){
            showProgressDialog(true);
            getImportantNumberList();
        }else{
            showSnackbar(getString(R.string.no_internet),false);
            importantNumberList = PrefUtils.getImportantNumberList(MahatoPurnaNumberActivity.this,PrefUtils.MAHATOPURNA_RESPONSE);
            if (importantNumberList != null) {

                setImportantNumberList(importantNumberList);
            }
          // getImportantNumberList();
        }

    }

    private void getImportantNumberList() {

       /* importantNumberList.add(new ImportantNumberDTO(getResources().getString(R.string.important_num_title1),getResources().getString(R.string.important_num1)+","+getResources().getString(R.string.important_num2)+","+getResources().getString(R.string.important_num2)+","+getResources().getString(R.string.important_num2)+","+getResources().getString(R.string.important_num2) ));
        importantNumberList.add(new ImportantNumberDTO(getResources().getString(R.string.important_num_title2), getResources().getString(R.string.important_num2)));
        importantNumberList.add(new ImportantNumberDTO(getResources().getString(R.string.important_num_title2), getResources().getString(R.string.important_num2) + ","));

        importantNumberList.add(new ImportantNumberDTO(getResources().getString(R.string.important_num_title1),getResources().getString(R.string.important_num1)+" "+getResources().getString(R.string.important_num2) ));
        importantNumberList.add(new ImportantNumberDTO(getResources().getString(R.string.important_num_title2), getResources().getString(R.string.important_num2)));

        importantNumberList.add(new ImportantNumberDTO(getResources().getString(R.string.important_num_title1),getResources().getString(R.string.important_num1)+" "+getResources().getString(R.string.important_num2) ));
        importantNumberList.add(new ImportantNumberDTO(getResources().getString(R.string.important_num_title2), getResources().getString(R.string.important_num2)));

        importantNumberList.add(new ImportantNumberDTO(getResources().getString(R.string.important_num_title1),getResources().getString(R.string.important_num1)+ "," +getResources().getString(R.string.important_num1) ));
        importantNumberList.add(new ImportantNumberDTO(getResources().getString(R.string.important_num_title2), getResources().getString(R.string.important_num2)));*/

       // setImportantNumberList(importantNumberList);

        final Retrofit retrofit = new RetrofitApiClient(MahatoPurnaNumberActivity.this).getAdapter();
        final APIServices apiServices = retrofit.create(APIServices.class);

        apiServices.getImportantNumbers().enqueue(new Callback<ImportantNumberResponse>() {

            @Override
            public void onResponse(Call<ImportantNumberResponse> call, Response<ImportantNumberResponse> response) {
                try {

                    if (response.body() != null) {
                        ImportantNumberResponse importantNumberResponse = response.body();
                        Log.d("Response : ", String.valueOf(response.body()));
                        importantNumberList = importantNumberResponse.getImportantNumberList();
                        PrefUtils.saveImportantNumberList(MahatoPurnaNumberActivity.this,importantNumberList,PrefUtils.MAHATOPURNA_RESPONSE);
                        showProgressDialog(false);
                        if (importantNumberList != null) {
                            setImportantNumberList(importantNumberList);
                        }

                    } else {
                        showSnackbar(getString(R.string.no_data),false);
                        showProgressDialog(false);
                        //Toast.makeText(MahatoPurnaNumberActivity.this, getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    }


                } catch (NullPointerException e) {
                    //showProgressDialog(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ImportantNumberResponse> call, Throwable t) {
                Log.d("Failure : ", t.getMessage());
                showSnackbar(getString(R.string.load_failed),true);
                showProgressDialog(false);
                //Toast.makeText(MahatoPurnaNumberActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setImportantNumberList(List<ImportantNumberDTO> importantNumberList) {

        importantNumberListAdapter.addImportantNumberList(importantNumberList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MahatoPurnaNumberActivity.this);

        important_number_recyclerView.setLayoutManager(layoutManager);
        important_number_recyclerView.setItemAnimator(new DefaultItemAnimator());
        //important_number_recyclerView.addItemDecoration(new ItemOffsetDecoration(PostActivity.this, R.dimen.item_offset));
        important_number_recyclerView.setAdapter(importantNumberListAdapter);
    }

    @Override
    public void onClick(View view, int position) {
        Toast.makeText(MahatoPurnaNumberActivity.this, "ItemClicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showProgressDialog(boolean shouldShow) {
        if(shouldShow){
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(MahatoPurnaNumberActivity.this);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(true);
                progressDialog.setMessage(getResources().getString(R.string.loading_data));
                progressDialog.show();
            }
        }else{
            if(progressDialog!=null){
                progressDialog.dismiss();
                progressDialog=null;
            }
        }

    }

    void showSnackbar(String message,boolean action){

        if(action == true){

            Snackbar.make(findViewById(R.id.toolbar_iv), message,
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.try_again), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Request the permission
                    //Toast.makeText(MahatoPurnaNumberActivity.this, "Action Clicked", Toast.LENGTH_SHORT).show();
                    getImportantNumberList();
                }
            }).show();
        }else{

            Snackbar.make(findViewById(R.id.toolbar_iv), message,
                    Snackbar.LENGTH_LONG).show();
        }

    }



    @TargetApi(Build.VERSION_CODES.M)
    public void requestRuntimePermission() {
       /* if (ContextCompat.checkSelfPermission(MahatoPurnaNumberActivity.this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(
                    new String[]{Manifest.permission.CALL_PHONE},
                    PERMISSIONS_MULTIPLE_REQUEST);

        }*/ if (ContextCompat.checkSelfPermission(MahatoPurnaNumberActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED &&

                ContextCompat.checkSelfPermission(MahatoPurnaNumberActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED &&

                ContextCompat.checkSelfPermission(MahatoPurnaNumberActivity.this,
                        Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {


            requestPermissions(
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CALL_PHONE},
                    PERMISSIONS_MULTIPLE_REQUEST);

        } else {
            // write your logic code if permission already granted
        }
    }

}
